#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import math
from sympy import *
import rospy
from geometry_msgs.msg import PoseArray
from geometry_msgs.msg import Pose
from sensor_msgs.msg import Joy
from sympy.mpmath import norm

class obstacle_movement(object):
  def __init__(self):
    rospy.init_node('obstacle_movement')
    #print "a"
    self.obs_pose = PoseArray()
    self.obs_num = rospy.get_param('/obs_num')
    self.obs_type = rospy.get_param('/obs_form')

    #self.obs = [0.0, 0.0, 0.11]
    self.obs = [0.06, 0.03, 0.36]
    r_small = 0.003
    #self.radius = 0.7*r_small*self.obs_num/(2.0*math.pi)
    self.radius = 0.7*r_small*5.0/(2.0*math.pi)
    self.radius = 0.0
    self.rec_vel = rospy.Subscriber('/joy', Joy, self.ps3_callback)
    obs_pub = rospy.Publisher('obs_pose', PoseArray, queue_size = 10)
    flag = 0
    r = rospy.Rate(40)
    self.delta_x = 0.001
    self.delta_y = 0.001
    self.delta_z = 0.001
    while not rospy.is_shutdown():
      self.obs_pose.poses = []
      if (self.obs_type == 1):
	for i in range(0,self.obs_num):
	  self.obs_pose_ind = Pose()
	  self.obs_pose_ind.position.x = self.obs[0] + self.radius*cos(i*2*pi/self.obs_num)
	  self.obs_pose_ind.position.y = self.obs[1] + self.radius*sin(i*2*pi/self.obs_num)
	  self.obs_pose_ind.position.z = self.obs[2]
	  self.obs_pose_ind.orientation.w = 0.1
	  self.obs_pose_ind.orientation.x = 0.1
	  self.obs_pose_ind.orientation.y = 0.1
	  self.obs_pose_ind.orientation.z = 0.1
	  self.obs_pose.poses.append(self.obs_pose_ind)
      elif (self.obs_type == 2):
	obs_total = 0
	obs_bound = 15
	if((obs_bound % 2)==0):
	  obs_bound_vert = obs_bound/2
	else:
	  obs_bound_vert = (obs_bound+1)/2

	for i in range(0, obs_bound_vert):
	  obs_bound = long(2*pi*self.radius*cos(i*pi/obs_bound_vert-pi/2)/(0.7*r_small))
	  #print(self.radius)
	  #print(2.0*pi*self.radius*cos(i*pi/obs_bound_vert-pi/2.0)/(0.7*r_small))
	  #print(i)
	  for j in range(0, obs_bound):
	    self.obs_pose_ind = Pose()
	    self.obs_pose_ind.position.x = self.obs[0] + self.radius*cos(j*2*pi/obs_bound)*cos(i*pi/obs_bound_vert-pi/2)
	    self.obs_pose_ind.position.y = self.obs[1] + self.radius*sin(j*2*pi/obs_bound)*cos(i*pi/obs_bound_vert-pi/2)
	    self.obs_pose_ind.position.z = self.obs[2] + self.radius*sin(i*pi/obs_bound_vert-pi/2)
	    self.obs_pose_ind.orientation.w = 0.1
	    self.obs_pose_ind.orientation.x = 0.1
	    self.obs_pose_ind.orientation.y = 0.1
	    self.obs_pose_ind.orientation.z = 0.1
	    self.obs_pose.poses.append(self.obs_pose_ind)
	    obs_total = obs_total + 1
	print(obs_total)
	rospy.set_param('/obs_num',obs_total)
      elif(self.obs_type == 3):
	obs_total = 0
	obs_bound = 6

	for i in range(0, obs_bound_vert):
	  obs_bound = long(2*pi*self.radius*cos(i*pi/obs_bound_vert-pi/2)/(0.7*r_small))
	  #print(self.radius)
	  #print(2.0*pi*self.radius*cos(i*pi/obs_bound_vert-pi/2.0)/(0.7*r_small))
	  #print(i)
	  for j in range(0, obs_bound):
	    self.obs_pose_ind = Pose()
	    self.obs_pose_ind.position.x = self.obs[0] + self.radius*cos(j*2*pi/obs_bound)*cos(i*pi/obs_bound_vert-pi/2)
	    self.obs_pose_ind.position.y = self.obs[1] + self.radius*sin(j*2*pi/obs_bound)*cos(i*pi/obs_bound_vert-pi/2)
	    self.obs_pose_ind.position.z = self.obs[2] + self.radius*sin(i*pi/obs_bound_vert-pi/2)
	    self.obs_pose_ind.orientation.w = 0.1
	    self.obs_pose_ind.orientation.x = 0.1
	    self.obs_pose_ind.orientation.y = 0.1
	    self.obs_pose_ind.orientation.z = 0.1
	    self.obs_pose.poses.append(self.obs_pose_ind)
	    obs_total = obs_total + 1
	print(obs_total)
	rospy.set_param('/obs_num',obs_total)

      elif(self.obs_type == 4):
	if(flag==0):
	  self.init_obstacle()
	  self.radius = 0.0
	  flag =1
	self.obs0[0] = self.obs0[0] + self.vel0[0]
	self.obs0[1] = self.obs0[1] + self.vel0[1]
	#self.obs0[2] = self.obs0[2] + self.vel0[2]
	self.obs1[0] = self.obs1[0] + self.vel1[0]
	self.obs1[1] = self.obs1[1] + self.vel1[1]
	#self.obs1[2] = self.obs1[2] + self.vel1[2]
	#self.obs2[0] = self.obs2[0] + self.vel2[0]
	#self.obs2[1] = self.obs2[1] + self.vel2[1]
	#self.obs2[2] = self.obs2[2] + self.vel2[2]

	self.obs_pose_ind = Pose()
	self.obs_pose_ind.position.x = self.obs0[0]
	self.obs_pose_ind.position.y = self.obs0[1]
	self.obs_pose_ind.position.z = self.obs0[2]
	self.obs_pose_ind.orientation.w = 0.1
	self.obs_pose_ind.orientation.x = 0.1
	self.obs_pose_ind.orientation.y = 0.1
	self.obs_pose_ind.orientation.z = 0.1
	self.obs_pose.poses.append(self.obs_pose_ind)

	self.obs_pose_ind = Pose()
	self.obs_pose_ind.position.x = self.obs1[0]
	self.obs_pose_ind.position.y = self.obs1[1]
	self.obs_pose_ind.position.z = self.obs1[2]
	self.obs_pose_ind.orientation.w = 0.1
	self.obs_pose_ind.orientation.x = 0.1
	self.obs_pose_ind.orientation.y = 0.1
	self.obs_pose_ind.orientation.z = 0.1
	self.obs_pose.poses.append(self.obs_pose_ind)

	#self.obs_pose_ind = Pose()
	#self.obs_pose_ind.position.x = self.obs2[0]
	#self.obs_pose_ind.position.y = self.obs2[1]
	#self.obs_pose_ind.position.z = self.obs2[2]
	#self.obs_pose_ind.orientation.w = 0.1
	#self.obs_pose_ind.orientation.x = 0.1
	#self.obs_pose_ind.orientation.y = 0.1
	#self.obs_pose_ind.orientation.z = 0.1
	#self.obs_pose.poses.append(self.obs_pose_ind)
      elif(self.obs_type == 5):
	if(flag==0):
	  self.init_obstacle()
	  self.radius = 0.0
	  flag =1

	self.obs_pose_ind = Pose()
	self.obs_pose_ind.position.x = self.obs0[0]
	self.obs_pose_ind.position.y = self.obs0[1]
	self.obs_pose_ind.position.z = self.obs0[2]
	self.obs_pose_ind.orientation.w = 0.1
	self.obs_pose_ind.orientation.x = 0.1
	self.obs_pose_ind.orientation.y = 0.1
	self.obs_pose_ind.orientation.z = 0.1
	self.obs_pose.poses.append(self.obs_pose_ind)
	self.obs0[0] = self.obs0[0] + self.vel0[0]
	self.obs0[1] = self.obs0[1] + self.vel0[1]

	self.obs_pose_ind = Pose()
	self.obs_pose_ind.position.x = self.obs1[0]
	self.obs_pose_ind.position.y = self.obs1[1]
	self.obs_pose_ind.position.z = self.obs1[2]
	self.obs_pose_ind.orientation.w = 0.1
	self.obs_pose_ind.orientation.x = 0.1
	self.obs_pose_ind.orientation.y = 0.1
	self.obs_pose_ind.orientation.z = 0.1
	self.obs_pose.poses.append(self.obs_pose_ind)
	self.obs1[0] = self.obs1[0] + self.vel1[0]
	self.obs1[1] = self.obs1[1] + self.vel1[1]

      else:
	self.obs_pose_ind = Pose()
	self.obs_pose_ind.position.x = 0.2
	self.obs_pose_ind.position.y = 0.063
	self.obs_pose_ind.position.z = 0.263
	self.obs_pose_ind.orientation.w = 0.1
	self.obs_pose_ind.orientation.x = 0.1
	self.obs_pose_ind.orientation.y = 0.1
	self.obs_pose_ind.orientation.z = 0.1
	self.obs_pose.poses.append(self.obs_pose_ind)
	# self.obs_pose_ind = Pose()
	# self.obs_pose_ind.position.x = 0.015
	# self.obs_pose_ind.position.y = 0.03
	# self.obs_pose_ind.position.z = 1.0095
	# self.obs_pose_ind.orientation.w = 0.1
	# self.obs_pose_ind.orientation.x = 0.1
	# self.obs_pose_ind.orientation.y = 0.1
	# self.obs_pose_ind.orientation.z = 0.1
	# self.obs_pose.poses.append(self.obs_pose_ind)
	# self.obs_pose_ind = Pose()
	# self.obs_pose_ind.position.x = 0.0050
	# self.obs_pose_ind.position.y = 0.08
	# self.obs_pose_ind.position.z = 1.450
	# self.obs_pose_ind.orientation.w = 0.1
	# self.obs_pose_ind.orientation.x = 0.1
	# self.obs_pose_ind.orientation.y = 0.1
	# self.obs_pose_ind.orientation.z = 0.1
	# self.obs_pose.poses.append(self.obs_pose_ind)
	#self.obs_pose_ind = Pose()
	#self.obs_pose_ind.position.x = -0.055
	#self.obs_pose_ind.position.y = 0.1
	##self.obs_pose_ind.position.z = 0.37
	#self.obs_pose_ind.position.z = 0.30
	#self.obs_pose_ind.orientation.w = 0.1
	#self.obs_pose_ind.orientation.x = 0.1
	#self.obs_pose_ind.orientation.y = 0.1
	#self.obs_pose_ind.orientation.z = 0.1
	#self.obs_pose.poses.append(self.obs_pose_ind)

	#print(pi.evalf())
      self.obs_pose.header.stamp = rospy.Time.now()
      obs_pub.publish(self.obs_pose)

      r.sleep()

        #rospy.spin()

  def ps3_callback(self,msg):
    #self.obs_pose.poses = []
    #self.obs_pose_ind.position.x += msg.axes[4]*self.delta_x
    #self.obs_pose_ind.position.y += msg.axes[5]*self.delta_y
    #self.obs_pose_ind.position.z += (msg.buttons[0]-msg.buttons[2])*self.delta_z
    #self.obs_pose.poses.append(self.obs_pose_ind)

    self.obs[0] += msg.axes[4]*self.delta_x
    self.obs[1] += msg.axes[5]*self.delta_y
    self.obs[2] += (msg.buttons[0]-msg.buttons[2])*self.delta_z
    self.radius += (msg.buttons[1]-msg.buttons[3])*self.delta_z

  def init_obstacle(self):
    self.origin = Matrix([[0.05], [0.05]])
    #self.obs0 = Matrix([[-0.035], [0.13815], [0.30]])
    #self.obs0 = Matrix([[0.135], [0.13815], [0.30]])
    self.obs0 = [-0.035, 0.13815, 0.30]

    #self.obs1 = Matrix([[0.3], [0.08], [0.20]])
    self.obs1 = Matrix([[0.1], [0.1], [0.20]])
    self.obs2 = Matrix([[0.40], [-0.40], [0.10]])
    #vector0 = Matrix([[self.obs0[0]], [self.obs0[1]]])
    #self.vel0 = Matrix([[-0.0003], [0.0]])
    self.vel1 = Matrix([[-0.00008], [-0.000085]])

    self.vel0 = Matrix([[0.00005], [-0.00015]])

    #vector1 = Matrix([[self.obs1[0]], [self.obs1[1]]])
    #vector2 = Matrix([[self.obs2[0]], [self.obs2[1]]])
    #self.vel0 = (self.origin - vector0)/norm((self.origin - vector0))*0.001
    #self.vel1 = (self.origin - vector1)/norm((self.origin - vector1))*0.001
    #self.vel2 = (self.origin - vector2)/norm((self.origin - vector2))*0.001


if __name__ == '__main__':
    try:
        obstacle_movement()
    except rospy.ROSInterruptException: pass
