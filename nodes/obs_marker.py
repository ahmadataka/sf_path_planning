#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import rospy
import tf
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray

def obs_marker():
    rospy.init_node('obs_marker')
    markerArray = MarkerArray()
    marker_pub = rospy.Publisher('obstacle_marker', MarkerArray, queue_size = 10)
    obs_num = rospy.get_param('/obs_num')
    r = rospy.Rate(40)
    marker_size = 0.06
    while not rospy.is_shutdown():
      markerArray.markers = []
      for i in range(0, obs_num):
	marker = Marker()
	marker.header.frame_id = "/obs"+str(i)
	marker.header.stamp = rospy.Time.now()
	marker.ns = "obstacle"
	marker.id = i
        marker.type = marker.SPHERE
        marker.action = marker.ADD
        marker.pose.position.x = 0;
	marker.pose.position.y = 0;
	marker.pose.position.z = 0;
	marker.pose.orientation.x = 0.0;
	marker.pose.orientation.y = 0.0;
	marker.pose.orientation.z = 0.0;
	marker.pose.orientation.w = 1.0;
        marker.scale.x = marker_size;
	marker.scale.y = marker_size;
	marker.scale.z = marker_size;
	marker.color.r = 0.0;
	marker.color.g = 1.0;
	marker.color.b = 0.0;
	marker.color.a = 1.0;
	marker.lifetime = rospy.Duration();
	markerArray.markers.append(marker)
      marker_pub.publish(markerArray)

      r.sleep()
      #rospy.spin()

if __name__ == '__main__':
    try:
        obs_marker()
    except rospy.ROSInterruptException: pass
