#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import rospy
from std_msgs.msg import Float64MultiArray

class pressure_producer(object):
  def __init__(self):
    rospy.init_node('pressure_producer')
    self.l = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    self.l_cur = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    self.p = [0.1*1e5, 0.1*1e5, 0.1*1e5, 0.1*1e5, 0.1*1e5, 0.1*1e5, 0.1*1e5, 0.1*1e5, 0.1*1e5]
    self.p_sent = Float64MultiArray()
    pressure_pub = rospy.Publisher('/pressure', Float64MultiArray)
    rec_length = rospy.Subscriber('/length', Float64MultiArray, self.length_callback)
    rec_cur_length = rospy.Subscriber('/cur_length', Float64MultiArray, self.cur_length_callback)
    delta_p = 0.05*1e5
    rate = rospy.Rate(40.0)
    while not rospy.is_shutdown():
      self.p_sent.data = []
      for i in range(0, 8):
	if(self.l_cur[i]<self.l[i]):
	  self.p[i] += delta_p
	elif(self.l_cur[i]>self.l[i]):
	  self.p[i] -= delta_p
	self.p_sent.data.append(self.p[i])
      pressure_pub.publish(self.p_sent)
      rate.sleep()

  def length_callback(self, msg):
    self.l[0] = msg.data[0]
    self.l[1] = msg.data[1]
    self.l[2] = msg.data[2]   
    self.l[3] = msg.data[3]      
    self.l[4] = msg.data[4]      
    self.l[5] = msg.data[5]      
    self.l[6] = msg.data[6]      
    self.l[7] = msg.data[7]      
    self.l[8] = msg.data[8]      

  def cur_length_callback(self, msg):
    self.l_cur[0] = msg.data[0]
    self.l_cur[1] = msg.data[1]
    self.l_cur[2] = msg.data[2]   
    self.l_cur[3] = msg.data[3]      
    self.l_cur[4] = msg.data[4]      
    self.l_cur[5] = msg.data[5]      
    self.l_cur[6] = msg.data[6]      
    self.l_cur[7] = msg.data[7]      
    self.l_cur[8] = msg.data[8]      

if __name__ == '__main__':
    try:
        pressure_producer()
    except rospy.ROSInterruptException: pass
