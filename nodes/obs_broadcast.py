#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import rospy
from geometry_msgs.msg import PoseArray
import tf
			
class obs_broadcast(object):
  def __init__(self):
    rospy.init_node('obs_broadcast')
    self.flag = rospy.get_param('/exp')
    obs_sub = rospy.Subscriber('obs_pose', PoseArray, self.get_obs)
    self.ref_frame = "world"
    if(self.flag==1):
      self.ref_frame = "base"
    #self.ref_frame = "tool0"
    rospy.spin()
    
  def get_obs(self,msg):
    obs = tf.TransformBroadcaster()
    obs_num = rospy.get_param('/obs_num')
    for i in range(0, obs_num):
      obs.sendTransform((msg.poses[i].position.x,msg.poses[i].position.y,msg.poses[i].position.z),
			(msg.poses[i].orientation.w, msg.poses[i].orientation.x, msg.poses[i].orientation.y, msg.poses[i].orientation.z),
			rospy.Time.now(),
			"obs"+str(i),
			self.ref_frame)
        
if __name__ == '__main__':
    try:
        obs_broadcast()
    except rospy.ROSInterruptException: pass
