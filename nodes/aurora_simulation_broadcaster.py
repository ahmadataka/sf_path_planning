#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import rospy
from geometry_msgs.msg import Pose
import tf
import random

def handle_pose(msg):
    path_frame = tf.TransformBroadcaster()
    path_frame.sendTransform((msg.position.x+random.uniform(0.0, 0.001),msg.position.y+random.uniform(0.0, 0.001),msg.position.z+random.uniform(0.0, 0.001)),
                     (0.0, 0.0, 0.0, 1.0),
                     rospy.Time.now(),
                     "aurora_marker1_sim",
                     "base")
    
def path_broadcaster():
    rospy.init_node('aurora_broadcaster')
    
    tip_sub = rospy.Subscriber('path', Pose, handle_pose)
        
    rospy.spin()
        
if __name__ == '__main__':
    try:
        path_broadcaster()
    except rospy.ROSInterruptException: pass
