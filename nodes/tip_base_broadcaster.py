#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import rospy
from geometry_msgs.msg import Pose
import tf
import random

def handle_pose(msg):
    path_frame = tf.TransformBroadcaster()
    path_frame.sendTransform((msg.position.x,msg.position.y,msg.position.z),
                     (0.0, 0.0, 0.0, 1.0),
                     rospy.Time.now(),
                     "Marker1_base",
                     "base")
    
def path_broadcaster():
    rospy.init_node('tip_base_broadcaster')
    
    tip_sub = rospy.Subscriber('tip_pose', Pose, handle_pose)
        
    rospy.spin()
        
if __name__ == '__main__':
    try:
        path_broadcaster()
    except rospy.ROSInterruptException: pass
