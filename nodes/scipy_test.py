#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import math
import rospy
from sympy import *
from scipy.integrate import ode
    
def deriv(t,y):  # return derivatives of the array y
    dydt = cos(t)
    return dydt

def scipy_test():
    y0, t0 = 0, 0
    #r = ode(deriv).set_integrator('zvode', method='bdf')
    r = ode(deriv).set_integrator('dopri5')
    r.set_initial_value(y0, t0)
    
    t1 = 2*pi
    dt = 0.02
    while r.successful() and r.t < t1:
      r.integrate(r.t+dt)
      #print("%g %g"%(r.t, r.y))
      print(r.t)
      print(r.y)
            
if __name__ == '__main__':
    try:
        scipy_test()
    except rospy.ROSInterruptException: pass
