#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import math
import rospy
import tf
from visualization_msgs.msg import Marker
from sympy import *

def sf_broadcaster():
    rospy.init_node('sf_broadcaster')
    x, y, z = symbols('x y z')
    
    marker = Marker()
    marker_pub = rospy.Publisher('soft_marker', Marker)
    
    r = rospy.Rate(1)
    marker.type = marker.CUBE
    while not rospy.is_shutdown():
	marker.header.frame_id = "/my_frame"
	marker.header.stamp = rospy.Time.now()
	marker.ns = "basic_shapes"
	marker.id = 0
        
        marker.action = marker.ADD
        marker.pose.position.x = 0;
	marker.pose.position.y = 0;
	marker.pose.position.z = 0;
	marker.pose.orientation.x = 0.0;
	marker.pose.orientation.y = 0.0;
	marker.pose.orientation.z = 0.0;
	marker.pose.orientation.w = 1.0;
        marker.scale.x = 1.0;
	marker.scale.y = 1.0;
	marker.scale.z = 1.0;
	marker.color.r = 0.0;
	marker.color.g = 1.0;
	marker.color.b = 0.0;
	marker.color.a = 1.0;
	marker.lifetime = rospy.Duration();
        marker_pub.publish(marker)
	
	if marker.type == marker.CUBE:
	  marker.type = marker.SPHERE
	elif marker.type == marker.SPHERE:
	  marker.type = marker.ARROW
	elif marker.type == marker.ARROW:
	  marker.type = marker.CYLINDER
	else:
	  marker.type = marker.CUBE
        expr = x+5
        expr2 = (x+y)**2
        expr3 = x**2 + 2*x*y + y**2
        expr4 =  x + Rational(1, 2)
        expr5 = x**3 + 4*x*y - z
	expr6 = sqrt(2)
	expr7 = cos(2*x)
	value = expr7.subs(x,pi)
	
	expr8 = x/y
	expr9 = atan2(-3,2)
        
        expr10 = exp(sin(x))
	
	M = Matrix([[1, 2, 3], [3, 2, 1]])
	N = Matrix([0, 1, 1]) 
	O = Matrix([[0,1,1]])
        #print(expr)
        #print(expr.subs(x,1))
        #print(expr2)
        #print(simplify(expr2))
        #print(expr2 == expr3)
        #print(simplify(expr2 - expr3))
        #print(expr4)
        #print(expand(expr2))
        #print(expr5.subs([(x, 2), (y, 4), (z, 0)]))
        #print(expr6.evalf())
        #print(expand_trig(expr7))
        #print(value.evalf())
        #print(trigsimp(sin(x)**2+cos(x)**2))
        #print(factorial(5))
        #print(expr8)
        #print(expr9.evalf())
        #print(diff(exp(x**2), x))
        #print(diff(exp(x**2)+y**3, y))
        #print(expr10.series(x, 0, 4))
        #print "ATAKA\n"
	#print(M)
	#print(M*N)
	#print(N.shape)
	#print(O.shape)
	#print(N)
	#print(O.T)
	#print(M.row(0).col(1))
	#print(eye(3))
	#print(eye(3).det())
	#print(eye(3)**-1)
        r.sleep()
        #rospy.spin()
        
if __name__ == '__main__':
    try:
        sf_broadcaster()
    except rospy.ROSInterruptException: pass
