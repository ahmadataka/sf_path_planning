#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import math
import rospy
from geometry_msgs.msg import Vector3
import numpy as np
from sympy import *

speed_goal = Vector3()
speed_obs = Vector3()

def find_magnitude(input1, input2):
  magnitude = math.sqrt(input1 ** 2 + input2 ** 2)
  return magnitude

def python_test():
    rospy.init_node('python_test')
    x, y, z = symbols('x y z')
    
    speed_total = Vector3()
    speed_goal.x = 3;
    speed_goal.y = 4;
    pub = rospy.Publisher('python_topic', Vector3)
    
    r = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
	speed_total.x = speed_goal.x + speed_obs.x
	speed_total.y = speed_goal.y + speed_obs.y
        linear = find_magnitude(speed_total.x, speed_total.y)
        angular = math.atan2(speed_total.y, speed_total.x)
    	#linear = 1
	#angular = 0.5
        pub.publish(speed_total)
        expr = x+5
        expr2 = (x+y)**2
        expr3 = x**2 + 2*x*y + y**2
        expr4 =  x + Rational(1, 2)
        expr5 = x**3 + 4*x*y - z
	expr6 = sqrt(2)
	expr7 = cos(2*x)
	value = expr7.subs(x,pi)
	
	expr8 = x/y
	expr9 = atan2(-3,2)
        
        expr10 = exp(sin(x))
	
	M = Matrix([[1, 2, 3],
		    [3, 4, 6]])
		    
	N = Matrix([0, 1, 1]) 
	O = Matrix([[0,1,1]])
	P = Matrix([[1, 2],
		    [3, 4],
		    [5, 6]])
	Q = Matrix([
		    [-0.999039998034306*sqrt(3) + 4.31881407547729, -0.00565812089119076 + 1.99807999606861*sqrt(3), -4.33013031725967 - 0.999039998034306*sqrt(3),    2.57113112672423 + 1.49769527788598*sqrt(3),   -2.99539055577197*sqrt(3) - 0.016967839091263,   -2.60506680490676 + 1.49769527788598*sqrt(3),  -1.74268627741132 - 0.00115185155078313*sqrt(3), -0.0113032021391871 + 0.00230370310156627*sqrt(3),  -0.00115185155078313*sqrt(3) + 1.72007987313295],
		    [ -7.48040540717027 - 1.73038726820368*sqrt(3),   0.0098001528589093 + 3.46077453640737*sqrt(3),  -1.73038726820368*sqrt(3) + 7.50000571288809,  -0.286236397344225*sqrt(3) + 4.51208564617187, 0.00978885368290236 + 0.572472794688449*sqrt(3),  -4.49250793880606 - 0.286236397344225*sqrt(3),  -0.0034581970865197 + 0.577127180123588*sqrt(3),   -1.15425436024718*sqrt(3) + 1.30274884751036e-8,  0.00345822314149665 + 0.577127180123588*sqrt(3)],
		    [                            0.646472713886671,                               0.333077080777949,                            0.0196814476692276, 0.0678061426050238*sqrt(3) + 0.410726839489328,  -0.135612285210048*sqrt(3) + 0.332693056506723, 0.0678061426050238*sqrt(3) + 0.254659273524118, -3.91114035314774e-5*sqrt(3) + 0.293938781836432,    7.82228070629548e-5*sqrt(3) + 0.33307752354225, -3.91114035314774e-5*sqrt(3) + 0.372216265248068]])

	#print("1")
	#print(np.linalg.pinv(P))
	#print(Matrix(np.linalg.pinv(P)))
	#print(np.linalg.pinv(P)*P)
	#print("2")
	#print(np.linalg.pinv(M))
	#print(M*np.linalg.pinv(M))
	print("1")
	jumper = Q.evalf()
	right_inv = jumper.T * (jumper*jumper.T).inv()
	check_mat = jumper*jumper.T
	print(check_mat.det())
	print(right_inv)
	print("2")
	check_mat2 = jumper.T*jumper
	print(check_mat2.det())
	#left_inv = (Q.evalf().T*Q).inv()*Q.evalf().T
	
	#print("3")
	#print(left_inv.evalf(4))
	#print(left_inv*Q)
	#right_inv = Q.T * (Q*Q.T).inv()
	#print("4")
	#print(right_inv.evalf(4))
	#print(Q*right_inv)
	print("5")
	print(np.linalg.pinv(Q.evalf()))
	#print(Q.evalf()*np.linalg.pinv(Q.evalf()))
	#print(np.linalg.pinv(Q.evalf())*Q.evalf())

        #print(expr)
        #print(expr.subs(x,1))
        #print(expr2)
        #print(simplify(expr2))
        #print(expr2 == expr3)
        #print(simplify(expr2 - expr3))
        #print(expr4)
        #print(expand(expr2))
        #print(expr5.subs([(x, 2), (y, 4), (z, 0)]))
        #print(expr6.evalf())
        #print(expand_trig(expr7))
        #print(value.evalf())
        #print(trigsimp(sin(x)**2+cos(x)**2))
        #print(factorial(5))
        #print(expr8)
        #print(expr9.evalf())
        #print(diff(exp(x**2), x))
        #print(diff(exp(x**2)+y**3, y))
        #print(expr10.series(x, 0, 4))
        #print "ATAKA\n"
	#print(M)
	#print(M*N)
	#print(N.shape)
	#print(O.shape)
	#print(N)
	#print(O.T)
	#print(M.row(0).col(1))
	#print(M[0,1])
	#print(eye(3))
	#print(eye(3).det())
	#print(eye(3)**-1)
        r.sleep()
        #rospy.spin()
        
if __name__ == '__main__':
    try:
        python_test()
    except rospy.ROSInterruptException: pass
