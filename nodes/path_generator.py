#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import math
import rospy
from geometry_msgs.msg import Pose
from sensor_msgs.msg import Joy
from std_msgs.msg import Float64MultiArray
import tf

class path_generator(object):
  def __init__(self):
    rospy.init_node('path_generator')
    #print "a"
    self.goal_pose = Pose()
    #We should use parameter value for this initial goal
    #self.goal = [0.0115, 0.0121, 3.03]
    #self.goal = [-0.9465, 0.5488, 2.7370]
    #self.goal = [ -0.0104626115946, 0.0363025846822, 0.36351393246]
    self.goal = [ -0.0104626, 0.0363026, 0.363514]
    # self.goal = [0.2944086776927468, -0.039215662698727255, 1.0206535062911635]

    self.euler = [0.2944086776927468, -0.039215662698727255, 1.0206535062911635]
    # self.euler = [math.pi/2.0, 0.0, 0.0]
    quat = tf.transformations.quaternion_from_euler(self.euler[2], self.euler[1], self.euler[0])

    self.goal_pose.position.x = self.goal[0]
    self.goal_pose.position.y = self.goal[1]
    self.goal_pose.position.z = self.goal[2]
    self.goal_pose.orientation.w = quat[3]
    self.goal_pose.orientation.x = quat[0]
    self.goal_pose.orientation.y = quat[1]
    self.goal_pose.orientation.z = quat[2]
    self.rec_vel = rospy.Subscriber('/joy', Joy, self.ps3_callback)
    goal_pub = rospy.Publisher('goal_pose', Pose, queue_size = 10)
    euler_pub = rospy.Publisher('euler_goal', Float64MultiArray, queue_size = 10)

    r = rospy.Rate(40)
    self.delta_x = 0.001
    self.delta_y = 0.001
    self.delta_z = 0.001
    self.flag_move = 0
    while not rospy.is_shutdown():
        goal_pub.publish(self.goal_pose)

        euler_sent = Float64MultiArray()
        euler_sent.data = []
        for i in range(0,3):
            euler_sent.data.append(self.euler[i])
        euler_pub.publish(euler_sent)
	#self.goal_pose.position.x += 0.0*self.delta_x
	#self.goal_pose.position.y += 0.0*self.delta_y
	#self.goal_pose.position.z += 0.2*self.delta_z

        rospy.set_param("/moving", self.flag_move)
        r.sleep()

        #rospy.spin()

  def ps3_callback(self,msg):
    self.goal_pose.position.x += msg.axes[0]*self.delta_x
    self.goal_pose.position.y += msg.axes[1]*self.delta_y
    self.goal_pose.position.z += msg.axes[3]*self.delta_z
    # self.euler[0] += msg.axes[0]*self.delta_x
    # self.euler[1] += msg.axes[1]*self.delta_y
    # self.euler[2] += msg.axes[3]*self.delta_z
    # quat = tf.transformations.quaternion_from_euler(self.euler[2], self.euler[1], self.euler[0])
    # self.goal_pose.orientation.w = quat[3]
    # self.goal_pose.orientation.x = quat[0]
    # self.goal_pose.orientation.y = quat[1]
    # self.goal_pose.orientation.z = quat[2]
    if(msg.axes[2] == 1):
      self.flag_move = 1
    elif(msg.axes[2] == -1):
      self.flag_move = 0


if __name__ == '__main__':
    try:
        path_generator()
    except rospy.ROSInterruptException: pass
