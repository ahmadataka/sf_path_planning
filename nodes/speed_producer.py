#!/usr/bin/env python
import roslib
roslib.load_manifest('sf_path_planning')
import rospy
import math
import tf
from geometry_msgs.msg import PoseArray
from geometry_msgs.msg import Pose
from geometry_msgs.msg import Vector3
from continuum_manipulator.msg import Vector3Array

if __name__ == '__main__':
    rospy.init_node('speed_producer')
    segment_num = 3
    obs_num = rospy.get_param('/obs_num')
    num_per_seg = 5
    flag_sensor = 0
    flag_obs = 0
    listener = tf.TransformListener()
    sensor_pose = PoseArray()
    obs_pose = PoseArray()
    send_vel = Vector3Array()
    vel = rospy.Publisher('/twist', Vector3Array, queue_size = 10)
    #turtle_dist = rospy.Publisher('/%s/dist_to_goal' %turtle_name, Vector3, queue_size = 10)

    # Init saved distance
    distance_saved = []
    zeros = Vector3()
    zeros.x = 0
    zeros.y = 0
    zeros.z = 0
    for i in range(0, num_per_seg*segment_num*obs_num):
      distance_saved.append(zeros)

    freq = 40.0
    rate = rospy.Rate(freq)

    while not rospy.is_shutdown():

      # Listen to sensor
      sensor_pose.poses = []
      for i in range(0,num_per_seg*segment_num):
        try:
            (trans,rot) = listener.lookupTransform('/world', '/sensor'+str(i), rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue
	sensor = Pose()
	sensor.position.x = trans[0]
	sensor.position.y = trans[1]
	sensor.position.z = trans[2]
	sensor_pose.poses.append(sensor)
	flag_sensor = 1

      # Listen to obstacle
      obs_pose.poses = []
      for i in range(0,obs_num):
	try:
            (trans,rot) = listener.lookupTransform('/world', '/obs'+str(i), rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue
	sensor = Pose()
	sensor.position.x = trans[0]
	sensor.position.y = trans[1]
	sensor.position.z = trans[2]
	obs_pose.poses.append(sensor)
	flag_obs = 1


      if((flag_obs == 1) and (flag_sensor == 1)):
	# Calculate sensor-obstacle distance
	distance = []
	for i in range(0, num_per_seg*segment_num):
	  for j in range(0,obs_num):
	    dist = Vector3()
	    dist.x = sensor_pose.poses[i].position.x - obs_pose.poses[j].position.x
	    dist.y = sensor_pose.poses[i].position.y - obs_pose.poses[j].position.y
	    dist.z = sensor_pose.poses[i].position.z - obs_pose.poses[j].position.z
	    distance.append(dist)
	    #print distance[0].x

	# Estimate speed
	speed = []
	for i in range(0, num_per_seg*segment_num*obs_num):
	  sp = Vector3()
	  sp.x = (distance[i].x - distance_saved[i].x)*freq
	  sp.y = (distance[i].y - distance_saved[i].y)*freq
	  sp.z = (distance[i].z - distance_saved[i].z)*freq
	  speed.append(sp)

	# Save distance
	distance_saved = []
	for i in range(0, num_per_seg*segment_num*obs_num):
	  save = Vector3()
	  save.x = distance[i].x
	  save.y = distance[i].y
	  save.z = distance[i].z
	  distance_saved.append(save)
	print speed

	# Save to send
	send_vel.stamp = rospy.Time.now()
	send_vel.vector = []
	send_vel.name = []
	ind = 0
	for i in range(0, num_per_seg*segment_num):
	  for j in range(0, obs_num):
	    send_vel.vector.append(speed[ind])
	    send_vel.name.append('sensor'+str(i)+'_'+'obs'+str(j))
	    ind += 1

      #turtle_vel.publish(vel_msg)
      vel.publish(send_vel)
      #turtle_dist.publish(dist_msg)
      #print(obs_pose)
      #print(sensor_pose)

      rate.sleep()
