#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import math
import rospy
import numpy as np
from sympy import *
from scipy.integrate import ode
from geometry_msgs.msg import Pose
from std_msgs.msg import Float64MultiArray

k, p, s = symbols('k p s')
l1, l2, l3 = symbols('l1 l2 l3')

def l_to_phi(l_1, l_2, l_3):
    #phi_out = atan2((sqrt(3)*(l_2+l_3-2*l_1)),(3*(l_2-l_3)))
    phi_out = atan((sqrt(3)*(l_2+l_3-2*l_1))/(3*(l_2-l_3)))
    #print(trigsimp(phi_out))
    return phi_out.evalf()

class fuzzy_control(object):
  def __init__(self):
    rospy.init_node('fuzzy_control')
    self.feedback = rospy.get_param('/feedback')
    if(self.feedback == 0):
      self.K1 = 0.0
    else:
      self.K1 = 0.01
      
    self.d = 0.0134
    self.flag_print = 0
    self.initialize_Jg()
    
    self.initialize_Jh()

    #self.k1_ex = 2*(sqrt(pow(l1,2)+pow(l2,2)+pow(l3,2)-l1*l2-l1*l3-l2*l3))/(self.d*(l1+l2+l3))
    self.k1_ex = 2*(pow(pow(l1,2)+pow(l2,2)+pow(l3,2)-l1*l2-l1*l3-l2*l3,0.5))/(self.d*(l1+l2+l3))
    self.s1_ex = (l1+l2+l3)/3
    self.k_out = lambdify((l1, l2, l3), self.k1_ex, "numpy")
    self.s_out = lambdify((l1, l2, l3), self.s1_ex, "numpy")
    
    self.e_vector = zeros(3,1)
    self.path_vector = zeros(3,1)
    self.tip = zeros(3,1)
    self.v = zeros(3,1)
    self.path_vel = Matrix([[2.5/10000.0], [7.5/10000.0], [2.6667/10000.0]])
    self.vel_send = Float64MultiArray()
    #print(self.F[1].tolist()[0][0])
    #print(self.tip.tolist()[0][0])
    vel_pub = rospy.Publisher('motor_speed', Float64MultiArray)
    pub = rospy.Publisher('/configuration_space', Float64MultiArray)
    length_pub = rospy.Publisher('/length', Float64MultiArray)
    send_pub = rospy.Publisher('/record_data', sf_msg)

    self.rec_vel = rospy.Subscriber('/field', Float64MultiArray, self.vel_callback)
    self.rec_goal = rospy.Subscriber('/goal_pose', Pose, self.goal_callback)
    self.rec_tip = rospy.Subscriber('/tip_pose', PoseArray, self.tip_callback)
    self.rec_obs = rospy.Subscriber('/obs_pose', PoseArray, self.obspose_callback)
    self.rec_vel_obs = rospy.Subscriber('/obs_field_configuration_space', Float64MultiArray, self.obs_callback)
    error_sub = rospy.Subscriber('error', Pose, self.get_error)
    ref_sub = rospy.Subscriber('ref_speed', Float64MultiArray, self.get_ref)
    #tip_to_goal = tf.TransformListener()

    #Initial Condition. The states are the length of the tendos.
    y0, t0 = [0.1232, 0.1203, 0.1123], 0.0
    self.l = y0
    self.k_val = self.k_out(y0[0],y0[1],y0[2])
    self.p_val = l_to_phi(y0[0],y0[1],y0[2])
    self.s_val = self.s_out(y0[0],y0[1],y0[2])
    
    #r = ode(self.deriv).set_integrator('dopri5')
    #r.set_initial_value(y0, t0)
    self.dt = 1.0/40.0
    
    rate = rospy.Rate(40.0)
    
    while not rospy.is_shutdown():
        self.vel_send.data = []
        self.assign_Jg()
        self.assign_Jh()
        self.integral_length()
        #if r.successful():
	  #r.integrate(r.t+self.dt)
	  #self.update(r.y)
        
        for i in range(0,3):
	  self.vel_send.data.append(self.v[i])
	#print(self.vel_send)
	vel_pub.publish(self.vel_send)
	
	#print(self.K1)
        #rate.sleep()
        
        #rospy.spin()

  def get_error(self,msg):
    self.e_vector = Matrix([[msg.position.x],[msg.position.y],[msg.position.z]])
    
  def get_ref(self,msg):
    self.path_vel = Matrix([[msg.data[0]],[msg.data[1]],[msg.data[2]]])
    
  
  def integral_length(self):
    self.inverse_jacobian()
    self.l = [self.l[0]+self.v[0]*self.dt, self.l[1]+self.v[1]*self.dt, self.l[2]+self.v[2]*self.dt]
    #print(self.l)
    self.k_val = self.k_out(self.l[0],self.l[1],self.l[2])
    self.p_val = l_to_phi(self.l[0],self.l[1],self.l[2])
    self.s_val = self.s_out(self.l[0],self.l[1],self.l[2])
    
  def deriv(self,t,y):  # return derivatives of the array y
    self.inverse_jacobian()
    dydt_mat = Matrix([self.v[0], self.v[1], self.v[2]])
    
    dydt = dydt_mat.tolist()
    #print(dydt)
    return dydt

  def assign_Jg(self):
    self.Jg_val = self.Jg(self.l[0], self.l[1], self.l[2])
    #print(self.Jg_val)
    
  def assign_Jh(self):
    self.Jh_val = self.Jh(self.k_val, self.p_val, self.s_val)
    #print(self.Jh_val.det())
    
  def initialize_Jh(self):
    self.Jh_ex = Matrix([[cos(p)*(k*s*sin(k*s)+cos(k*s)-1)/pow(k,2), -sin(p)*(1-cos(k*s))/k, cos(p)*sin(k*s)],
			[sin(p)*(k*s*sin(k*s)+cos(k*s)-1)/pow(k,2), cos(p)*(1-cos(k*s))/k, sin(p)*sin(k*s)],
			[(-sin(k*s)+k*s*cos(k*s))/pow(k,2), 0, cos(k*s)]])
    self.Jh = lambdify((k, p, s), self.Jh_ex, "sympy")

  def initialize_Jg(self):
    l_sum = l1+l2+l3
    l_sqrt = sqrt(pow(l1,2)+pow(l2,2)+pow(l3,2)-l1*l2-l1*l3-l2*l3)
    self.Jg_ex = Matrix([[3*(l1*l2+l1*l3-pow(l2,2)-pow(l3,2))/(self.d*pow(l_sum,2)*l_sqrt), -3*(pow(l1,2)-l1*l2-l2*l3+pow(l3,2))/(self.d*pow(l_sum,2)*l_sqrt), -3*(pow(l1,2)-l1*l3-l2*l3+pow(l2,2))/(self.d*pow(l_sum,2)*l_sqrt)],
			[sqrt(3)*(l3-l2)/(2*pow(l_sqrt,2)), sqrt(3)*(l1-l3)/(2*pow(l_sqrt,2)), sqrt(3)*(l2-l1)/(2*pow(l_sqrt,2))],
			[1.0/3.0, 1.0/3.0, 1.0/3.0]])
    #print(self.Jg_ex)
    self.Jg = lambdify((l1, l2, l3), self.Jg_ex, "sympy")
    
    
  def inverse_jacobian(self):
    Ju = self.Jh_val*self.Jg_val
    
    #if(self.flag_print == 0):
      #print("Jh")
      #print(self.Jh_val.evalf())
      #print("Jg")
      #print(self.Jg_val.evalf())
      #print("Ju")
      #print(Ju.evalf())
      #print("inverse Ju")
      #print(np.linalg.inv(Ju))
      #self.flag_print = 1
    
    #print(Ju.det())
    #self.v = Ju.inv()*(self.path_vel+self.K1*self.e_vector)
    #print(self.path_vel)
    self.v = np.linalg.inv(Ju)*(self.path_vel-self.K1*self.e_vector)
    
  def update(self,state):
    self.l = [state[0], state[1], state[2]]
    self.k_val = self.k_out(self.l[0],self.l[1],self.l[2])
    self.p_val = l_to_phi(self.l[0],self.l[1],self.l[2])
    self.s_val = self.s_out(self.l[0],self.l[1],self.l[2])

if __name__ == '__main__':
    try:
        fuzzy_control()
    except rospy.ROSInterruptException: pass