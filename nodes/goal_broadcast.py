#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import rospy
from geometry_msgs.msg import Pose
import tf

  
class goal_broadcast(object):
  def __init__(self):
    rospy.init_node('goal_broadcast')
    self.flag = rospy.get_param('/exp')
    goal_sub = rospy.Subscriber('goal_pose', Pose, self.get_goal)
    self.ref_frame = "world"
    if(self.flag==1):
      self.ref_frame = "base"
    #self.ref_frame = "tool0"
    rospy.spin()
    
  def get_goal(self,msg):
    goal = tf.TransformBroadcaster()
    
    goal.sendTransform((msg.position.x,msg.position.y,msg.position.z),
		      (0.0, 0.0, 0.0, 1.0),
		      rospy.Time.now(),
		      "goal",
		      self.ref_frame)

if __name__ == '__main__':
    try:
        goal_broadcast()
    except rospy.ROSInterruptException: pass
