#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import math
import rospy
from geometry_msgs.msg import Pose
from sensor_msgs.msg import Joy
from sf_msgs.msg import SfPressureDemand
class path_generator(object):
  def __init__(self):
    rospy.init_node('path_generator')
    #print "a"
    self.pressure_1 = SfPressureDemand()
    self.pressure_2 = SfPressureDemand()
    self.pre_1 = [ 10000.0, 10000.0, 10000.0]
    self.pre_2 = [ 10000.0, 10000.0, 10000.0]

    self.rec_vel = rospy.Subscriber('/joy', Joy, self.ps3_callback)
    goal_pub = rospy.Publisher('pressure_controller/segment_0', SfPressureDemand, queue_size = 10)
    goal2_pub = rospy.Publisher('pressure_controller/segment_1', SfPressureDemand, queue_size = 10)
    r = rospy.Rate(40)
    self.delta = 1000.0
    while not rospy.is_shutdown():
        self.pressure_1.pressures = []
        self.pressure_1.pressures.append(self.pre_1[0])
        self.pressure_1.pressures.append(self.pre_1[1])
        self.pressure_1.pressures.append(self.pre_1[2])
        self.pressure_2.pressures = []
        self.pressure_2.pressures.append(self.pre_2[0])
        self.pressure_2.pressures.append(self.pre_2[1])
        self.pressure_2.pressures.append(self.pre_2[2])

	goal_pub.publish(self.pressure_1)
	goal2_pub.publish(self.pressure_2)
        r.sleep()
        
        #rospy.spin()

  def ps3_callback(self,msg):
    self.pre_1[0] += msg.axes[0]*self.delta
    self.pre_1[1] += msg.axes[1]*self.delta
    self.pre_1[2] += msg.axes[3]*self.delta
    self.pre_2[0] += msg.axes[4]*self.delta
    self.pre_2[1] += msg.axes[5]*self.delta
    self.pre_2[2] += (msg.buttons[1]-msg.buttons[3])*self.delta
    	
if __name__ == '__main__':
    try:
        path_generator()
    except rospy.ROSInterruptException: pass
