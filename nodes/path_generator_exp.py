#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('soft_fuzzy')
import math
import rospy
from geometry_msgs.msg import Pose
from sympy import *
from std_msgs.msg import Float64MultiArray
from sympy.mpmath import norm

class path_generator(object):
  def __init__(self):
    rospy.init_node('path_generator')
    #print "a"
    path_point = Pose()
    self.vel_send = Float64MultiArray()
    #We should use parameter value for this initial goal
    #position = [0.00436, -0.01351, 0.11241]
    
    #position = [0.02, -0.02, 0.114]
    #position = [0.0190507, -0.0186357, 0.118665]
    position = Matrix([[-0.0104626115946], [0.0363025846822], [0.36351393246]])
    #final_position = Matrix([[-0.06], [0.24], [0.36351393246]])
    final_position = Matrix([[-0.011], [0.037], [1.6]])
    #final = [0.025, 0.025, 0.130]
    
    #duration = 100
    speed = 0.01
    duration = long(norm(final_position - position)/speed)
    #duration = 10.0
    #Ar = Matrix([[0, -pi.evalf()/80, 0],[pi.evalf()/80, 0, 0],[0, 0, 0]])
    #vel = Matrix([[5.0*1.0/10000.0], [5.0*1.0/10000.0], [5.0*7.5/10000.0]])
    vel = Matrix([[0.0], [0.0], [0.0]])
    #vel = (final_position - position)/float(duration)
    
    #vel = 0.1*Ar*position
    #for i in range(0,3):
      #vel[i] = (final[i]-position[i])/duration
    print(vel)
    path_pub = rospy.Publisher('goal_pose', Pose, queue_size = 10)
    move = 0
    frequency = 40.0
    r = rospy.Rate(frequency)
    delta = 1.0/frequency
    delay = 5
    begin = rospy.get_rostime()
    while not rospy.is_shutdown():
	self.vel_send.data = []
	for i in range(0,3):
	  position[i,0] = position[i,0] + vel[i,0]*delta
	path_point.position.x = position[0,0]
	path_point.position.y = position[1,0]
	path_point.position.z = position[2,0]
	path_pub.publish(path_point)
	for i in range(0,3):
	  self.vel_send.data.append(vel[i])
	
	#print(path_point)
        r.sleep()
        ending = rospy.get_rostime()
        current_duration = ending.secs - begin.secs
        #vel = 0.1*Ar*position
        #print(move)
        
        if((current_duration>delay) and (current_duration<=(duration+delay)) and (move == 0)):
	  vel = speed*(final_position - position)/norm(final_position - position)
	  move = 1
	  
        #elif(current_duration>(duration+delay)):
        #else:
	  #vel = Matrix([[0.0], [0.0], [0.0]])
        #rospy.spin()
    	
if __name__ == '__main__':
    try:
        path_generator()
    except rospy.ROSInterruptException: pass
