#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import math
import rospy
import numpy as np
from sympy import *
from scipy.integrate import ode
from std_msgs.msg import Float64MultiArray
from sf_path_planning.msg import sf_msg
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseArray

k1, k2, k3, p1, p2, p3, s1, s2, s3 = symbols('k1 k2 k3 p1 p2 p3 s1 s2 s3')
l11, l12, l13, l21, l22, l23, l31, l32, l33 = symbols('l11 l12 l13 l21 l22 l23 l31 l32 l33')

def homogeneous_transform_sym(segment_num):
    #TransMatrix = Matrix([[cos(p1)*cos(k1*s1), -sin(p1), cos(p1)*sin(k1*s1), cos(p1)*(1-cos(k1*s1))/k1],
			  #[sin(p1)*cos(k1*s1), cos(p1), sin(p1)*sin(k1*s1), sin(p1)*(1-cos(k1*s1))/k1],
			  #[-sin(k1*s1), 0, cos(k1*s1), sin(k1*s1)/k1],
			  #[0, 0, 0, 1]])
    Tb = eye(4)
      
    if (segment_num == 1):
      #TM = TransMatrix
      TM = Tb*Matrix([[cos(p1)*cos(k1*s1), -sin(p1), sin(k1*s1)*cos(p1), (-cos(k1*s1) + 1)*cos(p1)/k1], [sin(p1)*cos(k1*s1), cos(p1), sin(p1)*sin(k1*s1), (-cos(k1*s1) + 1)*sin(p1)/k1], [-sin(k1*s1), 0, cos(k1*s1), sin(k1*s1)/k1], [0, 0, 0, 1]])
    elif(segment_num == 2):
      #TM = TransMatrix*(TransMatrix.subs([(k1, k2),(p1, p2),(s1, s2)]))
      TM = Tb*Matrix([[-sin(p1)*sin(p2)*cos(k2*s2) - sin(k1*s1)*sin(k2*s2)*cos(p1) + cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2), -sin(p1)*cos(p2) - sin(p2)*cos(p1)*cos(k1*s1), -sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1), -(-cos(k2*s2) + 1)*sin(p1)*sin(p2)/k2 + (-cos(k2*s2) + 1)*cos(p1)*cos(p2)*cos(k1*s1)/k2 + sin(k1*s1)*sin(k2*s2)*cos(p1)/k2 + (-cos(k1*s1) + 1)*cos(p1)/k1], [-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2), -sin(p1)*sin(p2)*cos(k1*s1) + cos(p1)*cos(p2), sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1), (-cos(k2*s2) + 1)*sin(p1)*cos(p2)*cos(k1*s1)/k2 + (-cos(k2*s2) + 1)*sin(p2)*cos(p1)/k2 + sin(p1)*sin(k1*s1)*sin(k2*s2)/k2 + (-cos(k1*s1) + 1)*sin(p1)/k1], [-sin(k1*s1)*cos(p2)*cos(k2*s2) - sin(k2*s2)*cos(k1*s1), sin(p2)*sin(k1*s1), -sin(k1*s1)*sin(k2*s2)*cos(p2) + cos(k1*s1)*cos(k2*s2), -(-cos(k2*s2) + 1)*sin(k1*s1)*cos(p2)/k2 + sin(k2*s2)*cos(k1*s1)/k2 + sin(k1*s1)/k1], [0, 0, 0, 1]])
    else:
      #TM = TransMatrix*(TransMatrix.subs([(k1, k2),(p1, p2),(s1, s2)]))*(TransMatrix.subs([(k1, k3),(p1, p3),(s1, s3)]))
      TM = Tb*Matrix([[(-sin(p1)*cos(p2) - sin(p2)*cos(p1)*cos(k1*s1))*sin(p3)*cos(k3*s3) - (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*sin(k3*s3) + (-sin(p1)*sin(p2)*cos(k2*s2) - sin(k1*s1)*sin(k2*s2)*cos(p1) + cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*cos(p3)*cos(k3*s3), (-sin(p1)*cos(p2) - sin(p2)*cos(p1)*cos(k1*s1))*cos(p3) - (-sin(p1)*sin(p2)*cos(k2*s2) - sin(k1*s1)*sin(k2*s2)*cos(p1) + cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(p3), (-sin(p1)*cos(p2) - sin(p2)*cos(p1)*cos(k1*s1))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(k3*s3) + (-sin(p1)*sin(p2)*cos(k2*s2) - sin(k1*s1)*sin(k2*s2)*cos(p1) + cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3)*cos(p3), (-sin(p1)*cos(p2) - sin(p2)*cos(p1)*cos(k1*s1))*(-cos(k3*s3) + 1)*sin(p3)/k3 + (-cos(k3*s3) + 1)*(-sin(p1)*sin(p2)*cos(k2*s2) - sin(k1*s1)*sin(k2*s2)*cos(p1) + cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*cos(p3)/k3 + (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*sin(k3*s3)/k3 - (-cos(k2*s2) + 1)*sin(p1)*sin(p2)/k2 + (-cos(k2*s2) + 1)*cos(p1)*cos(p2)*cos(k1*s1)/k2 + sin(k1*s1)*sin(k2*s2)*cos(p1)/k2 + (-cos(k1*s1) + 1)*cos(p1)/k1], [(-sin(p1)*sin(p2)*cos(k1*s1) + cos(p1)*cos(p2))*sin(p3)*cos(k3*s3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*cos(p3)*cos(k3*s3) - (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*sin(k3*s3), (-sin(p1)*sin(p2)*cos(k1*s1) + cos(p1)*cos(p2))*cos(p3) - (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(p3), (-sin(p1)*sin(p2)*cos(k1*s1) + cos(p1)*cos(p2))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3)*cos(p3) + (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(k3*s3), (-sin(p1)*sin(p2)*cos(k1*s1) + cos(p1)*cos(p2))*(-cos(k3*s3) + 1)*sin(p3)/k3 + (-cos(k3*s3) + 1)*(-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*cos(p3)/k3 + (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*sin(k3*s3)/k3 + (-cos(k2*s2) + 1)*sin(p1)*cos(p2)*cos(k1*s1)/k2 + (-cos(k2*s2) + 1)*sin(p2)*cos(p1)/k2 + sin(p1)*sin(k1*s1)*sin(k2*s2)/k2 + (-cos(k1*s1) + 1)*sin(p1)/k1], [-(-sin(k1*s1)*sin(k2*s2)*cos(p2) + cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (-sin(k1*s1)*cos(p2)*cos(k2*s2) - sin(k2*s2)*cos(k1*s1))*cos(p3)*cos(k3*s3) + sin(p2)*sin(p3)*sin(k1*s1)*cos(k3*s3), -(-sin(k1*s1)*cos(p2)*cos(k2*s2) - sin(k2*s2)*cos(k1*s1))*sin(p3) + sin(p2)*sin(k1*s1)*cos(p3), (-sin(k1*s1)*sin(k2*s2)*cos(p2) + cos(k1*s1)*cos(k2*s2))*cos(k3*s3) + (-sin(k1*s1)*cos(p2)*cos(k2*s2) - sin(k2*s2)*cos(k1*s1))*sin(k3*s3)*cos(p3) + sin(p2)*sin(p3)*sin(k1*s1)*sin(k3*s3), (-sin(k1*s1)*sin(k2*s2)*cos(p2) + cos(k1*s1)*cos(k2*s2))*sin(k3*s3)/k3 + (-sin(k1*s1)*cos(p2)*cos(k2*s2) - sin(k2*s2)*cos(k1*s1))*(-cos(k3*s3) + 1)*cos(p3)/k3 + (-cos(k3*s3) + 1)*sin(p2)*sin(p3)*sin(k1*s1)/k3 - (-cos(k2*s2) + 1)*sin(k1*s1)*cos(p2)/k2 + sin(k2*s2)*cos(k1*s1)/k2 + sin(k1*s1)/k1], [0, 0, 0, 1]])
    
    return TM

def homogeneous_transform(kappa_sym, phi_sym, s_sym):
    T = Matrix([[cos(phi_sym)*cos(kappa_sym*s_sym), -sin(phi_sym), cos(phi_sym)*sin(kappa_sym*s_sym), cos(phi_sym)*(1-cos(kappa_sym*s_sym))/kappa_sym],
		[sin(phi_sym)*cos(kappa_sym*s_sym), cos(phi_sym), sin(phi_sym)*sin(kappa_sym*s_sym), sin(phi_sym)*(1-cos(kappa_sym*s_sym))/kappa_sym],
		[-sin(kappa_sym*s_sym), 0, cos(kappa_sym*s_sym), sin(kappa_sym*s_sym)/kappa_sym],
		[0, 0, 0, 1]])
    return T
    
def homo_to_pose(T):
    position = Matrix([[T[0,3]],[T[1,3]],[T[2,3]]])
    #print(position)
    return position
    
def l_to_phi(l1, l2, l3):
    #phi_out = atan2((sqrt(3)*(l2+l3-2*l1)),(3*(l2-l3)))
    y_atan = (sqrt(3)*(l2+l3-2*l1))
    x_atan = (3*(l2-l3))
    y_final = float(y_atan.evalf())
    x_final = float(x_atan)
    
    phi_out = np.arctan2(y_final,x_final)
    #phi_out = atan((sqrt(3)*(l2+l3-2*l1))/(3*(l2-l3)))
    #print(trigsimp(phi_out))
    #return phi_out.evalf()
    return phi_out
      
class observer(object):
  
  def __init__(self):
    rospy.init_node('observer')
    
    self.JacVelSym = zeros(3,9)
    self.JacVelSymBot = zeros(3,9)
    self.JacVelSymMid = zeros(3,9)
    self.JacVe=lSymUp = zeros(3,9)
    self.JacOmegaSym = zeros(3,9)
    self.Jw = zeros(3,9)
    self.Jv = zeros(3,9)
    self.v = zeros(9,1)
    self.Lo = 0.1200
    d = 0.0134
    self.p1_ex = atan((sqrt(3)*(l12+l13-2*l11))/(3*(l12-l13)))
    self.k1_ex = 2*(sqrt(pow(l11,2)+pow(l12,2)+pow(l13,2)-l11*l12-l11*l13-l12*l13))/(d*(3*self.Lo+l11+l12+l13))
    self.s1_ex = (3*self.Lo+l11+l12+l13)/3
    self.p2_ex = atan((sqrt(3)*(l22+l23-2*l21))/(3*(l22-l23)))
    self.k2_ex = 2*(sqrt(pow(l21,2)+pow(l22,2)+pow(l23,2)-l21*l22-l21*l23-l22*l23))/(d*(3*self.Lo+l21+l22+l23))
    self.s2_ex = (3*self.Lo+l21+l22+l23)/3
    self.p3_ex = atan((sqrt(3)*(l32+l33-2*l31))/(3*(l32-l33)))
    self.k3_ex = 2*(sqrt(pow(l31,2)+pow(l32,2)+pow(l33,2)-l31*l32-l31*l33-l32*l33))/(d*(3*self.Lo+l31+l32+l33))
    self.s3_ex = (3*self.Lo+l31+l32+l33)/3
    
    self.k_out = lambdify((l11, l12, l13), self.k1_ex, "sympy")
    self.s_out = lambdify((l11, l12, l13), self.s1_ex, "sympy")
    
    #self.init_jacobian_velocity_fast(3)
    self.init_jacobian_velocity()
    self.init_jacobian_kps_l()
    
    speed_sub = rospy.Subscriber('motor_speed', Float64MultiArray, self.get_speed)
    pose_sub = rospy.Subscriber('tip_pose', PoseArray, self.get_pose)
    state_pub = rospy.Publisher('state', Float64MultiArray, queue_size = 10)
    
    self.tip_pose = zeros(9,1)
    self.state_send = Float64MultiArray()
    
    # Kalman initialization
    self.mu, self.sigma = 0, 0.001
    self.state = Matrix([[self.Lo+0.01], [self.Lo-0.01], [self.Lo], [self.Lo+0.01], [self.Lo-0.01], [self.Lo], [self.Lo+0.01], [self.Lo-0.01], [self.Lo]])
    self.P = eye(9)
    self.Q = (self.sigma**2)*eye(9)
    self.R = (self.sigma**2)*eye(9)
    #self.Q = zeros(6,6)
    #self.R = zeros(3,3)
    
    self.dt = 1.0/40.0
    
    rate = rospy.Rate(40.0)

    while (not rospy.is_shutdown()):
      self.jacobian_kps_l()
      self.integral_length()
      self.transform_to_sent()
      state_pub.publish(self.state_send)
      
  def get_pose(self,msg):
    self.tip_pose = Matrix([[msg.poses[0].position.x],[msg.poses[0].position.y],[msg.poses[0].position.z], [msg.poses[1].position.x],[msg.poses[1].position.y],[msg.poses[1].position.z], [msg.poses[2].position.x],[msg.poses[2].position.y],[msg.poses[2].position.z]])  
    #print(self.tip_pose)
  
  def get_speed(self,msg):
    self.v = Matrix([[msg.data[0]],[msg.data[1]],[msg.data[2]], [msg.data[3]],[msg.data[4]],[msg.data[5]], [msg.data[6]],[msg.data[7]],[msg.data[8]]])

  def integral_length(self):
    self.noise = np.random.normal(self.mu, self.sigma, 3)
    self.inverse_jacobian()
        
    self.u = self.v
    #print(self.u)
    #print("A")
    
    ## Extended Kalman Filter
    self.P_dum = self.A*self.P*self.A.T + self.Q
    self.P_dum = self.P_dum.evalf()
    #print(self.P_dum.cols)
    #print(self.P_dum)
    ##self.Kalman = self.P_dum*self.C.T*np.linalg.inv(self.C*self.P_dum*self.C.T+self.R)
    matrix_dum = (self.C*self.P_dum*self.C.T+self.R)
    matrix_dum = matrix_dum.evalf()
    #print(matrix_dum)
    if(matrix_dum.det() != 0):
      self.Kalman = self.P_dum*self.C.T*matrix_dum.inv()
      self.Kalman = self.Kalman.evalf()
    else:
      print("DET=0")
    #print(self.Kalman)
    #print("2")
    end_effector = eye(4)
    #print(self.k)
    #print(self.p)
    #print(self.s)
    for i in range(0,3):
      tip_matrix = homogeneous_transform(self.k[i], self.p[i], self.s[i])
      end_effector = end_effector*tip_matrix
      
      tip_segment = homo_to_pose(end_effector)
      if(i==0):
	tip = tip_segment
	tip = tip.as_mutable()
      else:
	tip = tip_segment.row_insert(3, tip)
	tip = tip.as_mutable()
    #print(tip)
    #print(end_effector)
    self.state = self.A*self.state + self.B*self.u + self.Kalman*(self.tip_pose - tip)
    self.state = self.state.evalf()
    #print("3")
    #print(self.state)
    self.P = (eye(9)-self.Kalman*self.C)*self.P_dum
    self.P = self.P.evalf()

    
  def inverse_jacobian(self):
    for i in range(0,3):
      self.calc_sensor_jacobian(i+1)
      self.J = self.sensor_jacobian*self.JacobianKPSlNow
      #print(self.J)
      if(i == 0):
	self.C = self.J
	self.C = self.C.as_mutable()
      else:
	self.C = self.C.row_insert(3*i, self.J)
	self.C = self.C.as_mutable()
    #self.D = Matrix([[0, 1], [3, 4]])
    #self.E = Matrix([[1, 2], [5, 6]])
    #self.D = self.D.row_insert(2,self.E)
    #print(self.D)
    self.A = eye(9)
    self.B = self.dt*eye(9)
    #print(self.C)
    
    #C_dum = self.C.row_insert(1, self.J)
    #self.C = C_dum
    #self.C = self.C.row_insert(5, self.J)
    #print(self.C)
    #self.C = self.C.row_insert(6, self.J)
    #self.Obs = self.C*self.A
    #self.Obs = self.Obs.row_insert(0, self.C)
    #self.Obs = self.C
    #for i in range(0,7):
      #self.Obs = self.Obs.row_insert(0, self.C)
    
    #print(self.Obs.rank())
    
  def transform_to_sent(self):
    self.state_send.data = []
    self.state_send.data.append(self.state[0])
    self.state_send.data.append(self.state[1])
    self.state_send.data.append(self.state[2])
    self.state_send.data.append(self.state[3])
    self.state_send.data.append(self.state[4])
    self.state_send.data.append(self.state[5])
    self.state_send.data.append(self.state[6])
    self.state_send.data.append(self.state[7])
    self.state_send.data.append(self.state[8])
    
  def init_jacobian_velocity(self):
    for segment_num in range(1,4):
      homo_matrix = homogeneous_transform_sym(segment_num)
      pose_point = homo_to_pose(homo_matrix)
      
      self.JacVelSym = Matrix([[diff(pose_point[0,0],k1), diff(pose_point[0,0],p1), diff(pose_point[0,0],s1), diff(pose_point[0,0],k2), diff(pose_point[0,0],p2),diff(pose_point[0,0],s2), diff(pose_point[0,0],k3), diff(pose_point[0,0],p3),diff(pose_point[0,0],s3)],
				  [diff(pose_point[1,0],k1), diff(pose_point[1,0],p1), diff(pose_point[1,0],s1), diff(pose_point[1,0],k2), diff(pose_point[1,0],p2),diff(pose_point[1,0],s2), diff(pose_point[1,0],k3), diff(pose_point[1,0],p3),diff(pose_point[1,0],s3)],
				  [diff(pose_point[2,0],k1), diff(pose_point[2,0],p1), diff(pose_point[2,0],s1), diff(pose_point[2,0],k2), diff(pose_point[2,0],p2),diff(pose_point[2,0],s2), diff(pose_point[2,0],k3), diff(pose_point[2,0],p3),diff(pose_point[2,0],s3)]])
      if(segment_num == 1):
	self.JacVelFuncBot = lambdify((k1, k2, k3, p1, p2, p3, s1, s2, s3), self.JacVelSym, "sympy")
      elif(segment_num == 2):
	self.JacVelFuncMid = lambdify((k1, k2, k3, p1, p2, p3, s1, s2, s3), self.JacVelSym, "sympy")
      else:
	self.JacVelFuncUp = lambdify((k1, k2, k3, p1, p2, p3, s1, s2, s3), self.JacVelSym, "sympy")

    
  def init_jacobian_kps_l(self):
    self.Jacobian_kps_l = Matrix([ [diff(self.k1_ex, l11), diff(self.k1_ex, l12), diff(self.k1_ex, l13), diff(self.k1_ex, l21), diff(self.k1_ex, l22), diff(self.k1_ex, l23), diff(self.k1_ex, l31), diff(self.k1_ex, l32), diff(self.k1_ex, l33)],
				    [diff(self.p1_ex, l11), diff(self.p1_ex, l12), diff(self.p1_ex, l13), diff(self.p1_ex, l21), diff(self.p1_ex, l22), diff(self.p1_ex, l23), diff(self.p1_ex, l31), diff(self.p1_ex, l32), diff(self.p1_ex, l33)],
				    [diff(self.s1_ex, l11), diff(self.s1_ex, l12), diff(self.s1_ex, l13), diff(self.s1_ex, l21), diff(self.s1_ex, l22), diff(self.s1_ex, l23), diff(self.s1_ex, l31), diff(self.s1_ex, l32), diff(self.s1_ex, l33)],
				    [diff(self.k2_ex, l11), diff(self.k2_ex, l12), diff(self.k2_ex, l13), diff(self.k2_ex, l21), diff(self.k2_ex, l22), diff(self.k2_ex, l23), diff(self.k2_ex, l31), diff(self.k2_ex, l32), diff(self.k2_ex, l33)],
				    [diff(self.p2_ex, l11), diff(self.p2_ex, l12), diff(self.p2_ex, l13), diff(self.p2_ex, l21), diff(self.p2_ex, l22), diff(self.p2_ex, l23), diff(self.p2_ex, l31), diff(self.p2_ex, l32), diff(self.p2_ex, l33)],
				    [diff(self.s2_ex, l11), diff(self.s2_ex, l12), diff(self.s2_ex, l13), diff(self.s2_ex, l21), diff(self.s2_ex, l22), diff(self.s2_ex, l23), diff(self.s2_ex, l31), diff(self.s2_ex, l32), diff(self.s2_ex, l33)],
				    [diff(self.k3_ex, l11), diff(self.k3_ex, l12), diff(self.k3_ex, l13), diff(self.k3_ex, l21), diff(self.k3_ex, l22), diff(self.k3_ex, l23), diff(self.k3_ex, l31), diff(self.k3_ex, l32), diff(self.k3_ex, l33)],
				    [diff(self.p3_ex, l11), diff(self.p3_ex, l12), diff(self.p3_ex, l13), diff(self.p3_ex, l21), diff(self.p3_ex, l22), diff(self.p3_ex, l23), diff(self.p3_ex, l31), diff(self.p3_ex, l32), diff(self.p3_ex, l33)],
				    [diff(self.s3_ex, l11), diff(self.s3_ex, l12), diff(self.s3_ex, l13), diff(self.s3_ex, l21), diff(self.s3_ex, l22), diff(self.s3_ex, l23), diff(self.s3_ex, l31), diff(self.s3_ex, l32), diff(self.s3_ex, l33)]])
    self.Jacobian_kps_l_one = Matrix([[diff(self.k1_ex, l11), diff(self.k1_ex, l12), diff(self.k1_ex, l13)],
				      [diff(self.p1_ex, l11), diff(self.p1_ex, l12), diff(self.p1_ex, l13)],
				      [diff(self.s1_ex, l11), diff(self.s1_ex, l12), diff(self.s1_ex, l13)]])
    #print(self.Jacobian_kps_l_one)
    self.JacobianKPSlFunc = lambdify((l11, l12, l13, l21, l22, l23, l31, l32, l33), self.Jacobian_kps_l, "sympy")

  
  def init_jacobian_velocity_fast(self,segment_num):
    self.JacVelSymUp = Matrix([[(k1**2*k2*s1*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1**2*k3*s1*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k1*k2*k3*s1*sin(k1*s1) + k2*k3*(cos(k1*s1) - 1))*cos(p1)/(k1**2*k2*k3), (k1*k2*(-(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k3*s3) - 1)*sin(p3) + (cos(k3*s3) - 1)*(-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*cos(p3) - (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*sin(k3*s3)) + k1*k3*((cos(k2*s2) - 1)*sin(p1)*cos(p2)*cos(k1*s1) + (cos(k2*s2) - 1)*sin(p2)*cos(p1) - sin(p1)*sin(k1*s1)*sin(k2*s2)) + k2*k3*(cos(k1*s1) - 1)*sin(p1))/(k1*k2*k3), (k1*k2*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1*k3*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k2*k3*sin(k1*s1))*cos(p1)/(k2*k3), (k2**2*s2*((cos(k3*s3) - 1)*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(p3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3)) + k2*k3*s2*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1)) + k3*(-(cos(k2*s2) - 1)*sin(p1)*sin(p2) + (cos(k2*s2) - 1)*cos(p1)*cos(p2)*cos(k1*s1) - sin(k1*s1)*sin(k2*s2)*cos(p1)))/(k2**2*k3), (k2*(-(sin(p1)*sin(p2) - cos(p1)*cos(p2)*cos(k1*s1))*(cos(k3*s3) - 1)*sin(p3) + (sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3)*cos(k2*s2) - (sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*sin(k2*s2)*sin(k3*s3)) + k3*(sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k2*s2) - 1))/(k2*k3), (k2*(cos(k3*s3) - 1)*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(p3) - k2*(sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + k3*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1)))/k3, (k3*s3*(-(sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(k3*s3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3)*cos(p3)) - (sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k3*s3) - 1)*sin(p3) - (cos(k3*s3) - 1)*(sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*cos(p3) - (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*sin(k3*s3))/k3**2, ((sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*cos(p3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(p3))*(cos(k3*s3) - 1)/k3, -(sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(k3*s3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3)*cos(p3)], [(k1**2*k2*s1*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1**2*k3*s1*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k1*k2*k3*s1*sin(k1*s1) + k2*k3*(cos(k1*s1) - 1))*sin(p1)/(k1**2*k2*k3), (k1*k2*((sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k3*s3) - 1)*sin(p3) + (cos(k3*s3) - 1)*(sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*cos(p3) + (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*sin(k3*s3)) + k1*k3*((cos(k2*s2) - 1)*sin(p1)*sin(p2) - (cos(k2*s2) - 1)*cos(p1)*cos(p2)*cos(k1*s1) + sin(k1*s1)*sin(k2*s2)*cos(p1)) - k2*k3*(cos(k1*s1) - 1)*cos(p1))/(k1*k2*k3), (k1*k2*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1*k3*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k2*k3*sin(k1*s1))*sin(p1)/(k2*k3), (k2**2*s2*((cos(k3*s3) - 1)*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(p3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3)) + k2*k3*s2*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1)) + k3*((cos(k2*s2) - 1)*sin(p1)*cos(p2)*cos(k1*s1) + (cos(k2*s2) - 1)*sin(p2)*cos(p1) - sin(p1)*sin(k1*s1)*sin(k2*s2)))/(k2**2*k3), (k2*((sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k3*s3) - 1)*cos(p3)*cos(k2*s2) - (sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*sin(k2*s2)*sin(k3*s3) + (sin(p1)*cos(p2)*cos(k1*s1) + sin(p2)*cos(p1))*(cos(k3*s3) - 1)*sin(p3)) + k3*(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k2*s2) - 1))/(k2*k3), (k2*(cos(k3*s3) - 1)*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(p3) + k2*(-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3) + k3*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1)))/k3, (k3*s3*(-(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3)*cos(p3) + (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(k3*s3)) - (sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k3*s3) - 1)*sin(p3) + (cos(k3*s3) - 1)*(-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*cos(p3) - (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*sin(k3*s3))/k3**2, ((sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*cos(p3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(p3))*(cos(k3*s3) - 1)/k3, -(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3)*cos(p3) + (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(k3*s3)], [(-k1**2*k2*s1*((sin(k1*s1)*sin(k2*s2) - cos(p2)*cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) + (sin(k1*s1)*cos(k2*s2) + sin(k2*s2)*cos(p2)*cos(k1*s1))*sin(k3*s3) + (cos(k3*s3) - 1)*sin(p2)*sin(p3)*cos(k1*s1)) + k1**2*k3*s1*((cos(k2*s2) - 1)*cos(p2)*cos(k1*s1) - sin(k1*s1)*sin(k2*s2)) + k1*k2*k3*s1*cos(k1*s1) - k2*k3*sin(k1*s1))/(k1**2*k2*k3), 0, (-k1*k2*((sin(k1*s1)*sin(k2*s2) - cos(p2)*cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) + (sin(k1*s1)*cos(k2*s2) + sin(k2*s2)*cos(p2)*cos(k1*s1))*sin(k3*s3) + (cos(k3*s3) - 1)*sin(p2)*sin(p3)*cos(k1*s1)) + k1*k3*((cos(k2*s2) - 1)*cos(p2)*cos(k1*s1) - sin(k1*s1)*sin(k2*s2)) + k2*k3*cos(k1*s1))/(k2*k3), (-k2**2*s2*((sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3)) + k2*k3*s2*(-sin(k1*s1)*sin(k2*s2)*cos(p2) + cos(k1*s1)*cos(k2*s2)) - k3*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)))/(k2**2*k3), (k2*(-(cos(k3*s3) - 1)*sin(p2)*cos(p3)*cos(k2*s2) - (cos(k3*s3) - 1)*sin(p3)*cos(p2) + sin(p2)*sin(k2*s2)*sin(k3*s3)) - k3*(cos(k2*s2) - 1)*sin(p2))*sin(k1*s1)/(k2*k3), (-k2*(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) - k2*(sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3) + k3*(-sin(k1*s1)*sin(k2*s2)*cos(p2) + cos(k1*s1)*cos(k2*s2)))/k3, (k3*s3*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*cos(k3*s3) - (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3)*cos(p3) + sin(p2)*sin(p3)*sin(k1*s1)*sin(k3*s3)) + (sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) - (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) + (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1))/k3**2, -((sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(p3) + sin(p2)*sin(k1*s1)*cos(p3))*(cos(k3*s3) - 1)/k3, -(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*cos(k3*s3) - (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3)*cos(p3) + sin(p2)*sin(p3)*sin(k1*s1)*sin(k3*s3)]])
    self.JacVelFunc = lambdify((k1, k2, k3, p1, p2, p3, s1, s2, s3), self.JacVelSymUp, "sympy")

  def jacobian_velocity(self, segment_num):
    if(segment_num == 1):
      self.k = [self.k_out(self.state[0],self.state[1],self.state[2]), 0, 0]
      self.p = [l_to_phi(self.state[0],self.state[1],self.state[2]),0,0]
      self.s = [self.s_out(self.state[0],self.state[1],self.state[2]), 0, 0]
      JacVel = self.JacVelFuncBot(self.k[0],self.k[1],self.k[2],self.p[0],self.p[1],self.p[2],self.s[0],self.s[1],self.s[2])
    elif(segment_num == 2):
      self.k = [self.k_out(self.state[0],self.state[1],self.state[2]), self.k_out(self.state[3],self.state[4],self.state[5]), 0]
      self.p = [l_to_phi(self.state[0],self.state[1],self.state[2]),l_to_phi(self.state[3],self.state[4],self.state[5]),0]
      self.s = [self.s_out(self.state[0],self.state[1],self.state[2]), self.s_out(self.state[3],self.state[4],self.state[5]), 0]
      JacVel = self.JacVelFuncMid(self.k[0],self.k[1],self.k[2],self.p[0],self.p[1],self.p[2],self.s[0],self.s[1],self.s[2])
    elif(segment_num == 3):
      self.k = [self.k_out(self.state[0],self.state[1],self.state[2]), self.k_out(self.state[3],self.state[4],self.state[5]), self.k_out(self.state[6],self.state[7],self.state[8])]
      self.p = [l_to_phi(self.state[0],self.state[1],self.state[2]),l_to_phi(self.state[3],self.state[4],self.state[5]),l_to_phi(self.state[6],self.state[7],self.state[8])]
      self.s = [self.s_out(self.state[0],self.state[1],self.state[2]), self.s_out(self.state[3],self.state[4],self.state[5]), self.s_out(self.state[6],self.state[7],self.state[8])]
      JacVel = self.JacVelFuncUp(self.k[0],self.k[1],self.k[2],self.p[0],self.p[1],self.p[2],self.s[0],self.s[1],self.s[2])
    
    return JacVel
    
  def jacobian_kps_l(self):
    self.JacobianKPSlNow = self.JacobianKPSlFunc(self.state[0],self.state[1],self.state[2],self.state[3],self.state[4],self.state[5],self.state[6],self.state[7],self.state[8])    
    
  def calc_sensor_jacobian(self, segment_num):
    if(segment_num == 1):
      self.sensor_jacobian = self.jacobian_velocity(segment_num)
    elif(segment_num == 2):
      self.sensor_jacobian = self.jacobian_velocity(segment_num)
    elif(segment_num == 3):
      self.sensor_jacobian = self.jacobian_velocity(segment_num)
  
if __name__ == '__main__':
    try:
        observer()
    except rospy.ROSInterruptException: pass
