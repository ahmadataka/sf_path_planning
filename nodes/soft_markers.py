#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import math
import rospy
from visualization_msgs.msg import Marker
from visualization_msgs.msg import MarkerArray
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseArray
from std_msgs.msg import Float64MultiArray
from sympy import *

def homogeneous_transform(kappa_sym, phi_sym, s_sym):
    T = Matrix([[cos(phi_sym)*cos(kappa_sym*s_sym), -sin(phi_sym), cos(phi_sym)*sin(kappa_sym*s_sym), cos(phi_sym)*(1-cos(kappa_sym*s_sym))/kappa_sym],
		[sin(phi_sym)*cos(kappa_sym*s_sym), cos(phi_sym), sin(phi_sym)*sin(kappa_sym*s_sym), sin(phi_sym)*(1-cos(kappa_sym*s_sym))/kappa_sym],
		[-sin(kappa_sym*s_sym), 0, cos(kappa_sym*s_sym), sin(kappa_sym*s_sym)/kappa_sym],
		[0, 0, 0, 1]])
    return T

def homogeneous_transform_base(xb, yb, zb, alphab, betab, gammab):
    Tb = Matrix([ [cos(alphab)*cos(betab), cos(alphab)*sin(betab)*sin(gammab)-sin(alphab)*cos(gammab), cos(alphab)*sin(betab)*cos(gammab)+sin(alphab)*sin(gammab), xb],
		  [sin(alphab)*cos(betab), sin(alphab)*sin(betab)*sin(gammab)+cos(alphab)*cos(gammab), sin(alphab)*sin(betab)*cos(gammab)-cos(alphab)*sin(gammab), yb],
		  [-sin(betab), cos(betab)*sin(gammab), cos(betab)*cos(gammab), zb],
		  [0, 0, 0, 1]])
    return Tb

def homo_to_pose(T):
    pose = Pose()
    pose.position.x = T[0,3]
    pose.position.y = T[1,3]
    pose.position.z = T[2,3]
    pose.orientation.w = 0.5*sqrt(1+T[0,0]+T[1,1]+T[2,2])
    pose.orientation.x = (T[2,1]-T[1,2])/(4*pose.orientation.w)
    pose.orientation.y = (T[0,2]-T[2,0])/(4*pose.orientation.w)
    pose.orientation.z = (T[1,0]-T[0,1])/(4*pose.orientation.w)
    
    return pose

class soft_markers(object):
  def __init__(self):
    rospy.init_node('soft_markers')
    #print "a"
    self.base = rospy.get_param('/base')
    self.observer = rospy.get_param('/kalman_filter')
    self.flag = rospy.get_param('/exp')
    #k, p, s = symbols('k p s')
    #we should make this parameters in the future
    self.k = [0.706325262037712, 0.706325262037712, 0.706325262037712]
    self.p = [2.09439510239320, -1.04719755119660, 2.09439510239320]
    self.s = [0.122, 0.122, 0.122]
    self.position_base = [0.0, 0.0, 0.0]
    self.angle_base = [0.0, 0.0, 0.0]

    counter = 0
    inc = 0.02
    segment_index = 0
    
    tip_pose = PoseArray()
    
    marker_pub = rospy.Publisher('soft_marker', MarkerArray, queue_size = 10)
    if(self.observer == 0):
      config_space_sub = rospy.Subscriber('configuration_space', Float64MultiArray, self.get_kps)
    else:
      config_space_sub = rospy.Subscriber('configuration_space_est', Float64MultiArray, self.get_kps)
    #tip_pub = rospy.Publisher('tip_pose', PoseArray)
    
    r = rospy.Rate(200)
    markerArray = MarkerArray()
    index_length = 0
    while not rospy.is_shutdown():
	begin = rospy.Time.now()
	#if (counter > 5):
	  #for i in range(0,3):
	    #k[i] += 0.01
	    #s[i] += 0.01
	    #counter = 0
	marker = Marker()
	#print(self.k)
	#print(self.p)
	#print(self.s)
	if(self.flag==0):
	  marker.header.frame_id = "/world"
	else:
	  marker.header.frame_id = "/base"
	#marker.header.frame_id = "/tool0"
	marker.header.stamp = rospy.Time.now()
	marker.ns = "soft_backbone"
	marker.action = marker.ADD
	marker.type = marker.SPHERE
        marker.scale.x = 0.0134*2.0
        marker.scale.y = 0.0134*2.0
        marker.scale.z = 0.0134*2.0
	
	if(self.base == 1):
	  base_matrix = homogeneous_transform_base(self.position_base[0], self.position_base[1], self.position_base[2], self.angle_base[0], self.angle_base[1], self.angle_base[2])
	else:
	  base_matrix = eye(4)
	
	if (segment_index == 0):
	  dot_matrix = base_matrix*homogeneous_transform(self.k[0], self.p[0], index_length)
	  marker.color.a = 1.0
	  marker.color.r = 1.0
	  marker.color.g = 0.0
	  marker.color.b = 0.0
	
	elif (segment_index == 1):
	  dot_matrix = base_matrix*homogeneous_transform(self.k[0], self.p[0], self.s[0])*homogeneous_transform(self.k[1], self.p[1], index_length)
	  marker.color.a = 1.0
	  marker.color.r = 0.0
	  marker.color.g = 1.0
	  marker.color.b = 0.0
	
	else:
	  dot_matrix = base_matrix*homogeneous_transform(self.k[0], self.p[0], self.s[0])*homogeneous_transform(self.k[1], self.p[1], self.s[1])*homogeneous_transform(self.k[2], self.p[2], index_length)
	  marker.color.a = 1.0
	  marker.color.r = 0.0
	  marker.color.g = 0.0
	  marker.color.b = 1.0
	
	marker.pose = homo_to_pose(dot_matrix)
	markerArray.markers.append(marker)
	 # Renumber the marker IDs
	id = 0
	for m in markerArray.markers:
	    m.id = id
	    id += 1
	marker_pub.publish(markerArray);
	#print "\n"
	index_length += inc
	
	if(index_length>self.s[segment_index]):
	  index_length = 0
	  segment_index += 1
	  
	  
	if(segment_index > 2):
	  markerArray.markers = []
	  segment_index = 0
	  #counter += 1
	ending = (rospy.Time.now())
	#print(ending.nsecs-begin.nsecs)
        r.sleep()
        #rospy.spin()

  def get_kps(self,kps):
    if(self.base == 1):
      self.position_base = [kps.data[0], kps.data[1], kps.data[2]]
      self.angle_base = [kps.data[3], kps.data[4], kps.data[5]]
      self.k = [kps.data[6], kps.data[9], kps.data[12]]
      self.p = [kps.data[7], kps.data[10], kps.data[13]]
      self.s = [kps.data[8], kps.data[11], kps.data[14]]
    else:
      self.k = [kps.data[0], kps.data[3], kps.data[6]]
      self.p = [kps.data[1], kps.data[4], kps.data[7]]
      self.s = [kps.data[2], kps.data[5], kps.data[8]]
    print(self.s)

            
if __name__ == '__main__':
    try:
        soft_markers()
    except rospy.ROSInterruptException: pass
