#!/usr/bin/env python  
import roslib
roslib.load_manifest('soft_fuzzy')
import rospy
import tf
from geometry_msgs.msg import Pose

if __name__ == '__main__':
    rospy.init_node('tip_listener')
    
    active_frame = rospy.get_param('~frame')
    reference_frame = rospy.get_param('~ref')
    topic_name = rospy.get_param('~publish_to')
    listener = tf.TransformListener()

    tip_pose = rospy.Publisher(topic_name, Pose)

    rate = rospy.Rate(40.0)
    while not rospy.is_shutdown():
        try:
            (trans,rot) = listener.lookupTransform(reference_frame, active_frame, rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue
	pose = Pose()
        pose.position.x = trans[0]
        pose.position.y = trans[1]
        pose.position.z = trans[2]
        
        tip_pose.publish(pose)

        rate.sleep()