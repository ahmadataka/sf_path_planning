#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import math
import rospy
import numpy as np
from sympy import *
from scipy.integrate import ode
from std_msgs.msg import Float64MultiArray
from sf_path_planning.msg import sf_msg
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseArray

k1, k2, k3, p1, p2, p3, s1, s2, s3 = symbols('k1 k2 k3 p1 p2 p3 s1 s2 s3')
l11, l12, l13, l21, l22, l23, l31, l32, l33 = symbols('l11 l12 l13 l21 l22 l23 l31 l32 l33')

def homogeneous_transform_sym(segment_num):
    #TransMatrix = Matrix([[cos(p1)*cos(k1*s1), -sin(p1), cos(p1)*sin(k1*s1), cos(p1)*(1-cos(k1*s1))/k1],
			  #[sin(p1)*cos(k1*s1), cos(p1), sin(p1)*sin(k1*s1), sin(p1)*(1-cos(k1*s1))/k1],
			  #[-sin(k1*s1), 0, cos(k1*s1), sin(k1*s1)/k1],
			  #[0, 0, 0, 1]])
    TransMatrix = Matrix([[cos(k1*s1), 0, sin(k1*s1), (1-cos(k1*s1))/k1],
			  [0, 1, 0, 0],
			  [-sin(k1*s1), 0, cos(k1*s1), sin(k1*s1)/k1],
			  [0, 0, 0, 1]])
    
    if (segment_num == 1):
      TM = TransMatrix
    elif(segment_num == 2):
      TM = TransMatrix*(TransMatrix.subs([(k1, k2),(p1, p2),(s1, s2)]))
    else:
      TM = TransMatrix*(TransMatrix.subs([(k1, k2),(p1, p2),(s1, s2)]))*(TransMatrix.subs([(k1, k3),(p1, p3),(s1, s3)]))
    
    return TM
    
def homo_to_pose_sym(T):
    position = Matrix([[T[0,3]],[T[1,3]],[T[2,3]]])
    #print(position)
    return position
    
def l_to_phi(l1, l2, l3):
    #phi_out = atan2((sqrt(3)*(l2+l3-2*l1)),(3*(l2-l3)))
    y_atan = (sqrt(3)*(l2+l3-2*l1))
    x_atan = (3*(l2-l3))
    y_final = float(y_atan.evalf())
    x_final = float(x_atan)
    
    phi_out = np.arctan2(y_final,x_final)
    #phi_out = atan((sqrt(3)*(l2+l3-2*l1))/(3*(l2-l3)))
    #print(trigsimp(phi_out))
    #return phi_out.evalf()
    return phi_out
      
class inv_jacobi(object):
  
  def __init__(self):
    rospy.init_node('inv_jacobi')
    
    self.JacVelSym = zeros(3,9)
    self.JacVelSymBot = zeros(3,9)
    self.JacVelSymMid = zeros(3,9)
    self.JacVe=lSymUp = zeros(3,9)
    self.JacOmegaSym = zeros(3,9)
    self.Jw = zeros(3,9)
    self.Jv = zeros(3,9)
    self.Lo = 0.1200
    d = 0.0134
    self.p1_ex = atan((sqrt(3)*(l12+l13-2*l11))/(3*(l12-l13)))
    self.k1_ex = 2*(sqrt(pow(l11,2)+pow(l12,2)+pow(l13,2)-l11*l12-l11*l13-l12*l13))/(d*(3*self.Lo+l11+l12+l13))
    self.s1_ex = (3*self.Lo+l11+l12+l13)/3
    self.p2_ex = atan((sqrt(3)*(l22+l23-2*l21))/(3*(l22-l23)))
    self.k2_ex = 2*(sqrt(pow(l21,2)+pow(l22,2)+pow(l23,2)-l21*l22-l21*l23-l22*l23))/(d*(3*self.Lo+l21+l22+l23))
    self.s2_ex = (3*self.Lo+l21+l22+l23)/3
    self.p3_ex = atan((sqrt(3)*(l32+l33-2*l31))/(3*(l32-l33)))
    self.k3_ex = 2*(sqrt(pow(l31,2)+pow(l32,2)+pow(l33,2)-l31*l32-l31*l33-l32*l33))/(d*(3*self.Lo+l31+l32+l33))
    self.s3_ex = (3*self.Lo+l31+l32+l33)/3
    
    self.k_out = lambdify((l11, l12, l13), self.k1_ex, "sympy")
    self.s_out = lambdify((l11, l12, l13), self.s1_ex, "sympy")
    
    self.init_jacobian_velocity(3)
    #self.init_jacobian_velocity_fast(3)
    self.init_jacobian_kps_l()
    #print(np.arctan2(0.0,0.0))
    #print(simplify(sin(k1)**2+cos(k1)**2))
    #just_try = Matrix([[sin(k1)+k2], [k1+sin(k2)]])
    #out = lambdify((k1, k2), just_try, "numpy")
    #print(out(0, 2))
    #print((self.JacVelSymUp))
    #print(self.Jacobian_kps_l)
    
    self.dt = 0.01
    
    while (not rospy.is_shutdown()):
      ataka = 1
      #diff_length_pub.publish(self.diff_length)
      
    
  def init_jacobian_velocity(self,segment_num):
    homo_matrix = homogeneous_transform_sym(segment_num)
    pose_point = homo_to_pose_sym(homo_matrix)
    pose2D = Matrix([[pose_point[0,0]], [pose_point[2,0]]])
    #print(pose2D)
    #print(pose_point)
    #if(segment_num == 1):
      #self.JacVelSymBot = Matrix([[simplify(diff(pose_point[0,0],k1)), simplify(diff(pose_point[0,0],p1)), diff(pose_point[0,0],s1), diff(pose_point[0,0],k2), diff(pose_point[0,0],p2),diff(pose_point[0,0],s2), diff(pose_point[0,0],k3), diff(pose_point[0,0],p3),diff(pose_point[0,0],s3)],
				  #[diff(pose_point[1,0],k1), diff(pose_point[1,0],p1), diff(pose_point[1,0],s1), diff(pose_point[1,0],k2), diff(pose_point[1,0],p2),diff(pose_point[1,0],s2), diff(pose_point[1,0],k3), diff(pose_point[1,0],p3),diff(pose_point[1,0],s3)],
				  #[diff(pose_point[2,0],k1), diff(pose_point[2,0],p1), diff(pose_point[2,0],s1), diff(pose_point[2,0],k2), diff(pose_point[2,0],p2),diff(pose_point[2,0],s2), diff(pose_point[2,0],k3), diff(pose_point[2,0],p3),diff(pose_point[2,0],s3)]])
    #elif(segment_num == 2):
      #self.JacVelSymMid = Matrix([[diff(pose_point[0,0],k1), diff(pose_point[0,0],p1), diff(pose_point[0,0],s1), diff(pose_point[0,0],k2), diff(pose_point[0,0],p2),diff(pose_point[0,0],s2), diff(pose_point[0,0],k3), diff(pose_point[0,0],p3),diff(pose_point[0,0],s3)],
				  #[diff(pose_point[1,0],k1), diff(pose_point[1,0],p1), diff(pose_point[1,0],s1), diff(pose_point[1,0],k2), diff(pose_point[1,0],p2),diff(pose_point[1,0],s2), diff(pose_point[1,0],k3), diff(pose_point[1,0],p3),diff(pose_point[1,0],s3)],
				  #[diff(pose_point[2,0],k1), diff(pose_point[2,0],p1), diff(pose_point[2,0],s1), diff(pose_point[2,0],k2), diff(pose_point[2,0],p2),diff(pose_point[2,0],s2), diff(pose_point[2,0],k3), diff(pose_point[2,0],p3),diff(pose_point[2,0],s3)]])
    #else:
      #self.JacVelSymUp = Matrix([[simplify(diff(pose_point[0,0],k1)), simplify(diff(pose_point[0,0],p1)), simplify(diff(pose_point[0,0],s1)), simplify(diff(pose_point[0,0],k2)), simplify(diff(pose_point[0,0],p2)),simplify(diff(pose_point[0,0],s2)), simplify(diff(pose_point[0,0],k3)), simplify(diff(pose_point[0,0],p3)),simplify(diff(pose_point[0,0],s3))],
				  #[simplify(diff(pose_point[1,0],k1)), simplify(diff(pose_point[1,0],p1)), simplify(diff(pose_point[1,0],s1)), simplify(diff(pose_point[1,0],k2)), simplify(diff(pose_point[1,0],p2)),simplify(diff(pose_point[1,0],s2)), simplify(diff(pose_point[1,0],k3)), simplify(diff(pose_point[1,0],p3)),simplify(diff(pose_point[1,0],s3))],
				  #[simplify(diff(pose_point[2,0],k1)), simplify(diff(pose_point[2,0],p1)), simplify(diff(pose_point[2,0],s1)), simplify(diff(pose_point[2,0],k2)), simplify(diff(pose_point[2,0],p2)),simplify(diff(pose_point[2,0],s2)), simplify(diff(pose_point[2,0],k3)), simplify(diff(pose_point[2,0],p3)),simplify(diff(pose_point[2,0],s3))]])
    #self.JacVelSymUp = Matrix([[diff(pose_point[0,0],k1), diff(pose_point[0,0],p1), diff(pose_point[0,0],s1), diff(pose_point[0,0],k2), diff(pose_point[0,0],p2),diff(pose_point[0,0],s2), diff(pose_point[0,0],k3), diff(pose_point[0,0],p3),diff(pose_point[0,0],s3)],
				  #[diff(pose_point[1,0],k1), diff(pose_point[1,0],p1), diff(pose_point[1,0],s1), diff(pose_point[1,0],k2), diff(pose_point[1,0],p2),diff(pose_point[1,0],s2), diff(pose_point[1,0],k3), diff(pose_point[1,0],p3),diff(pose_point[1,0],s3)],
				  #[diff(pose_point[2,0],k1), diff(pose_point[2,0],p1), diff(pose_point[2,0],s1), diff(pose_point[2,0],k2), diff(pose_point[2,0],p2),diff(pose_point[2,0],s2), diff(pose_point[2,0],k3), diff(pose_point[2,0],p3),diff(pose_point[2,0],s3)]])
    self.JacVelSymUp = Matrix([[diff(pose_point[0,0],k1), diff(pose_point[0,0],s1), diff(pose_point[0,0],k2), diff(pose_point[0,0],s2)],
				  [diff(pose_point[1,0],k1), diff(pose_point[1,0],s1), diff(pose_point[1,0],k2), diff(pose_point[1,0],s2)],
				  [diff(pose_point[2,0],k1), diff(pose_point[2,0],s1), diff(pose_point[2,0],k2), diff(pose_point[2,0],s2)]])
    #Jacobi2D = Matrix([[diff(pose2D[0,0],k1), diff(pose2D[0,0],s1), diff(pose2D[0,0],k2), diff(pose2D[0,0],s2)],
		       #[diff(pose2D[1,0],k1), diff(pose2D[1,0],s1), diff(pose2D[1,0],k2), diff(pose2D[1,0],s2)]])
    Jacobi2D = Matrix([[diff(pose2D[0,0],k1), diff(pose2D[0,0],s1), diff(pose2D[0,0],k2), diff(pose2D[0,0],s2), diff(pose2D[0,0],k3), diff(pose2D[0,0],s3)],
		       [diff(pose2D[1,0],k1), diff(pose2D[1,0],s1), diff(pose2D[1,0],k2), diff(pose2D[1,0],s2), diff(pose2D[1,0],k3), diff(pose2D[1,0],s3)]])
    
    print(Jacobi2D[1,5])
    #print(self.JacVelSymUp[2,8])
    self.JacVelFunc = lambdify((k1, k2, k3, p1, p2, p3, s1, s2, s3), self.JacVelSymUp, "sympy")
    #print(self.JacVelFunc(1.0, 0.5, -1.0, 0.2, 0.1, 0.3, 0.12, 0.12, 0.12))
    #print(self.JacVelSymUp.subs([(k1, 1.0),(k2, self.k[1]),(k3, self.k[2]),(p1, self.p[0]),(p2, self.p[1]),(p3, self.p[2]),(s1, self.s[0]),(s2, self.s[1]),(s3, self.s[2])]))
    #return JacVelSym
  def init_jacobian_kps_l(self):
    self.Jacobian_kps_l = Matrix([ [diff(self.k1_ex, l11), diff(self.k1_ex, l12), diff(self.k1_ex, l13), diff(self.k1_ex, l21), diff(self.k1_ex, l22), diff(self.k1_ex, l23), diff(self.k1_ex, l31), diff(self.k1_ex, l32), diff(self.k1_ex, l33)],
				    [diff(self.p1_ex, l11), diff(self.p1_ex, l12), diff(self.p1_ex, l13), diff(self.p1_ex, l21), diff(self.p1_ex, l22), diff(self.p1_ex, l23), diff(self.p1_ex, l31), diff(self.p1_ex, l32), diff(self.p1_ex, l33)],
				    [diff(self.s1_ex, l11), diff(self.s1_ex, l12), diff(self.s1_ex, l13), diff(self.s1_ex, l21), diff(self.s1_ex, l22), diff(self.s1_ex, l23), diff(self.s1_ex, l31), diff(self.s1_ex, l32), diff(self.s1_ex, l33)],
				    [diff(self.k2_ex, l11), diff(self.k2_ex, l12), diff(self.k2_ex, l13), diff(self.k2_ex, l21), diff(self.k2_ex, l22), diff(self.k2_ex, l23), diff(self.k2_ex, l31), diff(self.k2_ex, l32), diff(self.k2_ex, l33)],
				    [diff(self.p2_ex, l11), diff(self.p2_ex, l12), diff(self.p2_ex, l13), diff(self.p2_ex, l21), diff(self.p2_ex, l22), diff(self.p2_ex, l23), diff(self.p2_ex, l31), diff(self.p2_ex, l32), diff(self.p2_ex, l33)],
				    [diff(self.s2_ex, l11), diff(self.s2_ex, l12), diff(self.s2_ex, l13), diff(self.s2_ex, l21), diff(self.s2_ex, l22), diff(self.s2_ex, l23), diff(self.s2_ex, l31), diff(self.s2_ex, l32), diff(self.s2_ex, l33)],
				    [diff(self.k3_ex, l11), diff(self.k3_ex, l12), diff(self.k3_ex, l13), diff(self.k3_ex, l21), diff(self.k3_ex, l22), diff(self.k3_ex, l23), diff(self.k3_ex, l31), diff(self.k3_ex, l32), diff(self.k3_ex, l33)],
				    [diff(self.p3_ex, l11), diff(self.p3_ex, l12), diff(self.p3_ex, l13), diff(self.p3_ex, l21), diff(self.p3_ex, l22), diff(self.p3_ex, l23), diff(self.p3_ex, l31), diff(self.p3_ex, l32), diff(self.p3_ex, l33)],
				    [diff(self.s3_ex, l11), diff(self.s3_ex, l12), diff(self.s3_ex, l13), diff(self.s3_ex, l21), diff(self.s3_ex, l22), diff(self.s3_ex, l23), diff(self.s3_ex, l31), diff(self.s3_ex, l32), diff(self.s3_ex, l33)]])
    self.Jacobian_kps_l_one = Matrix([[diff(self.k1_ex, l11), diff(self.k1_ex, l12), diff(self.k1_ex, l13)],
				      [diff(self.p1_ex, l11), diff(self.p1_ex, l12), diff(self.p1_ex, l13)],
				      [diff(self.s1_ex, l11), diff(self.s1_ex, l12), diff(self.s1_ex, l13)]])
    #print(self.Jacobian_kps_l_one)
    self.JacobianKPSlFunc = lambdify((l11, l12, l13, l21, l22, l23, l31, l32, l33), self.Jacobian_kps_l, "sympy")
  
      
if __name__ == '__main__':
    try:
        inv_jacobi()
    except rospy.ROSInterruptException: pass
