#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import math
import rospy
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseArray
from std_msgs.msg import Float64MultiArray
from sympy import *
import random

def homogeneous_transform(kappa_sym, phi_sym, s_sym):
    T = Matrix([[cos(phi_sym)*cos(kappa_sym*s_sym), -sin(phi_sym), cos(phi_sym)*sin(kappa_sym*s_sym), cos(phi_sym)*(1-cos(kappa_sym*s_sym))/kappa_sym],
		[sin(phi_sym)*cos(kappa_sym*s_sym), cos(phi_sym), sin(phi_sym)*sin(kappa_sym*s_sym), sin(phi_sym)*(1-cos(kappa_sym*s_sym))/kappa_sym],
		[-sin(kappa_sym*s_sym), 0, cos(kappa_sym*s_sym), sin(kappa_sym*s_sym)/kappa_sym],
		[0, 0, 0, 1]])
    return T

def homogeneous_transform_base(xb, yb, zb, alphab, betab, gammab):
    Tb = Matrix([ [cos(alphab)*cos(betab), cos(alphab)*sin(betab)*sin(gammab)-sin(alphab)*cos(gammab), cos(alphab)*sin(betab)*cos(gammab)+sin(alphab)*sin(gammab), xb],
		  [sin(alphab)*cos(betab), sin(alphab)*sin(betab)*sin(gammab)+cos(alphab)*cos(gammab), sin(alphab)*sin(betab)*cos(gammab)-cos(alphab)*sin(gammab), yb],
		  [-sin(betab), cos(betab)*sin(gammab), cos(betab)*cos(gammab), zb],
		  [0, 0, 0, 1]])
    return Tb

def homo_to_pose(T):
    pose = Pose()
    pose.position.x = T[0,3]
    pose.position.y = T[1,3]
    pose.position.z = T[2,3]
    pose.orientation.w = 0.5*sqrt(1+T[0,0]+T[1,1]+T[2,2])
    pose.orientation.x = (T[2,1]-T[1,2])/(4*pose.orientation.w)
    pose.orientation.y = (T[0,2]-T[2,0])/(4*pose.orientation.w)
    pose.orientation.z = (T[1,0]-T[0,1])/(4*pose.orientation.w)
    
    return pose

def add_noise(tip):
	# Noise generator
	tip_noi = Pose()
	mu, sigma = 0, 0.005
	scalar_noise = random.gauss(mu, sigma)
	#scalar_noise = 0.0
	tip_noi.position.x = tip.position.x + scalar_noise
	tip_noi.position.y = tip.position.y + scalar_noise
	tip_noi.position.z = tip.position.z + scalar_noise
	tip_noi.orientation.x = tip.orientation.x
	tip_noi.orientation.y = tip.orientation.y
	tip_noi.orientation.z = tip.orientation.z
	tip_noi.orientation.w = tip.orientation.w
	return tip_noi
	
class tip_pose_node(object):
  def __init__(self):
    rospy.init_node('tip_pose_node')
    self.base = rospy.get_param('/base')
    self.observer = rospy.get_param('/kalman_filter')
    #k, p, s = symbols('k p s')
    self.k = [0.706325262037712, 0.706325262037712, 0.706325262037712]
    self.p = [2.09439510239320, -1.04719755119660, 2.09439510239320]
    self.s = [0.122, 0.122, 0.122]
    self.k_e = [0.706325262037712, 0.706325262037712, 0.706325262037712]
    self.p_e = [2.09439510239320, -1.04719755119660, 2.09439510239320]
    self.s_e = [0.122, 0.122, 0.122]
    self.position_base = [0.0, 0.0, 0.0]
    self.angle_base = [0.0, 0.0, 0.0]

    tip_pose = PoseArray()
    tip_pose_est = PoseArray()
    
    config_space_sub = rospy.Subscriber('configuration_space', Float64MultiArray, self.get_kps)
    config_space_est_sub = rospy.Subscriber('configuration_space_est', Float64MultiArray, self.get_kps_est)
    tip_pub = rospy.Publisher('tip_pose', PoseArray, queue_size = 10)
    tip_est_pub = rospy.Publisher('tip_pose_est', PoseArray, queue_size = 10)
    r = rospy.Rate(40)
    while not rospy.is_shutdown():
	end_effector = eye(4)
	end_effector_noi = eye(4)
	tip_pose.poses = []
	tip_pose_est.poses = []
	if(self.base == 1):
	  end_effector = end_effector*homogeneous_transform_base(self.position_base[0], self.position_base[1], self.position_base[2], self.angle_base[0], self.angle_base[1], self.angle_base[2])
	for i in range(0,3):
	  tip_matrix = homogeneous_transform(self.k[i], self.p[i], self.s[i])
	  end_effector = end_effector*tip_matrix
	  #print(i)
	  #print(end_effector)
	  if(self.observer == 0):
	    tip_pose.poses.append(homo_to_pose(end_effector))
	  else:
	    tip_pose.poses.append(add_noise(homo_to_pose(end_effector)))
	  if(self.observer == 1):
	    tip_matrix = homogeneous_transform(self.k_e[i], self.p_e[i], self.s_e[i])
	    end_effector_noi = end_effector_noi*tip_matrix
	    tip_pose_est.poses.append(homo_to_pose(end_effector_noi))
	  #print(homo_to_pose(end_effector))
	  #print(tip_pose)
	#print(tip_pose.poses[0].position.x)
	tip_pose.header.stamp = rospy.Time.now()
	tip_pose_est.header.stamp = rospy.Time.now()
	tip_pub.publish(tip_pose);	
	tip_est_pub.publish(tip_pose_est)
        r.sleep()
        #rospy.spin()
        
  def get_kps(self,kps):
    if(self.base == 1):
      self.position_base = [kps.data[0], kps.data[1], kps.data[2]]
      self.angle_base = [kps.data[3], kps.data[4], kps.data[5]]
      self.k = [kps.data[6], kps.data[9], kps.data[12]]
      self.p = [kps.data[7], kps.data[10], kps.data[13]]
      self.s = [kps.data[8], kps.data[11], kps.data[14]]
    else:
      self.k = [kps.data[0], kps.data[3], kps.data[6]]
      self.p = [kps.data[1], kps.data[4], kps.data[7]]
      self.s = [kps.data[2], kps.data[5], kps.data[8]]
      
  def get_kps_est(self,kps):
      self.k_e = [kps.data[0], kps.data[3], kps.data[6]]
      self.p_e = [kps.data[1], kps.data[4], kps.data[7]]
      self.s_e = [kps.data[2], kps.data[5], kps.data[8]]  
        
if __name__ == '__main__':
    try:
        tip_pose_node()
    except rospy.ROSInterruptException: pass
