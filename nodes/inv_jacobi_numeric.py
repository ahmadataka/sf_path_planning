#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import math
import rospy
import numpy as np
from sympy import *
from scipy.integrate import ode
from std_msgs.msg import Float64MultiArray

def homogeneous_transform_sym(k1, p1, s1, k2, p2, s2, k3, p3, s3):

    TM1 = Matrix([[cos(p1)*cos(k1*s1), -sin(p1), cos(p1)*sin(k1*s1), cos(p1)*(1-cos(k1*s1))/k1],
		  [sin(p1)*cos(k1*s1), cos(p1), sin(p1)*sin(k1*s1), sin(p1)*(1-cos(k1*s1))/k1],
		  [-sin(k1*s1), 0, cos(k1*s1), sin(k1*s1)/k1],
		  [0, 0, 0, 1]])
    TM2 = Matrix([[cos(p2)*cos(k2*s2), -sin(p2), cos(p2)*sin(k2*s2), cos(p2)*(1-cos(k2*s2))/k2],
		  [sin(p2)*cos(k2*s2), cos(p2), sin(p2)*sin(k2*s2), sin(p2)*(1-cos(k2*s2))/k2],
		  [-sin(k2*s2), 0, cos(k2*s2), sin(k2*s2)/k2],
		  [0, 0, 0, 1]])
    TM3 = Matrix([[cos(p3)*cos(k3*s3), -sin(p3), cos(p3)*sin(k3*s3), cos(p3)*(1-cos(k3*s3))/k3],
		  [sin(p3)*cos(k3*s3), cos(p3), sin(p3)*sin(k3*s3), sin(p3)*(1-cos(k3*s3))/k3],
		  [-sin(k3*s3), 0, cos(k3*s3), sin(k3*s3)/k3],
		  [0, 0, 0, 1]])

    TM = TM1*TM2*TM3
    
    return TM
    
def homo_to_pose_sym(T):
    position = Matrix([[T[0,3]],[T[1,3]],[T[2,3]]])
    #print(position)
    return position
    
def l_to_phi(l1, l2, l3):
    phi_out = atan2((sqrt(3)*(l2+l3-2*l1)),(3*(l2-l3)))
    #phi_out = atan((sqrt(3)*(l2+l3-2*l1))/(3*(l2-l3)))
    #print(trigsimp(phi_out))
    return phi_out.evalf()
      
class inv_jacobi(object):
  
  def __init__(self):
    rospy.init_node('inv_jacobi')
    self.JacVelSym = zeros(3,9)
    self.JacVelSymBot = zeros(3,9)
    self.JacVelSymMid = zeros(3,9)
    self.JacVe=lSymUp = zeros(3,9)
    self.JacOmegaSym = zeros(3,9)
    self.Jw = zeros(3,9)
    self.Jv = zeros(3,9)
    self.Lo = 1
    self.d = 17e-3
    
    pub = rospy.Publisher('/configuration_space', Float64MultiArray)
    length_pub = rospy.Publisher('/length', Float64MultiArray)
    #trial_pub = rospy.Publisher('/configuration_field', Float64MultiArray)
    #diff_length_pub = rospy.Publisher('/length_difference', Float64MultiArray)
    self.rec_vel = rospy.Subscriber('/field', Float64MultiArray, self.vel_callback)
    self.rec_vel_obs = rospy.Subscriber('/obs_field_configuration_space', Float64MultiArray, self.obs_callback)
    self.kps = Float64MultiArray()
    self.length = Float64MultiArray()
    self.config_field = Float64MultiArray()
    self.diff_length = Float64MultiArray()
    self.error_flag = 0
    
    #this is the state space initial value
    #in this case the segment's length component
    y0, t0 = [0.01, 0.02, 0.03, 0.01, 0.02, 0.03, 0.01, 0.02, 0.03], 0.0
    #y0, t0 = [0.01, 0.0101, 0.01, 0.01, 0.0101, 0.01, 0.01, 0.0101, 0.01], 0.0
    self.l = y0
    self.k = [self.k_out(y0[0],y0[1],y0[2]), self.k_out(y0[3],y0[4],y0[5]), self.k_out(y0[6],y0[7],y0[8])]
    self.p = [l_to_phi(y0[0],y0[1],y0[2]),l_to_phi(y0[3],y0[4],y0[5]),l_to_phi(y0[6],y0[7],y0[8])]
    self.s = [self.s_out(y0[0],y0[1],y0[2]), self.s_out(y0[3],y0[4],y0[5]), self.s_out(y0[6],y0[7],y0[8])]
    
    self.v_obs = zeros(9,1)
    self.v = [0, 0, 0]
    self.vel = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    
    self.smin = -0.5
    self.smax = 0.5
    self.slim = 0.1
    self.K_config = 0.001
    #r = ode(deriv).set_integrator('zvode', method='bdf')
    #r = ode(self.deriv).set_integrator('dopri5')
    #r = ode(self.deriv).set_integrator('dop853')
    r = ode(self.deriv).set_integrator('vode', method = 'bdf', order=5)
    #r = ode(self.deriv).set_integrator('zvode', method = 'bdf')
    r.set_initial_value(y0, t0)
    
    dt = 0.01
    self.delta = 0.01
    while (not rospy.is_shutdown()):
      #saiki = rospy.Time.now()
      self.pose_tip = homo_to_pose_sym(homogeneous_transform_sym(self.k[0], self.p[0], self.s[0], self.k[1], self.p[1], self.s[1], self.k[2], self.p[2], self.s[2]))
      
      self.pose_tip_after = homo_to_pose_sym(homogeneous_transform_sym(self.k[0]+self.delta, self.p[0], self.s[0], self.k[1], self.p[1], self.s[1], self.k[2], self.p[2], self.s[2]))
      #self.jacobian_velocity(self.k[0], self.k[1], self.k[2], self.p[0], self.p[1], self.p[2], self.s[0], self.s[1], self.s[2])
      #self.jacobian_kps_l(self.l[0], self.l[1], self.l[2], self.l[3], self.l[4], self.l[5], self.l[6], self.l[7], self.l[8])
      self.jacobian_velocity(self.k[0], self.k[1], self.k[2], self.p[0], self.p[1], self.p[2], self.s[0], self.s[1], self.s[2])
      self.jacobian_kps_l(self.l[0], self.l[1], self.l[2], self.l[3], self.l[4], self.l[5], self.l[6], self.l[7], self.l[8])
      dummy_matrix = self.Jv*self.JacobianKPSlNow
      self.inv_J = np.linalg.pinv(dummy_matrix)
      #while r.successful() and r.t < t1:
      if r.successful():
	#print "a"
	r.integrate(r.t+dt)
	#print "i"
	#print(r.t)
	#print(r.y[0])
        self.update(r.y)
      self.transform_to_sent()
      pub.publish(self.kps)
      length_pub.publish(self.length)
      #trial_pub.publish(self.config_field)
      #diff_length_pub.publish(self.diff_length)
      
  def vel_callback(self, msg):
    self.v[0] = msg.data[0]
    self.v[1] = msg.data[1]
    self.v[2] = msg.data[2]
    
  def k_out(self, l11, l12, l13):
    return 2*(sqrt(pow(l11,2)+pow(l12,2)+pow(l13,2)-l11*l12-l11*l13-l12*l13))/(self.d*(3*self.Lo+l11+l12+l13))
    
  def s_out(self, l11, l12, l13):
    return (3*self.Lo+l11+l12+l13)/3
    
  def obs_callback(self, msg):
    self.v_obs = Matrix([[msg.data[0]],[msg.data[1]],[msg.data[2]],[msg.data[3]],[msg.data[4]],[msg.data[5]],[msg.data[6]],[msg.data[7]],[msg.data[8]]])
    
  def deriv(self,t,y):  # return derivatives of the array y
    velocity = Matrix([self.v[0], self.v[1], self.v[2]])
    #velocity = Matrix([[0], [0], [5*sin(50*t)]])
    #oke = pseudo_inv(velocity)
    #velocity = 2*embuh
    #print(self.dummy)
    #print(self.embuh2_array)
    #print(self.embuh2.tolist())
    #dydt_mat = self.inv_J*velocity + self.v_obs
    
    #Potential Field in Config Space
    #self.configuration_field(y)
    #matrix_con_field = Matrix([[self.vel[0]],[self.vel[1]],[self.vel[2]],[self.vel[3]],[self.vel[4]],[self.vel[5]],[self.vel[6]],[self.vel[7]],[self.vel[8]]])
    dydt_mat = self.inv_J*velocity + self.v_obs #+ matrix_con_field
    
    dydt = dydt_mat.tolist()
    #Limit
    for i in range(0,9):
      if(y[i]<self.smin):
	y[i] = self.smin
	if(dydt[i][0]<0):
	  dydt[i][0]=0
      elif(y[i]>self.smax):
	y[i] = self.smax
	if(dydt[i][0]>0):
	  dydt[i][0]=0

    #print(dydt)
    return dydt
    
  def update(self,state):
    self.l = [state[0], state[1], state[2], state[3], state[4], state[5], state[6], state[7], state[8]]
    self.k = [self.k_out(self.l[0],self.l[1],self.l[2]), self.k_out(self.l[3],self.l[4],self.l[5]), self.k_out(self.l[6],self.l[7],self.l[8])]
    self.p = [l_to_phi(self.l[0],self.l[1],self.l[2]),l_to_phi(self.l[3],self.l[4],self.l[5]),l_to_phi(self.l[6],self.l[7],self.l[8])]
    self.s = [self.s_out(self.l[0],self.l[1],self.l[2]), self.s_out(self.l[3],self.l[4],self.l[5]), self.s_out(self.l[6],self.l[7],self.l[8])]
    #self.l_diff = [abs(state[0]-state[1]), abs(state[0]-state[2]), abs(state[1]-state[2]), abs(state[3]-state[4]), abs(state[3]-state[5]), abs(state[4]-state[5]), abs(state[6]-state[7]), abs(state[6]-state[8]), abs(state[7]-state[8])]
    self.singularity_checker()

    
  def singularity_checker(self):
    #checker = [0.0, 0.0, 0.0]
    #for i in range(0,3):
      #for j in range(0,3):
	#checker[j] = self.l_diff[3*i+j]
      #if((checker[0]<1e-5) and (checker[1]<1e-5) and (checker[2]<1e-5)):
	#print "SINGULARITY in segment" + str(i)
    for i in range(0,3):
      if(self.k[i]<1e-5):
	print "SINGULARITY in segment" + str(i)
    
  def transform_to_sent(self):
    self.kps.data = []
    self.length.data = []
    self.diff_length.data = []
    self.kps.data.append(self.k[0])
    self.kps.data.append(self.p[0])
    self.kps.data.append(self.s[0])
    self.kps.data.append(self.k[1])
    self.kps.data.append(self.p[1])
    self.kps.data.append(self.s[1])
    self.kps.data.append(self.k[2])
    self.kps.data.append(self.p[2])
    self.kps.data.append(self.s[2])
    self.length.data.append(self.l[0])
    self.length.data.append(self.l[1])
    self.length.data.append(self.l[2])
    self.length.data.append(self.l[3])
    self.length.data.append(self.l[4])
    self.length.data.append(self.l[5])
    self.length.data.append(self.l[6])
    self.length.data.append(self.l[7])
    self.length.data.append(self.l[8])
      
  def jacobian_velocity(self, k1, k2, k3, p1, p2, p3, s1, s2, s3):
    #self.Jv = Matrix([[(k1**2*k2*s1*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1**2*k3*s1*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k1*k2*k3*s1*sin(k1*s1) + k2*k3*(cos(k1*s1) - 1))*cos(p1)/(k1**2*k2*k3), (k1*k2*(-(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k3*s3) - 1)*sin(p3) + (cos(k3*s3) - 1)*(-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*cos(p3) - (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*sin(k3*s3)) + k1*k3*((cos(k2*s2) - 1)*sin(p1)*cos(p2)*cos(k1*s1) + (cos(k2*s2) - 1)*sin(p2)*cos(p1) - sin(p1)*sin(k1*s1)*sin(k2*s2)) + k2*k3*(cos(k1*s1) - 1)*sin(p1))/(k1*k2*k3), (k1*k2*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1*k3*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k2*k3*sin(k1*s1))*cos(p1)/(k2*k3), (k2**2*s2*((cos(k3*s3) - 1)*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(p3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3)) + k2*k3*s2*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1)) + k3*(-(cos(k2*s2) - 1)*sin(p1)*sin(p2) + (cos(k2*s2) - 1)*cos(p1)*cos(p2)*cos(k1*s1) - sin(k1*s1)*sin(k2*s2)*cos(p1)))/(k2**2*k3), (k2*(-(sin(p1)*sin(p2) - cos(p1)*cos(p2)*cos(k1*s1))*(cos(k3*s3) - 1)*sin(p3) + (sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3)*cos(k2*s2) - (sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*sin(k2*s2)*sin(k3*s3)) + k3*(sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k2*s2) - 1))/(k2*k3), (k2*(cos(k3*s3) - 1)*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(p3) - k2*(sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + k3*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1)))/k3, (k3*s3*(-(sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(k3*s3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3)*cos(p3)) - (sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k3*s3) - 1)*sin(p3) - (cos(k3*s3) - 1)*(sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*cos(p3) - (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*sin(k3*s3))/k3**2, ((sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*cos(p3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(p3))*(cos(k3*s3) - 1)/k3, -(sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(k3*s3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3)*cos(p3)], [(k1**2*k2*s1*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1**2*k3*s1*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k1*k2*k3*s1*sin(k1*s1) + k2*k3*(cos(k1*s1) - 1))*sin(p1)/(k1**2*k2*k3), (k1*k2*((sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k3*s3) - 1)*sin(p3) + (cos(k3*s3) - 1)*(sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*cos(p3) + (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*sin(k3*s3)) + k1*k3*((cos(k2*s2) - 1)*sin(p1)*sin(p2) - (cos(k2*s2) - 1)*cos(p1)*cos(p2)*cos(k1*s1) + sin(k1*s1)*sin(k2*s2)*cos(p1)) - k2*k3*(cos(k1*s1) - 1)*cos(p1))/(k1*k2*k3), (k1*k2*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1*k3*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k2*k3*sin(k1*s1))*sin(p1)/(k2*k3), (k2**2*s2*((cos(k3*s3) - 1)*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(p3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3)) + k2*k3*s2*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1)) + k3*((cos(k2*s2) - 1)*sin(p1)*cos(p2)*cos(k1*s1) + (cos(k2*s2) - 1)*sin(p2)*cos(p1) - sin(p1)*sin(k1*s1)*sin(k2*s2)))/(k2**2*k3), (k2*((sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k3*s3) - 1)*cos(p3)*cos(k2*s2) - (sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*sin(k2*s2)*sin(k3*s3) + (sin(p1)*cos(p2)*cos(k1*s1) + sin(p2)*cos(p1))*(cos(k3*s3) - 1)*sin(p3)) + k3*(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k2*s2) - 1))/(k2*k3), (k2*(cos(k3*s3) - 1)*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(p3) + k2*(-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3) + k3*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1)))/k3, (k3*s3*(-(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3)*cos(p3) + (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(k3*s3)) - (sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k3*s3) - 1)*sin(p3) + (cos(k3*s3) - 1)*(-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*cos(p3) - (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*sin(k3*s3))/k3**2, ((sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*cos(p3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(p3))*(cos(k3*s3) - 1)/k3, -(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3)*cos(p3) + (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(k3*s3)], [(-k1**2*k2*s1*((sin(k1*s1)*sin(k2*s2) - cos(p2)*cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) + (sin(k1*s1)*cos(k2*s2) + sin(k2*s2)*cos(p2)*cos(k1*s1))*sin(k3*s3) + (cos(k3*s3) - 1)*sin(p2)*sin(p3)*cos(k1*s1)) + k1**2*k3*s1*((cos(k2*s2) - 1)*cos(p2)*cos(k1*s1) - sin(k1*s1)*sin(k2*s2)) + k1*k2*k3*s1*cos(k1*s1) - k2*k3*sin(k1*s1))/(k1**2*k2*k3), 0, (-k1*k2*((sin(k1*s1)*sin(k2*s2) - cos(p2)*cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) + (sin(k1*s1)*cos(k2*s2) + sin(k2*s2)*cos(p2)*cos(k1*s1))*sin(k3*s3) + (cos(k3*s3) - 1)*sin(p2)*sin(p3)*cos(k1*s1)) + k1*k3*((cos(k2*s2) - 1)*cos(p2)*cos(k1*s1) - sin(k1*s1)*sin(k2*s2)) + k2*k3*cos(k1*s1))/(k2*k3), (-k2**2*s2*((sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3)) + k2*k3*s2*(-sin(k1*s1)*sin(k2*s2)*cos(p2) + cos(k1*s1)*cos(k2*s2)) - k3*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)))/(k2**2*k3), (k2*(-(cos(k3*s3) - 1)*sin(p2)*cos(p3)*cos(k2*s2) - (cos(k3*s3) - 1)*sin(p3)*cos(p2) + sin(p2)*sin(k2*s2)*sin(k3*s3)) - k3*(cos(k2*s2) - 1)*sin(p2))*sin(k1*s1)/(k2*k3), (-k2*(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) - k2*(sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3) + k3*(-sin(k1*s1)*sin(k2*s2)*cos(p2) + cos(k1*s1)*cos(k2*s2)))/k3, (k3*s3*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*cos(k3*s3) - (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3)*cos(p3) + sin(p2)*sin(p3)*sin(k1*s1)*sin(k3*s3)) + (sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) - (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) + (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1))/k3**2, -((sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(p3) + sin(p2)*sin(k1*s1)*cos(p3))*(cos(k3*s3) - 1)/k3, -(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*cos(k3*s3) - (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3)*cos(p3) + sin(p2)*sin(p3)*sin(k1*s1)*sin(k3*s3)]])
    
    self.Jv = Matrix([[(k1**2*k2*s1*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1**2*k3*s1*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k1*k2*k3*s1*sin(k1*s1) + k2*k3*(cos(k1*s1) - 1))*cos(p1)/(k1**2*k2*k3), (k1*k2*(-(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k3*s3) - 1)*sin(p3) + (cos(k3*s3) - 1)*(-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*cos(p3) - (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*sin(k3*s3)) + k1*k3*((cos(k2*s2) - 1)*sin(p1)*cos(p2)*cos(k1*s1) + (cos(k2*s2) - 1)*sin(p2)*cos(p1) - sin(p1)*sin(k1*s1)*sin(k2*s2)) + k2*k3*(cos(k1*s1) - 1)*sin(p1))/(k1*k2*k3), (k1*k2*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1*k3*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k2*k3*sin(k1*s1))*cos(p1)/(k2*k3), (k2**2*s2*((cos(k3*s3) - 1)*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(p3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3)) + k2*k3*s2*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1)) + k3*(-(cos(k2*s2) - 1)*sin(p1)*sin(p2) + (cos(k2*s2) - 1)*cos(p1)*cos(p2)*cos(k1*s1) - sin(k1*s1)*sin(k2*s2)*cos(p1)))/(k2**2*k3), (k2*(-(sin(p1)*sin(p2) - cos(p1)*cos(p2)*cos(k1*s1))*(cos(k3*s3) - 1)*sin(p3) + (sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3)*cos(k2*s2) - (sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*sin(k2*s2)*sin(k3*s3)) + k3*(sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k2*s2) - 1))/(k2*k3), (k2*(cos(k3*s3) - 1)*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(p3) - k2*(sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + k3*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1)))/k3, (k3*s3*(-(sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(k3*s3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3)*cos(p3)) - (sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k3*s3) - 1)*sin(p3) - (cos(k3*s3) - 1)*(sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*cos(p3) - (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*sin(k3*s3))/k3**2, ((sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*cos(p3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(p3))*(cos(k3*s3) - 1)/k3, -(sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(k3*s3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3)*cos(p3)], [(k1**2*k2*s1*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1**2*k3*s1*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k1*k2*k3*s1*sin(k1*s1) + k2*k3*(cos(k1*s1) - 1))*sin(p1)/(k1**2*k2*k3), (k1*k2*((sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k3*s3) - 1)*sin(p3) + (cos(k3*s3) - 1)*(sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*cos(p3) + (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*sin(k3*s3)) + k1*k3*((cos(k2*s2) - 1)*sin(p1)*sin(p2) - (cos(k2*s2) - 1)*cos(p1)*cos(p2)*cos(k1*s1) + sin(k1*s1)*sin(k2*s2)*cos(p1)) - k2*k3*(cos(k1*s1) - 1)*cos(p1))/(k1*k2*k3), (k1*k2*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1*k3*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k2*k3*sin(k1*s1))*sin(p1)/(k2*k3), (k2**2*s2*((cos(k3*s3) - 1)*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(p3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3)) + k2*k3*s2*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1)) + k3*((cos(k2*s2) - 1)*sin(p1)*cos(p2)*cos(k1*s1) + (cos(k2*s2) - 1)*sin(p2)*cos(p1) - sin(p1)*sin(k1*s1)*sin(k2*s2)))/(k2**2*k3), (k2*((sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k3*s3) - 1)*cos(p3)*cos(k2*s2) - (sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*sin(k2*s2)*sin(k3*s3) + (sin(p1)*cos(p2)*cos(k1*s1) + sin(p2)*cos(p1))*(cos(k3*s3) - 1)*sin(p3)) + k3*(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k2*s2) - 1))/(k2*k3), (k2*(cos(k3*s3) - 1)*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(p3) + k2*(-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3) + k3*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1)))/k3, (k3*s3*(-(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3)*cos(p3) + (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(k3*s3)) - (sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k3*s3) - 1)*sin(p3) + (cos(k3*s3) - 1)*(-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*cos(p3) - (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*sin(k3*s3))/k3**2, ((sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*cos(p3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(p3))*(cos(k3*s3) - 1)/k3, -(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3)*cos(p3) + (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(k3*s3)], [(-k1**2*k2*s1*((sin(k1*s1)*sin(k2*s2) - cos(p2)*cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) + (sin(k1*s1)*cos(k2*s2) + sin(k2*s2)*cos(p2)*cos(k1*s1))*sin(k3*s3) + (cos(k3*s3) - 1)*sin(p2)*sin(p3)*cos(k1*s1)) + k1**2*k3*s1*((cos(k2*s2) - 1)*cos(p2)*cos(k1*s1) - sin(k1*s1)*sin(k2*s2)) + k1*k2*k3*s1*cos(k1*s1) - k2*k3*sin(k1*s1))/(k1**2*k2*k3), 0, (-k1*k2*((sin(k1*s1)*sin(k2*s2) - cos(p2)*cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) + (sin(k1*s1)*cos(k2*s2) + sin(k2*s2)*cos(p2)*cos(k1*s1))*sin(k3*s3) + (cos(k3*s3) - 1)*sin(p2)*sin(p3)*cos(k1*s1)) + k1*k3*((cos(k2*s2) - 1)*cos(p2)*cos(k1*s1) - sin(k1*s1)*sin(k2*s2)) + k2*k3*cos(k1*s1))/(k2*k3), (-k2**2*s2*((sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3)) + k2*k3*s2*(-sin(k1*s1)*sin(k2*s2)*cos(p2) + cos(k1*s1)*cos(k2*s2)) - k3*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)))/(k2**2*k3), (k2*(-(cos(k3*s3) - 1)*sin(p2)*cos(p3)*cos(k2*s2) - (cos(k3*s3) - 1)*sin(p3)*cos(p2) + sin(p2)*sin(k2*s2)*sin(k3*s3)) - k3*(cos(k2*s2) - 1)*sin(p2))*sin(k1*s1)/(k2*k3), (-k2*(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) - k2*(sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3) + k3*(-sin(k1*s1)*sin(k2*s2)*cos(p2) + cos(k1*s1)*cos(k2*s2)))/k3, (k3*s3*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*cos(k3*s3) - (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3)*cos(p3) + sin(p2)*sin(p3)*sin(k1*s1)*sin(k3*s3)) + (sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) - (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) + (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1))/k3**2, -((sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(p3) + sin(p2)*sin(k1*s1)*cos(p3))*(cos(k3*s3) - 1)/k3, -(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*cos(k3*s3) - (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3)*cos(p3) + sin(p2)*sin(p3)*sin(k1*s1)*sin(k3*s3)]])
    #print(self.Jv)
    #return JacVel
    
  def jacobian_kps_l(self, l11, l12, l13, l21, l22, l23, l31, l32, l33):
    self.JacobianKPSlNow = Matrix([[2*(l11 - l12/2 - l13/2)/((0.017*l11 + 0.017*l12 + 0.017*l13 + 0.051)*sqrt(l11**2 - l11*l12 - l11*l13 + l12**2 - l12*l13 + l13**2)) - 0.034*sqrt(l11**2 - l11*l12 - l11*l13 + l12**2 - l12*l13 + l13**2)/(0.017*l11 + 0.017*l12 + 0.017*l13 + 0.051)**2, 2*(-l11/2 + l12 - l13/2)/((0.017*l11 + 0.017*l12 + 0.017*l13 + 0.051)*sqrt(l11**2 - l11*l12 - l11*l13 + l12**2 - l12*l13 + l13**2)) - 0.034*sqrt(l11**2 - l11*l12 - l11*l13 + l12**2 - l12*l13 + l13**2)/(0.017*l11 + 0.017*l12 + 0.017*l13 + 0.051)**2, 2*(-l11/2 - l12/2 + l13)/((0.017*l11 + 0.017*l12 + 0.017*l13 + 0.051)*sqrt(l11**2 - l11*l12 - l11*l13 + l12**2 - l12*l13 + l13**2)) - 0.034*sqrt(l11**2 - l11*l12 - l11*l13 + l12**2 - l12*l13 + l13**2)/(0.017*l11 + 0.017*l12 + 0.017*l13 + 0.051)**2, 0, 0, 0, 0, 0, 0], [-2*sqrt(3)/((1 + 3*(-2*l11 + l12 + l13)**2/(3*l12 - 3*l13)**2)*(3*l12 - 3*l13)), (sqrt(3)/(3*l12 - 3*l13) - 3*sqrt(3)*(-2*l11 + l12 + l13)/(3*l12 - 3*l13)**2)/(1 + 3*(-2*l11 + l12 + l13)**2/(3*l12 - 3*l13)**2), (sqrt(3)/(3*l12 - 3*l13) + 3*sqrt(3)*(-2*l11 + l12 + l13)/(3*l12 - 3*l13)**2)/(1 + 3*(-2*l11 + l12 + l13)**2/(3*l12 - 3*l13)**2), 0, 0, 0, 0, 0, 0], [1/3, 1/3, 1/3, 0, 0, 0, 0, 0, 0], [0, 0, 0, 2*(l21 - l22/2 - l23/2)/((0.017*l21 + 0.017*l22 + 0.017*l23 + 0.051)*sqrt(l21**2 - l21*l22 - l21*l23 + l22**2 - l22*l23 + l23**2)) - 0.034*sqrt(l21**2 - l21*l22 - l21*l23 + l22**2 - l22*l23 + l23**2)/(0.017*l21 + 0.017*l22 + 0.017*l23 + 0.051)**2, 2*(-l21/2 + l22 - l23/2)/((0.017*l21 + 0.017*l22 + 0.017*l23 + 0.051)*sqrt(l21**2 - l21*l22 - l21*l23 + l22**2 - l22*l23 + l23**2)) - 0.034*sqrt(l21**2 - l21*l22 - l21*l23 + l22**2 - l22*l23 + l23**2)/(0.017*l21 + 0.017*l22 + 0.017*l23 + 0.051)**2, 2*(-l21/2 - l22/2 + l23)/((0.017*l21 + 0.017*l22 + 0.017*l23 + 0.051)*sqrt(l21**2 - l21*l22 - l21*l23 + l22**2 - l22*l23 + l23**2)) - 0.034*sqrt(l21**2 - l21*l22 - l21*l23 + l22**2 - l22*l23 + l23**2)/(0.017*l21 + 0.017*l22 + 0.017*l23 + 0.051)**2, 0, 0, 0], [0, 0, 0, -2*sqrt(3)/((1 + 3*(-2*l21 + l22 + l23)**2/(3*l22 - 3*l23)**2)*(3*l22 - 3*l23)), (sqrt(3)/(3*l22 - 3*l23) - 3*sqrt(3)*(-2*l21 + l22 + l23)/(3*l22 - 3*l23)**2)/(1 + 3*(-2*l21 + l22 + l23)**2/(3*l22 - 3*l23)**2), (sqrt(3)/(3*l22 - 3*l23) + 3*sqrt(3)*(-2*l21 + l22 + l23)/(3*l22 - 3*l23)**2)/(1 + 3*(-2*l21 + l22 + l23)**2/(3*l22 - 3*l23)**2), 0, 0, 0], [0, 0, 0, 1/3, 1/3, 1/3, 0, 0, 0], [0, 0, 0, 0, 0, 0, 2*(l31 - l32/2 - l33/2)/((0.017*l31 + 0.017*l32 + 0.017*l33 + 0.051)*sqrt(l31**2 - l31*l32 - l31*l33 + l32**2 - l32*l33 + l33**2)) - 0.034*sqrt(l31**2 - l31*l32 - l31*l33 + l32**2 - l32*l33 + l33**2)/(0.017*l31 + 0.017*l32 + 0.017*l33 + 0.051)**2, 2*(-l31/2 + l32 - l33/2)/((0.017*l31 + 0.017*l32 + 0.017*l33 + 0.051)*sqrt(l31**2 - l31*l32 - l31*l33 + l32**2 - l32*l33 + l33**2)) - 0.034*sqrt(l31**2 - l31*l32 - l31*l33 + l32**2 - l32*l33 + l33**2)/(0.017*l31 + 0.017*l32 + 0.017*l33 + 0.051)**2, 2*(-l31/2 - l32/2 + l33)/((0.017*l31 + 0.017*l32 + 0.017*l33 + 0.051)*sqrt(l31**2 - l31*l32 - l31*l33 + l32**2 - l32*l33 + l33**2)) - 0.034*sqrt(l31**2 - l31*l32 - l31*l33 + l32**2 - l32*l33 + l33**2)/(0.017*l31 + 0.017*l32 + 0.017*l33 + 0.051)**2], [0, 0, 0, 0, 0, 0, -2*sqrt(3)/((1 + 3*(-2*l31 + l32 + l33)**2/(3*l32 - 3*l33)**2)*(3*l32 - 3*l33)), (sqrt(3)/(3*l32 - 3*l33) - 3*sqrt(3)*(-2*l31 + l32 + l33)/(3*l32 - 3*l33)**2)/(1 + 3*(-2*l31 + l32 + l33)**2/(3*l32 - 3*l33)**2), (sqrt(3)/(3*l32 - 3*l33) + 3*sqrt(3)*(-2*l31 + l32 + l33)/(3*l32 - 3*l33)**2)/(1 + 3*(-2*l31 + l32 + l33)**2/(3*l32 - 3*l33)**2)], [0, 0, 0, 0, 0, 0, 1/3, 1/3, 1/3]])

      
if __name__ == '__main__':
    try:
        inv_jacobi()
    except rospy.ROSInterruptException: pass
