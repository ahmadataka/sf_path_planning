#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import rospy
from geometry_msgs.msg import Pose
import tf
from sympy import *          
def base_broadcaster():
    rospy.init_node('base_broadcaster')
    
    path_frame = tf.TransformBroadcaster()
    path_frame2 = tf.TransformBroadcaster()
    path_frame3 = tf.TransformBroadcaster()
    path_frame4 = tf.TransformBroadcaster()
    path_frame5 = tf.TransformBroadcaster()
    rate = rospy.Rate(40.0)
    while not rospy.is_shutdown():
      #path_frame.sendTransform(((-0.17200-0.1707)/2.0,(0.00380+0.00070)/2.0,(-0.12090-0.12360)/2.0),
                     #(0.0, 0.0, 0.0, 1.0),
                     #rospy.Time.now(),
                     #"base1",
                     #"aurora")
      path_frame.sendTransform(((-0.07304-0.07576)/2.0,(-0.04301+0.03023)/2.0,(-0.15514-0.2312)/2.0),
                     (0.0, 0.0, 0.0, 1.0),
                     rospy.Time.now(),
                     "base1",
                     "aurora")

      path_frame2.sendTransform((0.0,0.0,0.0),
                     (-0.5*sqrt(2.0), 0.0, 0.0, 0.5*sqrt(2.0)),
                     rospy.Time.now(),
                     "base2",
                     "base1")
      path_frame3.sendTransform((0.0,0.0,0.0),
                     (0.0, sqrt(2.0)/2.0, 0.0, sqrt(2.0)/2.0),
                     rospy.Time.now(),
                     "base",
                     "base2")
      #path_frame4.sendTransform((0.0,0.0,0.0),
                     #(sqrt(2.0)/2.0, 0.0, 0.0, -sqrt(2.0)/2.0),
                     #rospy.Time.now(),
                     #"base4",
                     #"base3")
      #path_frame5.sendTransform((0.0,0.0,0.0),
                     #(sqrt(2.0)/2.0, 0.0, 0.0, sqrt(2.0)/2.0),
                     #rospy.Time.now(),
                     #"base",
                     #"base4")

      rate.sleep()
        
if __name__ == '__main__':
    try:
        base_broadcaster()
    except rospy.ROSInterruptException: pass
