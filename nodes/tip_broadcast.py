#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import rospy
from geometry_msgs.msg import PoseArray
import tf
                     
class tip_broadcast(object):
  def __init__(self):
    rospy.init_node('tip_broadcast')
    self.flag = rospy.get_param('/base')
    tip_sub = rospy.Subscriber('tip_pose', PoseArray, self.handle_pose)
    self.ref_frame = "world"
    if(self.flag==1):
      self.ref_frame = "base"
    
    #self.ref_frame = "tool0"

    rospy.spin()
    
  def handle_pose(self,msg):
    bot = tf.TransformBroadcaster()
    bot.sendTransform((msg.poses[0].position.x,msg.poses[0].position.y,msg.poses[0].position.z),
                     (msg.poses[0].orientation.x, msg.poses[0].orientation.y, msg.poses[0].orientation.z, msg.poses[0].orientation.w),
                     rospy.Time.now(),
                     "bottom_tip",
                     self.ref_frame)
    mid = tf.TransformBroadcaster()
    mid.sendTransform((msg.poses[1].position.x,msg.poses[1].position.y,msg.poses[1].position.z),
                     (msg.poses[1].orientation.x, msg.poses[1].orientation.y, msg.poses[1].orientation.z, msg.poses[1].orientation.w),
                     rospy.Time.now(),
                     "middle_tip",
                     self.ref_frame)
    up = tf.TransformBroadcaster()
    up.sendTransform((msg.poses[2].position.x,msg.poses[2].position.y,msg.poses[2].position.z),
                     (msg.poses[2].orientation.x, msg.poses[2].orientation.y, msg.poses[2].orientation.z, msg.poses[2].orientation.w),
                     rospy.Time.now(),
                     "up_tip",
                     self.ref_frame)
        
if __name__ == '__main__':
    try:
        tip_broadcast()
    except rospy.ROSInterruptException: pass
