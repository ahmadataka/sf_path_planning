function ataka_complete_three_modules_three_dimention_quaternion()
clear all

% Goal Coordinate of End Effector
global xg xobs r_obs rho_0 xg2 xg3
global quat_goal
global K1 K2
global T1 T2 T T3
global K_quat
global s_sym phi_sym kappa_sym;
syms s_sym phi_sym kappa_sym;
global s_sym2 phi_sym2 kappa_sym2;
syms s_sym2 phi_sym2 kappa_sym2;
global s_sym3 phi_sym3 kappa_sym3;
syms s_sym3 phi_sym3 kappa_sym3;
global xb yb zb alphab betab gammab;
syms xb yb zb alphab betab gammab;
global rd0
global smin smax slim
global K1_s K2_s
global JacobianVelocity_mF JacobianVelocityMid_mF JacobianVelocityBot_mF JacOmega_mF
global position1_mF position2_mF position3_mF
global u_obs T_mF
global max_potent min_potent lim_potent
global max_field min_field lim_field

max_potent = zeros(3,1);
min_potent = 500*ones(3,1);
lim_potent = 0.2;
max_field = zeros(3,1);
min_field = 500*ones(3,1);
lim_field = 0.2;

u_obs = 0.01;
smin = 0.01; smax = 0.3; slim = 0.05;
K2_s = 3;
%xg3 = [0.2; 0.2; 0.2];
xg2 = [0.15; 0.15; -0.1];
xg = [0.02; 0.1; 0.1];
%xg2 = 0.25*[0.5; 0.5; 0.5];
% xobs = 10*[    0.08 0 0.7;
%             0.09 0 1.1];
K_quat = 1;

K1 = 1;
K2 = 0.001;
r_obs = 0;
rho_0 = 0.05;

inc = 1;

Tb = [ cos(alphab)*cos(betab)   cos(alphab)*sin(betab)*sin(gammab)-sin(alphab)*cos(gammab)  cos(alphab)*sin(betab)*cos(gammab)+sin(alphab)*sin(gammab)    xb;
    sin(alphab)*cos(betab)       sin(alphab)*sin(betab)*sin(gammab)+cos(alphab)*cos(gammab)   sin(alphab)*sin(betab)*cos(gammab)-cos(alphab)*sin(gammab)    yb;
    -sin(betab)               cos(betab)*sin(gammab)          cos(betab)*cos(gammab)             zb;
    0                           0          0                        1];

T1 = [ cos(phi_sym)*cos(kappa_sym*s_sym)   -sin(phi_sym)  cos(phi_sym)*sin(kappa_sym*s_sym)    cos(phi_sym)*(1-cos(kappa_sym*s_sym))/kappa_sym;
    sin(phi_sym)*cos(kappa_sym*s_sym)       cos(phi_sym)   sin(phi_sym)*sin(kappa_sym*s_sym)    sin(phi_sym)*(1-cos(kappa_sym*s_sym))/kappa_sym;
    -sin(kappa_sym*s_sym)               0          cos(kappa_sym*s_sym)             sin(kappa_sym*s_sym)/kappa_sym;
    0                           0          0                        1];

% Initialization
for i=1:3
s(inc) = 0.2;
phi(inc) = 0;

if(i==2) kappa(inc) = 0.5;
else kappa(inc) = -0.5;
end

Tw1(:,:,inc) = subs(T1, [kappa_sym; phi_sym; s_sym], [kappa(inc); phi(inc); s(inc)]);
inc=inc+1;
end
xb0 = 0; yb0 = 0; zb0 = 0; alphab0 = 0; betab0 = 0; gammab0 = 0;
Tb0 = subs(Tb, [xb yb zb alphab betab gammab], [xb0 yb0 zb0 alphab0 betab0 gammab0]);

T2 = subs(T1, [kappa_sym; phi_sym; s_sym], [kappa_sym2; phi_sym2; s_sym2]);
T3 = subs(T1, [kappa_sym; phi_sym; s_sym], [kappa_sym3; phi_sym3; s_sym3]);
T = Tb*T1*T2*T3;

% Initial Condition
Twu = Tb0*Tw1(:,:,1)*Tw1(:,:,2)*Tw1(:,:,3);
Twm = Tb0*Tw1(:,:,1)*Tw1(:,:,2);
Twb = Tb0*Tw1(:,:,1);
rd0 = [Twb(1:3,4); Twm(1:3,4); Twu(1:3,4)];

sc0 = [xb0 yb0 zb0 alphab0 betab0 gammab0 kappa(1) phi(1)  s(1)  kappa(2)  phi(2)  s(2)   kappa(3) phi(3)  s(3)];

% Initial Quaternion
%T_now = subs(T, [kappa_sym; phi_sym; s_sym; kappa_sym2; phi_sym2; s_sym2; kappa_sym3; phi_sym3; s_sym3], [kappa(1); phi(1); s(1); kappa(2); phi(2); s(2); kappa(3); phi(3); s(3)]);
T_mF = matlabFunction(T);
T_now = T_mF(alphab0, betab0, gammab0, kappa(1),kappa(2),kappa(3),phi(1),phi(2),phi(3),s(1),s(2),s(3), xb0, yb0, zb0);

R = T_now(1:3,1:3);
quat0(1) = 0.5*sqrt(1+R(1,1)+R(2,2)+R(3,3));
quat0(2) = (R(3,2)-R(2,3))/(4*quat0(1));
quat0(3) = (R(1,3)-R(3,1))/(4*quat0(1));
quat0(4) = (R(2,1)-R(1,2))/(4*quat0(1));

% % Angle-Axis to Quad
% wx = -1; wy = -1; wz = 1; theta = pi/2;
% quat_goal = [cos(theta/2);  wx*sin(theta/2);    wy*sin(theta/2);    wz*sin(theta/2)]
%quat_goal = [0.18;  -0.7;    0.58;    0.36];
quat_goal = quat0;

T2_0 = Tb*T1*T2;
T1_0 = Tb*T1;
position3 = [T(1:3,4)];
position2 = [T2_0(1:3,4)];
position1 = [T1_0(1:3,4)];

position3_mF = matlabFunction(position3);
position2_mF = matlabFunction(position2);
position1_mF = matlabFunction(position1);

% JacOmega11 = diff(T(3,1),kappa_sym)*T(2,1)+diff(T(3,2),kappa_sym)*T(2,2)+diff(T(3,3),kappa_sym)*T(2,3);
% JacOmega21 = diff(T(1,1),kappa_sym)*T(3,1)+diff(T(1,2),kappa_sym)*T(3,2)+diff(T(1,3),kappa_sym)*T(3,3);
% JacOmega31 = diff(T(2,1),kappa_sym)*T(1,1)+diff(T(2,2),kappa_sym)*T(1,2)+diff(T(2,3),kappa_sym)*T(1,3);
% JacOmega12 = diff(T(3,1),phi_sym)*T(2,1)+diff(T(3,2),phi_sym)*T(2,2)+diff(T(3,3),phi_sym)*T(2,3);
% JacOmega22 = diff(T(1,1),phi_sym)*T(3,1)+diff(T(1,2),phi_sym)*T(3,2)+diff(T(1,3),phi_sym)*T(3,3);
% JacOmega32 = diff(T(2,1),phi_sym)*T(1,1)+diff(T(2,2),phi_sym)*T(1,2)+diff(T(2,3),phi_sym)*T(1,3);
% JacOmega13 = diff(T(3,1),s_sym)*T(2,1)+diff(T(3,2),s_sym)*T(2,2)+diff(T(3,3),s_sym)*T(2,3);
% JacOmega23 = diff(T(1,1),s_sym)*T(3,1)+diff(T(1,2),s_sym)*T(3,2)+diff(T(1,3),s_sym)*T(3,3);
% JacOmega33 = diff(T(2,1),s_sym)*T(1,1)+diff(T(2,2),s_sym)*T(1,2)+diff(T(2,3),s_sym)*T(1,3);
% JacOmega14 = diff(T(3,1),kappa_sym2)*T(2,1)+diff(T(3,2),kappa_sym2)*T(2,2)+diff(T(3,3),kappa_sym2)*T(2,3);
% JacOmega24 = diff(T(1,1),kappa_sym2)*T(3,1)+diff(T(1,2),kappa_sym2)*T(3,2)+diff(T(1,3),kappa_sym2)*T(3,3);
% JacOmega34 = diff(T(2,1),kappa_sym2)*T(1,1)+diff(T(2,2),kappa_sym2)*T(1,2)+diff(T(2,3),kappa_sym2)*T(1,3);
% JacOmega15 = diff(T(3,1),phi_sym2)*T(2,1)+diff(T(3,2),phi_sym2)*T(2,2)+diff(T(3,3),phi_sym2)*T(2,3);
% JacOmega25 = diff(T(1,1),phi_sym2)*T(3,1)+diff(T(1,2),phi_sym2)*T(3,2)+diff(T(1,3),phi_sym2)*T(3,3);
% JacOmega35 = diff(T(2,1),phi_sym2)*T(1,1)+diff(T(2,2),phi_sym2)*T(1,2)+diff(T(2,3),phi_sym2)*T(1,3);
% JacOmega16 = diff(T(3,1),s_sym2)*T(2,1)+diff(T(3,2),s_sym2)*T(2,2)+diff(T(3,3),s_sym2)*T(2,3);
% JacOmega26 = diff(T(1,1),s_sym2)*T(3,1)+diff(T(1,2),s_sym2)*T(3,2)+diff(T(1,3),s_sym2)*T(3,3);
% JacOmega36 = diff(T(2,1),s_sym2)*T(1,1)+diff(T(2,2),s_sym2)*T(1,2)+diff(T(2,3),s_sym2)*T(1,3);
% JacOmega17 = diff(T(3,1),kappa_sym3)*T(2,1)+diff(T(3,2),kappa_sym3)*T(2,2)+diff(T(3,3),kappa_sym3)*T(2,3);
% JacOmega27 = diff(T(1,1),kappa_sym3)*T(3,1)+diff(T(1,2),kappa_sym3)*T(3,2)+diff(T(1,3),kappa_sym3)*T(3,3);
% JacOmega37 = diff(T(2,1),kappa_sym3)*T(1,1)+diff(T(2,2),kappa_sym3)*T(1,2)+diff(T(2,3),kappa_sym3)*T(1,3);
% JacOmega18 = diff(T(3,1),phi_sym3)*T(2,1)+diff(T(3,2),phi_sym3)*T(2,2)+diff(T(3,3),phi_sym3)*T(2,3);
% JacOmega28 = diff(T(1,1),phi_sym3)*T(3,1)+diff(T(1,2),phi_sym3)*T(3,2)+diff(T(1,3),phi_sym3)*T(3,3);
% JacOmega38 = diff(T(2,1),phi_sym3)*T(1,1)+diff(T(2,2),phi_sym3)*T(1,2)+diff(T(2,3),phi_sym3)*T(1,3);
% JacOmega19 = diff(T(3,1),s_sym3)*T(2,1)+diff(T(3,2),s_sym3)*T(2,2)+diff(T(3,3),s_sym3)*T(2,3);
% JacOmega29 = diff(T(1,1),s_sym3)*T(3,1)+diff(T(1,2),s_sym3)*T(3,2)+diff(T(1,3),s_sym3)*T(3,3);
% JacOmega39 = diff(T(2,1),s_sym3)*T(1,1)+diff(T(2,2),s_sym3)*T(1,2)+diff(T(2,3),s_sym3)*T(1,3);
% 
% JacOmega = [    JacOmega11 JacOmega12 JacOmega13 JacOmega14 JacOmega15 JacOmega16 JacOmega17 JacOmega18 JacOmega19;
%                 JacOmega21 JacOmega22 JacOmega23 JacOmega24 JacOmega25 JacOmega26 JacOmega27 JacOmega28 JacOmega29;
%                 JacOmega31 JacOmega32 JacOmega33 JacOmega34 JacOmega35 JacOmega36 JacOmega37 JacOmega38 JacOmega39];
% JacOmega_mF = matlabFunction(JacOmega);
% JacobianVelocity = [    diff(position1(1),kappa_sym)    diff(position1(1),phi_sym)  diff(position1(1),s_sym)   diff(position1(1),kappa_sym2)  diff(position1(1),phi_sym2)    diff(position1(1),s_sym2)   diff(position1(1),kappa_sym3)  diff(position1(1),phi_sym3)  diff(position1(1),s_sym3);
%                         diff(position1(2),kappa_sym)    diff(position1(2),phi_sym)  diff(position1(2),s_sym)   diff(position1(2),kappa_sym2)  diff(position1(2),phi_sym2)    diff(position1(2),s_sym2)   diff(position1(2),kappa_sym3)  diff(position1(2),phi_sym3)  diff(position1(2),s_sym3);
%                         diff(position1(3),kappa_sym)    diff(position1(3),phi_sym)  diff(position1(3),s_sym)   diff(position1(3),kappa_sym2)  diff(position1(3),phi_sym2)    diff(position1(3),s_sym2)   diff(position1(3),kappa_sym3)  diff(position1(3),phi_sym3)  diff(position1(3),s_sym3);
%                         diff(position2(1),kappa_sym)    diff(position2(1),phi_sym)  diff(position2(1),s_sym)   diff(position2(1),kappa_sym2)  diff(position2(1),phi_sym2)    diff(position2(1),s_sym2)   diff(position2(1),kappa_sym3)  diff(position2(1),phi_sym3)  diff(position2(1),s_sym3);
%                         diff(position2(2),kappa_sym)    diff(position2(2),phi_sym)  diff(position2(2),s_sym)   diff(position2(2),kappa_sym2)  diff(position2(2),phi_sym2)    diff(position2(2),s_sym2)   diff(position2(2),kappa_sym3)  diff(position2(2),phi_sym3)  diff(position2(2),s_sym3);
%                         diff(position2(3),kappa_sym)    diff(position2(3),phi_sym)  diff(position2(3),s_sym)   diff(position2(3),kappa_sym2)  diff(position2(3),phi_sym2)    diff(position2(3),s_sym2)   diff(position2(3),kappa_sym3)  diff(position2(3),phi_sym3)  diff(position2(3),s_sym3);
%                         diff(position3(1),kappa_sym)    diff(position3(1),phi_sym)  diff(position3(1),s_sym)   diff(position3(1),kappa_sym2)  diff(position3(1),phi_sym2)    diff(position3(1),s_sym2)   diff(position3(1),kappa_sym3)  diff(position3(1),phi_sym3)  diff(position3(1),s_sym3);
%                         diff(position3(2),kappa_sym)    diff(position3(2),phi_sym)  diff(position3(2),s_sym)   diff(position3(2),kappa_sym2)  diff(position3(2),phi_sym2)    diff(position3(2),s_sym2)   diff(position3(2),kappa_sym3)  diff(position3(2),phi_sym3)  diff(position3(2),s_sym3);
%                         diff(position3(3),kappa_sym)    diff(position3(3),phi_sym)  diff(position3(3),s_sym)   diff(position3(3),kappa_sym2)  diff(position3(3),phi_sym2)    diff(position3(3),s_sym2)   diff(position3(3),kappa_sym3)  diff(position3(3),phi_sym3)  diff(position3(3),s_sym3)];

JacobianVelocity = [    diff(position3(1),xb)    diff(position3(1),yb)  diff(position3(1),zb)   diff(position3(1),alphab)    diff(position3(1),betab)  diff(position3(1),gammab)   diff(position3(1),kappa_sym)    diff(position3(1),phi_sym)  diff(position3(1),s_sym)   diff(position3(1),kappa_sym2)  diff(position3(1),phi_sym2)    diff(position3(1),s_sym2)   diff(position3(1),kappa_sym3)  diff(position3(1),phi_sym3)  diff(position3(1),s_sym3);
                        diff(position3(2),xb)    diff(position3(2),yb)  diff(position3(2),zb)   diff(position3(2),alphab)    diff(position3(2),betab)  diff(position3(2),gammab)   diff(position3(2),kappa_sym)    diff(position3(2),phi_sym)  diff(position3(2),s_sym)   diff(position3(2),kappa_sym2)  diff(position3(2),phi_sym2)    diff(position3(2),s_sym2)   diff(position3(2),kappa_sym3)  diff(position3(2),phi_sym3)  diff(position3(2),s_sym3);
                        diff(position3(3),xb)    diff(position3(3),yb)  diff(position3(3),zb)   diff(position3(3),alphab)    diff(position3(3),betab)  diff(position3(3),gammab)   diff(position3(3),kappa_sym)    diff(position3(3),phi_sym)  diff(position3(3),s_sym)   diff(position3(3),kappa_sym2)  diff(position3(3),phi_sym2)    diff(position3(3),s_sym2)   diff(position3(3),kappa_sym3)  diff(position3(3),phi_sym3)  diff(position3(3),s_sym3)];
JacobianVelocity_mF = matlabFunction(JacobianVelocity);
JacobianVelocityMid = [ diff(position2(1),xb)    diff(position2(1),yb)  diff(position2(1),zb)   diff(position2(1),alphab)    diff(position2(1),betab)  diff(position2(1),gammab)   diff(position2(1),kappa_sym)    diff(position2(1),phi_sym)  diff(position2(1),s_sym)   diff(position2(1),kappa_sym2)  diff(position2(1),phi_sym2)    diff(position2(1),s_sym2)   diff(position2(1),kappa_sym3)  diff(position2(1),phi_sym3)  diff(position2(1),s_sym3);
                        diff(position2(2),xb)    diff(position2(2),yb)  diff(position2(2),zb)   diff(position2(2),alphab)    diff(position2(2),betab)  diff(position2(2),gammab)   diff(position2(2),kappa_sym)    diff(position2(2),phi_sym)  diff(position2(2),s_sym)   diff(position2(2),kappa_sym2)  diff(position2(2),phi_sym2)    diff(position2(2),s_sym2)   diff(position2(2),kappa_sym3)  diff(position2(2),phi_sym3)  diff(position2(2),s_sym3);
                        diff(position2(3),xb)    diff(position2(3),yb)  diff(position2(3),zb)   diff(position2(3),alphab)    diff(position2(3),betab)  diff(position2(3),gammab)   diff(position2(3),kappa_sym)    diff(position2(3),phi_sym)  diff(position2(3),s_sym)   diff(position2(3),kappa_sym2)  diff(position2(3),phi_sym2)    diff(position2(3),s_sym2)   diff(position2(3),kappa_sym3)  diff(position2(3),phi_sym3)  diff(position2(3),s_sym3)];
JacobianVelocityMid_mF = matlabFunction(JacobianVelocityMid);
JacobianVelocityBot = [ diff(position1(1),xb)    diff(position1(1),yb)  diff(position1(1),zb)   diff(position1(1),alphab)    diff(position1(1),betab)  diff(position1(1),gammab)   diff(position1(1),kappa_sym)    diff(position1(1),phi_sym)  diff(position1(1),s_sym)   diff(position1(1),kappa_sym2)  diff(position1(1),phi_sym2)    diff(position1(1),s_sym2)   diff(position1(1),kappa_sym3)  diff(position1(1),phi_sym3)  diff(position1(1),s_sym3);
                        diff(position1(2),xb)    diff(position1(2),yb)  diff(position1(2),zb)   diff(position1(2),alphab)    diff(position1(2),betab)  diff(position1(2),gammab)   diff(position1(2),kappa_sym)    diff(position1(2),phi_sym)  diff(position1(2),s_sym)   diff(position1(2),kappa_sym2)  diff(position1(2),phi_sym2)    diff(position1(2),s_sym2)   diff(position1(2),kappa_sym3)  diff(position1(2),phi_sym3)  diff(position1(2),s_sym3);
                        diff(position1(3),xb)    diff(position1(3),yb)  diff(position1(3),zb)   diff(position1(3),alphab)    diff(position1(3),betab)  diff(position1(3),gammab)   diff(position1(3),kappa_sym)    diff(position1(3),phi_sym)  diff(position1(3),s_sym)   diff(position1(3),kappa_sym2)  diff(position1(3),phi_sym2)    diff(position1(3),s_sym2)   diff(position1(3),kappa_sym3)  diff(position1(3),phi_sym3)  diff(position1(3),s_sym3)];
JacobianVelocityBot_mF = matlabFunction(JacobianVelocityBot);
tspan = 0:1e-1:60;
%[to,xo] = ode45(@InverseJacobian,tspan,sc0);
[to,xo] = ode15s(@InverseJacobian,tspan,sc0);
%options = odeset('NonNegative', 3, 'NonNegative', 6, 'NonNegative', 9);
%[to,xo] = ode45(@InverseJacobian,tspan,sc0, options);

[is,js] = size(to)
pose1_mF = matlabFunction(T1_0);
pose2_mF = matlabFunction(T2_0);
pose3_mF = matlabFunction(T);
for i=1:is
    %pose1 = subs(T1, [kappa_sym; phi_sym; s_sym], [xo(i,1:3)]);

    pose1 = pose1_mF(xo(i,4),xo(i,5),xo(i,6), xo(i,7),xo(i,8),xo(i,9), xo(i,1),xo(i,2),xo(i,3));
    x1(i) = pose1(1,4);
    y1(i) = pose1(2,4);
    z1(i) = pose1(3,4);
    %pose2 = subs(T1*T2, [kappa_sym; phi_sym; s_sym; kappa_sym2; phi_sym2; s_sym2], [xo(i,1:6)]);
    
    pose2 = pose2_mF(xo(i,4),xo(i,5),xo(i,6), xo(i,7),xo(i,10),xo(i,8),xo(i,11),xo(i,9),xo(i,12), xo(i,1),xo(i,2),xo(i,3));
    x2(i) = pose2(1,4);
    y2(i) = pose2(2,4);
    z2(i) = pose2(3,4);
    %pose3 = subs(T, [kappa_sym; phi_sym; s_sym; kappa_sym2; phi_sym2; s_sym2; kappa_sym3; phi_sym3; s_sym3], [xo(i,1:9)]);
    
    pose3 = pose3_mF(xo(i,4),xo(i,5),xo(i,6), xo(i,7),xo(i,10),xo(i,13),xo(i,8),xo(i,11),xo(i,14),xo(i,9),xo(i,12),xo(i,15), xo(i,1),xo(i,2),xo(i,3));
    x3(i) = pose3(1,4);
    y3(i) = pose3(2,4);
    z3(i) = pose3(3,4);
end

in = 1;
for i=0:0.05:xo(is,9)
    vector3D1(in,:) = [cos(xo(is,8))*(1-cos(xo(is,7)*i))/xo(is,7); sin(xo(is,8))*(1-cos(xo(is,7)*i))/xo(is,7);sin(xo(is,7)*i)/xo(is,7)];
    in = in+1;
end
in = 1;
%Homo_mF = matlabFunction(T1);
for i=0:0.05:xo(is,12)
    %Homo = subs(T1,[kappa_sym; phi_sym; s_sym], [xo(is,1:3)]);
    
    Homo = pose1_mF(xo(is,4),xo(is,5),xo(is,6), xo(is,7),xo(is,8),xo(is,9), xo(is,1),xo(is,2),xo(is,3));
   jumper = [cos(xo(is,11))*(1-cos(xo(is,10)*i))/xo(is,10); sin(xo(is,11))*(1-cos(xo(is,10)*i))/xo(is,10);sin(xo(is,10)*i)/xo(is,10); 1];
   jumper2 = Homo*jumper;
    vector3D2(in,:) = jumper2(1:3);
    in = in+1;
end
in = 1;
%Homo_mF = matlabFunction(T1*T2);
for i=0:0.05:xo(is,15)
    %Homo = subs(T1*T2,[kappa_sym; phi_sym; s_sym;    kappa_sym2; phi_sym2; s_sym2], [xo(is,1:6)]);
    
    Homo = pose2_mF(xo(is,4),xo(is,5),xo(is,6), xo(is,7),xo(is,10),xo(is,8),xo(is,11),xo(is,9),xo(is,12), xo(is,1),xo(is,2),xo(is,3));
   jumper = [cos(xo(is,14))*(1-cos(xo(is,13)*i))/xo(is,13); sin(xo(is,14))*(1-cos(xo(is,13)*i))/xo(is,13);sin(xo(is,13)*i)/xo(is,13); 1];
   jumper2 = Homo*jumper;
    vector3D3(in,:) = jumper2(1:3);
    in = in+1;
end

% Calculate the quaternion
T_mF = matlabFunction(T);
for i=1:is
    %T_now = subs(T, [kappa_sym; phi_sym; s_sym; kappa_sym2; phi_sym2; s_sym2; kappa_sym3; phi_sym3; s_sym3], [xo(i,1:9)]);
    T_now = T_mF(xo(i,4),xo(i,5),xo(i,6), xo(i,7),xo(i,10),xo(i,13),xo(i,8),xo(i,11),xo(i,14),xo(i,9),xo(i,12),xo(i,15), xo(i,1),xo(i,2),xo(i,3));
    R = T_now(1:3,1:3);
    quat(i,1) = 0.5*sqrt(1+R(1,1)+R(2,2)+R(3,3));
    quat(i,2) = (R(3,2)-R(2,3))/(4*quat(i,1));
    quat(i,3) = (R(1,3)-R(3,1))/(4*quat(i,1));
    quat(i,4) = (R(2,1)-R(1,2))/(4*quat(i,1));
    quat2(i,:) = SpinCalc('DCMtoQ',R,0.01,0);
    quat_mag(i,:) = quat(i,1)^2+quat(i,2)^2+quat(i,3)^2+quat(i,4)^2;
    beta(i) = atan2(-R(3,1),sqrt(R(1,1)^2+R(2,1)^2));
    alpha(i) = atan2(R(2,1)/cos(beta(i)),R(1,1)/cos(beta(i)));
    gamma(i) = atan2(R(3,2)/cos(beta(i)),R(3,3)/cos(beta(i)));
end

figure(1)
subplot(3,3,1); plot(to,x1);xlabel('t'); ylabel('x1 [m]'); 
subplot(3,3,2); plot(to,y1);xlabel('t'); ylabel('y1 [m]'); 
subplot(3,3,3); plot(to,z1);xlabel('t'); ylabel('z1 [m]'); 
subplot(3,3,4); plot(to,x2);xlabel('t'); ylabel('x2 [m]'); 
subplot(3,3,5); plot(to,y2);xlabel('t'); ylabel('y2 [m]'); 
subplot(3,3,6); plot(to,z2);xlabel('t'); ylabel('z2 [m]'); 
subplot(3,3,7); plot(to,x3);xlabel('t'); ylabel('x3 [m]'); 
subplot(3,3,8); plot(to,y3);xlabel('t'); ylabel('y3 [m]'); 
subplot(3,3,9); plot(to,z3);xlabel('t'); ylabel('z3 [m]'); 

figure(2)
subplot(3,1,1); plot3(x1,y1,z1);xlabel('x1'); ylabel('y1'); zlabel('z1'); title('Path End Effector 1'); 
subplot(3,1,2); plot3(x2,y2,z2);xlabel('x2'); ylabel('y2'); zlabel('z2'); title('Path End Effector 2'); 
subplot(3,1,3); plot3(x3,y3,z3);xlabel('x3'); ylabel('y3'); zlabel('z3'); title('Path End Effector 3'); 

figure(3)
subplot(3,3,1); plot(to,xo(:,7));xlabel('t'); ylabel('k1'); title('Curvature 1'); 
subplot(3,3,2); plot(to,xo(:,8));xlabel('t'); ylabel('phi1'); title('Angle 1');
subplot(3,3,3); plot(to,xo(:,9));xlabel('t'); ylabel('s1'); title('Segment Length 1'); 
subplot(3,3,4); plot(to,xo(:,10));xlabel('t'); ylabel('k2'); title('Curvature 2'); 
subplot(3,3,5); plot(to,xo(:,11));xlabel('t'); ylabel('phi2'); title('Angle 2');
subplot(3,3,6); plot(to,xo(:,12));xlabel('t'); ylabel('s2'); title('Segment Length 2'); 
subplot(3,3,7); plot(to,xo(:,13));xlabel('t'); ylabel('k3'); title('Curvature 3'); 
subplot(3,3,8); plot(to,xo(:,14));xlabel('t'); ylabel('phi3'); title('Angle 3');
subplot(3,3,9); plot(to,xo(:,15));xlabel('t'); ylabel('s3'); title('Segment Length 3'); 


figure(4)
subplot(2,4,1); plot(to, quat(:,1)); title('E0'); 
subplot(2,4,2); plot(to, quat(:,2)); title('E1'); 
subplot(2,4,3); plot(to, quat(:,3)); title('E2'); 
subplot(2,4,4); plot(to, quat(:,4)); title('E3'); 
subplot(2,4,5); plot(to, alpha); title('alpha'); 
subplot(2,4,6); plot(to, beta); title('beta'); 
subplot(2,4,7); plot(to, gamma); title('gamma'); 
subplot(2,4,8); plot(to, quat_mag); title('Magnitude Quaternion'); 

xobs = [-0.10 -0.006 0.48;
        0.20 0.015 0.21;
        0 -0.5 0.05];
for k=1:is
    xobs_plot(k,:,:) = xobs + [u_obs*to(k) 0 0; -u_obs*to(k) 0 0; 0 u_obs*to(k) 0];
end
figure(5)
subplot(3,3,1); plot(to,xobs_plot(:,1,1));xlabel('t'); ylabel('x obs');
subplot(3,3,2); plot(to,xobs_plot(:,1,2));xlabel('t'); ylabel('y obs');
subplot(3,3,3); plot(to,xobs_plot(:,1,3));xlabel('t'); ylabel('z obs');
subplot(3,3,4); plot(to,xobs_plot(:,2,1));xlabel('t'); ylabel('x obs');
subplot(3,3,5); plot(to,xobs_plot(:,2,2));xlabel('t'); ylabel('y obs');
subplot(3,3,6); plot(to,xobs_plot(:,2,3));xlabel('t'); ylabel('z obs');
subplot(3,3,7); plot(to,xobs_plot(:,3,1));xlabel('t'); ylabel('x obs');
subplot(3,3,8); plot(to,xobs_plot(:,3,2));xlabel('t'); ylabel('y obs');
subplot(3,3,9); plot(to,xobs_plot(:,3,3));xlabel('t'); ylabel('z obs');

%axis normal
vector3D1 = [];vector3D2 = [];vector3D3 = [];
in = 1;
for i=0:0.05:xo(1,9)
    vector3D1(in,:) = [cos(xo(1,8))*(1-cos(xo(1,7)*i))/xo(1,7); sin(xo(1,8))*(1-cos(xo(1,7)*i))/xo(1,7);sin(xo(1,7)*i)/xo(1,7)];
    in = in+1;
end

in = 1;
for i=0:0.05:xo(1,12)
    %Homo = subs(T1,[kappa_sym; phi_sym; s_sym], [xo(1,1:3)]);
    Homo = pose1_mF(xo(1,4),xo(1,5),xo(1,6), xo(1,7),xo(1,8),xo(1,9), xo(1,1),xo(1,2),xo(1,3));
   jumper = [cos(xo(1,11))*(1-cos(xo(1,10)*i))/xo(1,10); sin(xo(1,11))*(1-cos(xo(1,10)*i))/xo(1,10);sin(xo(1,10)*i)/xo(1,10); 1];
   jumper2 = Homo*jumper;
    vector3D2(in,:) = jumper2(1:3);
    in = in+1;
end

in = 1;
for i=0:0.05:xo(1,15)
    %Homo = subs(T1*T2,[kappa_sym; phi_sym; s_sym;    kappa_sym2; phi_sym2; s_sym2], [xo(1,1:6)]);
    Homo = pose2_mF(xo(1,4),xo(1,5),xo(1,6), xo(1,7),xo(1,10),xo(1,8),xo(1,11),xo(1,9),xo(1,12), xo(1,1),xo(1,2),xo(1,3));
   jumper = [cos(xo(1,14))*(1-cos(xo(1,13)*i))/xo(1,13); sin(xo(1,14))*(1-cos(xo(1,13)*i))/xo(1,13);sin(xo(1,13)*i)/xo(1,13); 1];
   jumper2 = Homo*jumper;
    vector3D3(in,:) = jumper2(1:3);
    in = in+1;
end
%xobs = [0.07; 0; 0.45];
xg3 = rd0(7:9);
max_potent
min_potent
max_field
min_field
figure(6)
graph = plot3(vector3D1(:,1),vector3D1(:,2),vector3D1(:,3),vector3D2(:,1),vector3D2(:,2),vector3D2(:,3),vector3D3(:,1),vector3D3(:,2),vector3D3(:,3),vector3D3(1,1),vector3D3(1,2),vector3D3(1,3),'or', vector3D2(1,1),vector3D2(1,2),vector3D2(1,3),'ob', vector3D1(1,1),vector3D1(1,2),vector3D1(1,3),'og',xg3(1),xg3(2),xg3(3),'ob',xobs(1,1),xobs(1,2),xobs(1,3),'ok',xobs(2,1),xobs(2,2),xobs(2,3),'or',xobs(3,1),xobs(3,2),xobs(3,3),'ob');
xlim([-0.5,0.25]);ylim([-0.2,0.1]);zlim([-0.01,0.65]);
xlabel('x'); ylabel('y');zlabel('z');
az = 45;
el = 45;
view(az, el);

for j = 1:2:is
    in = 1;
    vector3D1=[];
    vector3D2=[];
    vector3D3=[];
    
    for i=0:0.05:xo(j,9)
        vector3D1(in,:) = [cos(xo(j,8))*(1-cos(xo(j,7)*i))/xo(j,7); sin(xo(j,8))*(1-cos(xo(j,7)*i))/xo(j,7);sin(xo(j,7)*i)/xo(j,7)];
        in = in+1;
    end
    set(graph(1),'XData',vector3D1(:,1),'YData',vector3D1(:,2),'ZData',vector3D1(:,3));
    set(graph(6),'XData',vector3D1(in-1,1),'YData',vector3D1(in-1,2),'ZData',vector3D1(in-1,3));
    
    in=1;
    for i=0:0.05:xo(j,12)
        %Homo = subs(T1,[kappa_sym; phi_sym; s_sym], [xo(j,1:3)]);
        Homo = pose1_mF(xo(j,4),xo(j,5),xo(j,6), xo(j,7),xo(j,8),xo(j,9), xo(j,1),xo(j,2),xo(j,3));
        jumper = [cos(xo(j,11))*(1-cos(xo(j,10)*i))/xo(j,10); sin(xo(j,11))*(1-cos(xo(j,10)*i))/xo(j,10);sin(xo(j,10)*i)/xo(j,10); 1];
        jumper2 = Homo*jumper;
        vector3D2(in,:) = jumper2(1:3);
        in = in+1;
    end
    set(graph(2),'XData',vector3D2(:,1),'YData',vector3D2(:,2),'ZData',vector3D2(:,3));
    set(graph(5),'XData',vector3D2(in-1,1),'YData',vector3D2(in-1,2),'ZData',vector3D2(in-1,3));
    
    in = 1;
    for i=0:0.05:xo(j,15)
        %Homo = subs(T1*T2,[kappa_sym; phi_sym; s_sym;    kappa_sym2; phi_sym2; s_sym2], [xo(j,1:6)]);
        Homo = pose2_mF(xo(j,4),xo(j,5),xo(j,6), xo(j,7),xo(j,10),xo(j,8),xo(j,11),xo(j,9),xo(j,12), xo(j,1),xo(j,2),xo(j,3));
        jumper = [cos(xo(j,14))*(1-cos(xo(j,13)*i))/xo(j,13); sin(xo(j,14))*(1-cos(xo(j,13)*i))/xo(j,13);sin(xo(j,13)*i)/xo(j,13); 1];
        jumper2 = Homo*jumper;
        vector3D3(in,:) = jumper2(1:3);
        in = in+1;
    end
    set(graph(3),'XData',vector3D3(:,1),'YData',vector3D3(:,2),'ZData',vector3D3(:,3));
    set(graph(4),'XData',vector3D3(in-1,1),'YData',vector3D3(in-1,2),'ZData',vector3D3(in-1,3));
    
    xobs = [-0.10+u_obs*to(j) -0.006 0.48;
        0.20-u_obs*to(j) 0.015 0.21;
        0 -0.5+u_obs*to(j) 0.05];
    
    set(graph(8),'XData',xobs(1,1),'YData',xobs(1,2),'ZData',xobs(1,3));
    set(graph(9),'XData',xobs(2,1),'YData',xobs(2,2),'ZData',xobs(2,3));
    set(graph(10),'XData',xobs(3,1),'YData',xobs(3,2),'ZData',xobs(3,3));
    xg3 = rd0(7:9);%+[0;0;0.01*to(j)];
    set(graph(7),'XData',xg3(1),'YData',xg3(2),'ZData',xg3(3));
    
    drawnow;
    pause(0.1);
end

function dx = InverseJacobian(t,x)
global xg xobs r_obs rho_0
global xg2 quat_goal xg3
global K1 K2
global T1 T2 T T3
global s_sym phi_sym kappa_sym;
global s_sym2 phi_sym2 kappa_sym2;
global s_sym3 phi_sym3 kappa_sym3;
global K_quat
global rd0
global smin smax slim
global K1_s K2_s
global JacobianVelocity_mF JacobianVelocityMid_mF JacobianVelocityBot_mF JacOmega_mF
global position1_mF position2_mF position3_mF
global u_obs T_mF
global max_potent min_potent lim_potent
global max_field min_field lim_field

xg3 = [rd0(7); rd0(8); rd0(9)];%+0.01*t];
% xg2 = [rd0(4); rd0(5); rd0(6)+0.01*t];
% xg = [rd0(7); rd0(8); rd0(9)+0.01*t];

xobs = [-0.10+u_obs*t -0.006 0.48;
        0.20-u_obs*t 0.015 0.21;
        0 -0.5+u_obs*t 0.05];%-0.01*t];

% rd = [  subs(position1, [kappa_sym phi_sym s_sym kappa_sym2 phi_sym2 s_sym2 kappa_sym3 phi_sym3 s_sym3], [x(1:9)']);
%         subs(position2, [kappa_sym phi_sym s_sym kappa_sym2 phi_sym2 s_sym2 kappa_sym3 phi_sym3 s_sym3], [x(1:9)']);
%         subs(position3, [kappa_sym phi_sym s_sym kappa_sym2 phi_sym2 s_sym2 kappa_sym3 phi_sym3 s_sym3], [x(1:9)'])]; 

rd = [  position1_mF(x(4),x(5),x(6),x(7),x(8),x(9),x(1),x(2),x(3));
        position2_mF(x(4),x(5),x(6),x(7),x(10),x(8),x(11),x(9),x(12),x(1),x(2),x(3));
        position3_mF(x(4),x(5),x(6),x(7),x(10),x(13),x(8),x(11),x(14),x(9),x(12),x(15),x(1),x(2),x(3))];

kappa_ode = x(7);
phi_ode = x(8);
s_ode = x(9);
kappa_ode2 = x(10);
phi_ode2 = x(11);
s_ode2 = x(12);
kappa_ode3 = x(13);
phi_ode3 = x(14);
s_ode3 = x(15);

% Attractive and Repulsive Potential Field
error = rd(1:3) - xg;
error2 = rd(4:6) - xg2;
error3 = rd(7:9) - xg3;
%vectorobs=rd(1:3)-xobs;
%vectorobs2=rd(4:6)-xobs;
%vectorobs3=rd(7:9)-xobs;

%rho = sqrt(vectorobs'*vectorobs)-r_obs;
%rho2 = sqrt(vectorobs2'*vectorobs2)-r_obs;
%rho3 = sqrt(vectorobs3'*vectorobs3)-r_obs;

% if(rho<rho_0)
%    field = -K1*error+K2*vectorobs*(1/rho - 1/rho_0)/rho^3;
%    %field = K2*vectorobs*(1/rho - 1/rho_0)/rho^3;
%    %disp('obs')
% else
    field = -K1*error;
%     %field = zeros(2,1);
%      %   disp('clear')
% end
% if(rho2<rho_0)
%    field2 = -K1*error2+K2*vectorobs2*(1/rho2 - 1/rho_0)/rho2^3;
%    %field2 = K2*vectorobs2*(1/rho2 - 1/rho_0)/rho2^3;
%    %disp('obs')
% else
     field2 = -K1*error2;
%     %field2 = zeros(3,1);
%     %disp('clear')
% end
%if(rho3<rho_0)
%  field3 = -K1*error3+K2*vectorobs3*(1/rho3 - 1/rho_0)/rho3^3;
%else
    field3 = -K1*error3;
    if(field3>max_field)
        max_field = field3;
    end
    if(field3<min_field)
        min_field = field3;
    end
%end
%drd = [field; field2; field3];

%drd = -K1*error2;
drd = field3;

% Potential Field in Quaternion Space
%R = subs(T(1:3,1:3),[kappa_sym phi_sym s_sym kappa_sym2 phi_sym2 s_sym2 kappa_sym3 phi_sym3 s_sym3], [x(1:9)']);
T_dummy = T_mF(x(4),x(5),x(6),x(7),x(10),x(13),x(8),x(11),x(14),x(9),x(12),x(15),x(1),x(2),x(3));
R = T_dummy(1:3,1:3);
    quat_0 = 0.5*sqrt(1+R(1,1)+R(2,2)+R(3,3));
    quat_1 = (R(3,2)-R(2,3))/(4*quat_0);
    quat_2 = (R(1,3)-R(3,1))/(4*quat_0);
    quat_3 = (R(2,1)-R(1,2))/(4*quat_0);
% quat_0 = x(10);
% quat_1 = x(11);
% quat_2 = x(12);
% quat_3 = x(13);
error_quat_0 = quat_0 - quat_goal(1);
error_quat_1 = quat_1 - quat_goal(2);
error_quat_2 = quat_2 - quat_goal(3);
error_quat_3 = quat_3 - quat_goal(4);
drd_quat = [-K_quat*error_quat_0; -K_quat*error_quat_1; -K_quat*error_quat_2; -K_quat*error_quat_3];

OmegatoQuat = 0.5*[ -quat_1 -quat_2 -quat_3;
                    quat_0 quat_3 -quat_2;
                    -quat_3 quat_0 quat_1;
                    quat_2 -quat_1 quat_0];


                                              
%JacobianVelocityNow = subs(JacobianVelocity,[kappa_sym; phi_sym; s_sym; kappa_sym2; phi_sym2; s_sym2; kappa_sym3; phi_sym3; s_sym3], [kappa_ode; phi_ode; s_ode; kappa_ode2; phi_ode2; s_ode2; kappa_ode3; phi_ode3; s_ode3]);
JacobianVelocityNow = JacobianVelocity_mF(x(4),x(5),x(6),x(7),x(10),x(13),x(8),x(11),x(14),x(9),x(12),x(15));


%JacobianOmegaNow = subs(JacOmega,[kappa_sym; phi_sym; s_sym; kappa_sym2; phi_sym2; s_sym2; kappa_sym3; phi_sym3; s_sym3], [kappa_ode; phi_ode; s_ode; kappa_ode2; phi_ode2; s_ode2; kappa_ode3; phi_ode3; s_ode3]);
% JacobianOmegaNow = JacOmega_mF(x(1),x(4),x(7),x(2),x(5),x(8),x(3),x(6),x(9));


%MatrixComplete = [JacobianVelocityNow; OmegatoQuat*JacobianOmegaNow];

%dx = pinv(MatrixComplete)*[drd; drd_quat];
%dx = pinv(JacobianVelocityNow)*drd;

% Point subject to potential coordinate and its Jacobian
y=[];
Jacobi = [];
num = 2;

for i=1:num
%     y(i,:) = subs(position3, [kappa_sym; phi_sym; s_sym; kappa_sym2; phi_sym2; s_sym2; kappa_sym3; phi_sym3; s_sym3], [kappa_ode; phi_ode; s_ode; kappa_ode2; phi_ode2; s_ode2; kappa_ode3; phi_ode3; s_ode3-(i-1)*s_ode3/num]);
%     Jacobi(:,:,i) = subs(JacobianVelocity, [kappa_sym; phi_sym; s_sym; kappa_sym2; phi_sym2; s_sym2; kappa_sym3; phi_sym3; s_sym3], [kappa_ode; phi_ode; s_ode; kappa_ode2; phi_ode2; s_ode2; kappa_ode3; phi_ode3; s_ode3-(i-1)*s_ode3/num]);
    y(i,:) = position3_mF(x(4),x(5),x(6),x(7),x(10),x(13),x(8),x(11),x(14),x(9),x(12),x(15)-(i-1)*x(15)/num,x(1),x(2),x(3));
    Jacobi(:,:,i) = JacobianVelocity_mF(x(4),x(5),x(6),x(7),x(10),x(13),x(8),x(11),x(14),x(9),x(12),x(15)-(i-1)*x(15)/num);
end
[a,b] = size(y);

for i=1:num
%     y(a+i,:) = subs(position2, [kappa_sym; phi_sym; s_sym; kappa_sym2; phi_sym2; s_sym2], [kappa_ode; phi_ode; s_ode; kappa_ode2; phi_ode2; s_ode2-(i-1)*s_ode2/num]);
%     Jacobi(:,:,a+i) = subs(JacobianVelocityMid, [kappa_sym; phi_sym; s_sym; kappa_sym2; phi_sym2; s_sym2], [kappa_ode; phi_ode; s_ode; kappa_ode2; phi_ode2; s_ode2-(i-1)*s_ode2/num]);
    y(a+i,:) = position2_mF(x(4),x(5),x(6),x(7),x(10),x(8),x(11),x(9),x(12)-(i-1)*x(12)/num,x(1),x(2),x(3));
    Jacobi(:,:,a+i) = JacobianVelocityMid_mF(x(4),x(5),x(6),x(7),x(10),x(8),x(11),x(9),x(12)-(i-1)*x(12)/num);
end
[a,b] = size(y);

for i=1:num
%     y(a+i,:) = subs(position1, [kappa_sym;  phi_sym; s_sym], [kappa_ode; phi_ode; s_ode-(i-1)*s_ode/num]);
%     Jacobi(:,:,a+i) = subs(JacobianVelocityBot, [kappa_sym; phi_sym; s_sym], [kappa_ode; phi_ode; s_ode-(i-1)*s_ode/num]);
    y(a+i,:) = position1_mF(x(4),x(5),x(6),x(7),x(8),x(9)-(i-1)*x(9)/num,x(1),x(2),x(3));
    Jacobi(:,:,a+i) = JacobianVelocityBot_mF(x(4),x(5),x(6),x(7),x(8),x(9)-(i-1)*x(9)/num);
end

[a,b] = size(y);

[obs_num, dummy] = size(xobs);

sum = zeros(15,1);
for j=1:obs_num
for i=1:a
    vector_obs = y(i,1:3)' - xobs(j,:)';
    rho = sqrt(vector_obs'*vector_obs);

    if(rho<rho_0)
        velocity(:,i) = K2*vector_obs*(1/rho - 1/rho_0)/rho^3;
        if(velocity(:,i)>max_potent)
            max_potent = velocity(:,i);
        end
        if(velocity(:,i)<min_potent)
            min_potent = velocity(:,i);
        end

    else
        velocity(:,i) = zeros(3,1);
    end
    sum = sum + pinv(Jacobi(:,:,i))*velocity(:,i);
end
end
% Potential Field in configuration space
% vectors = [x(3); x(6); x(9)];
% 
% for i=1:3
%     vectormin = vectors(i)-smin;
%     vectormax = vectors(i)-smax;
%     rhomin = sqrt(vectormin'*vectormin);
%     rhomax = sqrt(vectormax'*vectormax);
%     
%     if(rhomin<slim)
%         field_s(i) = K2_s*vectormin*(1/rhomin - 1/slim)/rhomin^3;
%     else
%         field_s(i) = 0;
%     end
%     
%     if(rhomax<slim)
%         field_s(i) = K2_s*vectormax*(1/rhomax - 1/slim)/rhomax^3;
%     else
%         field_s(i) = 0;
%     end
% end
% drd_config = [0; 0; field_s(1); 0; 0; field_s(2); 0; 0; field_s(3)];
dx = pinv(JacobianVelocityNow)*drd + sum;%+drd_config;
if(x(9)< smin)
    x(9) = smin;
    if(dx(9)<0)
        dx(9)=0;
    end
elseif(x(9) > smax)
    x(9) = smax;
    if(dx(9)>0)
        dx(9)=0;
    end
end
if(x(12)< smin)
    x(12) = smin;
    if(dx(12)<0)
        dx(12)=0;
    end
elseif(x(12) > smax)
    x(12) = smax;
    if(dx(12)>0)
        dx(12)=0;
    end
end
if(x(15)< smin)
    x(15) = smin;
    if(dx(15)<0)
        dx(15)=0;
    end
elseif(x(15) > smax)
    x(15) = smax;
    if(dx(15)>0)
        dx(15)=0;
    end
end