#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import math
import rospy
from sensor_msgs.msg import Joy
from std_msgs.msg import Float64MultiArray

class joystick_input(object):
  
  def __init__(self):
    rospy.init_node('kps_generator_joy')

    
    self.pub = rospy.Publisher('/configuration_space', Float64MultiArray)
    self.rec_vel = rospy.Subscriber('/joy', Joy, self.ps3_callback)
    
    self.k = [0.5, -0.5, 0.5]
    self.p = [0, 0, 0]
    self.s = [1, 1, 1]
    
    self.delta_k = 0.01
    self.delta_p = 0.01
    self.delta_s = 0.01
    self.kps = Float64MultiArray()
    r = rospy.Rate(10) # 10hz
    rospy.spin()
    
  def ps3_callback(self, msg):
    self.k[0] += msg.axes[0]*self.delta_k
    self.k[1] += msg.axes[1]*self.delta_k
    self.k[2] += msg.axes[2]*self.delta_k
    
    self.p[0] += msg.axes[3]*self.delta_p
    self.p[1] += msg.axes[4]*self.delta_p
    self.p[2] += msg.axes[5]*self.delta_p
    
    self.s[0] += (msg.buttons[0]-msg.buttons[2])*self.delta_s
    self.s[1] += (msg.buttons[1]-msg.buttons[3])*self.delta_s
    self.s[2] += (msg.buttons[8]-msg.buttons[9])*self.delta_s
    
    self.kps.data = []
    self.kps.data.append(self.k[0])
    self.kps.data.append(self.p[0])
    self.kps.data.append(self.s[0])
    self.kps.data.append(self.k[1])
    self.kps.data.append(self.p[1])
    self.kps.data.append(self.s[1])
    self.kps.data.append(self.k[2])
    self.kps.data.append(self.p[2])
    self.kps.data.append(self.s[2])
    
    self.pub.publish(self.kps)
        
if __name__ == '__main__':
    try:
        running = joystick_input()
    except rospy.ROSInterruptException: pass