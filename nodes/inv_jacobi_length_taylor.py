#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import math
import rospy
import numpy as np
from sympy import *
from scipy.integrate import ode
from std_msgs.msg import Float64MultiArray

k1, k2, k3, p1, p2, p3, s1, s2, s3 = symbols('k1 k2 k3 p1 p2 p3 s1 s2 s3')
l11, l12, l13, l21, l22, l23, l31, l32, l33 = symbols('l11 l12 l13 l21 l22 l23 l31 l32 l33')
eps = symbols('eps')

def homogeneous_transform_sym(segment_num):
    TransMatrix = Matrix([[cos(p1)*cos(k1*s1), -sin(p1), cos(p1)*sin(k1*s1), cos(p1)*(1-cos(k1*s1))/k1],
			  [sin(p1)*cos(k1*s1), cos(p1), sin(p1)*sin(k1*s1), sin(p1)*(1-cos(k1*s1))/k1],
			  [-sin(k1*s1), 0, cos(k1*s1), sin(k1*s1)/k1],
			  [0, 0, 0, 1]])
    if (segment_num == 1):
      TM = TransMatrix
    elif(segment_num == 2):
      TM = TransMatrix*(TransMatrix.subs([(k1, k2),(p1, p2),(s1, s2)]))
    else:
      TM = TransMatrix*(TransMatrix.subs([(k1, k2),(p1, p2),(s1, s2)]))*(TransMatrix.subs([(k1, k3),(p1, p3),(s1, s3)]))
    
    return TM
    
def homo_to_pose_sym(T):
    position = Matrix([[T[0,3]],[T[1,3]],[T[2,3]]])
    #print(position)
    return position
    
def l_to_phi(l1, l2, l3):
    phi_out = atan2((sqrt(3)*(l2+l3-2*l1)),(3*(l2-l3)))
    #print(trigsimp(phi_out))
    return phi_out.evalf()
      
class inv_jacobi(object):
  
  def __init__(self):
    rospy.init_node('inv_jacobi')
    self.JacVelSym = zeros(3,9)
    self.JacVelSymBot = zeros(3,9)
    self.JacVelSymMid = zeros(3,9)
    self.JacVelSymUp = zeros(3,9)
    self.JacOmegaSym = zeros(3,9)
    self.Jw = zeros(3,9)
    self.Jv = zeros(3,9)
    self.Lo = 1
    d = 17e-3
    self.p1_ex = atan((sqrt(3)*(l12+l13-2*l11))/(3*(l12-l13)))
    self.k1_ex = 2*(sqrt(pow(l11,2)+pow(l12,2)+pow(l13,2)-l11*l12-l11*l13-l12*l13))/(d*(3*self.Lo+l11+l12+l13))
    self.s1_ex = (3*self.Lo+l11+l12+l13)/3
    self.p2_ex = atan((sqrt(3)*(l22+l23-2*l21))/(3*(l22-l23)))
    self.k2_ex = 2*(sqrt(pow(l21,2)+pow(l22,2)+pow(l23,2)-l21*l22-l21*l23-l22*l23))/(d*(3*self.Lo+l21+l22+l23))
    self.s2_ex = (3*self.Lo+l21+l22+l23)/3
    self.p3_ex = atan((sqrt(3)*(l32+l33-2*l31))/(3*(l32-l33)))
    self.k3_ex = 2*(sqrt(pow(l31,2)+pow(l32,2)+pow(l33,2)-l31*l32-l31*l33-l32*l33))/(d*(3*self.Lo+l31+l32+l33))
    self.s3_ex = (3*self.Lo+l31+l32+l33)/3
    
    self.k_out = lambdify((l11, l12, l13), self.k1_ex, "numpy")
    self.s_out = lambdify((l11, l12, l13), self.s1_ex, "numpy")
    
    self.init_jacobian_omega()
    self.init_jacobian_velocity(1)
    self.init_jacobian_velocity_fast(3)
    self.init_jacobian_kps_l()
    
    #print(simplify(sin(k1)**2+cos(k1)**2))
    #just_try = Matrix([[sin(k1)+k2], [k1+sin(k2)]])
    #out = lambdify((k1, k2), just_try, "numpy")
    #print(out(0, 2))
    #print((self.JacVelSymUp))
    #print(self.Jacobian_kps_l)
    pub = rospy.Publisher('/configuration_space', Float64MultiArray)
    length_pub = rospy.Publisher('/length', Float64MultiArray)
    trial_pub = rospy.Publisher('/configuration_field', Float64MultiArray)
    diff_length_pub = rospy.Publisher('/length_difference', Float64MultiArray)
    self.rec_vel = rospy.Subscriber('/field', Float64MultiArray, self.vel_callback)
    self.rec_vel_obs = rospy.Subscriber('/obs_field_configuration_space', Float64MultiArray, self.obs_callback)
    self.kps = Float64MultiArray()
    self.length = Float64MultiArray()
    self.config_field = Float64MultiArray()
    self.diff_length = Float64MultiArray()
    self.error_flag = 0
    
    #this is the state space initial value
    #in this case the segment's length component
    y0, t0 = [0.01, 0.02, 0.03, 0.01, 0.02, 0.03, 0.01, 0.02, 0.03], 0.0
    self.l = y0
    self.k = [self.k_out(y0[0],y0[1],y0[2]), self.k_out(y0[3],y0[4],y0[5]), self.k_out(y0[6],y0[7],y0[8])]
    self.p = [l_to_phi(y0[0],y0[1],y0[2]),l_to_phi(y0[3],y0[4],y0[5]),l_to_phi(y0[6],y0[7],y0[8])]
    self.s = [self.s_out(y0[0],y0[1],y0[2]), self.s_out(y0[3],y0[4],y0[5]), self.s_out(y0[6],y0[7],y0[8])]
    self.l_diff = [abs(y0[0]-y0[1]), abs(y0[0]-y0[2]), abs(y0[1]-y0[2]), abs(y0[3]-y0[4]), abs(y0[3]-y0[5]), abs(y0[4]-y0[5]), abs(y0[6]-y0[7]), abs(y0[6]-y0[8]), abs(y0[7]-y0[8])]
    
    self.v_obs = zeros(9,1)
    self.v = [0, 0, 0]
    self.vel = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    
    self.smin = -0.5
    self.smax = 0.5
    self.slim = 0.1
    self.K_config = 0.001
    #print((atan2(sqrt(3),1)).evalf())
    #print(atan2((sqrt(3)*(0.2+0.3-2*0.1)),(3*(0.2-0.3))))
    #print(self.k)
    #print(self.p)
    #print(self.s)
    #r = ode(deriv).set_integrator('zvode', method='bdf')
    r = ode(self.deriv).set_integrator('dopri5')
    r.set_initial_value(y0, t0)
    
    dt = 0.01
    
    while (not rospy.is_shutdown()) and (self.error_flag==0):
      #saiki = rospy.Time.now()
      #print(self.k)
      #print("a")
      self.jacobian_velocity(3)
      self.jacobian_kps_l()
      #print(self.Jv)
      #self.jacobian_omega()
      #print(self.error_flag)
      #self.inv_Jv = self.pseudo_inv(Jv)
      #print(self.Jv)
      #print(self.JacobianKPSlNow)
      dummy_matrix = self.Jv*self.JacobianKPSlNow
      self.inv_J = np.linalg.pinv(dummy_matrix)
      #rampung = rospy.Time.now()
      #print((rampung.nsecs-saiki.nsecs))
      #print(embuh)
      #print(embuh2)
      #while r.successful() and r.t < t1:
      if r.successful():
	#print "a"
	r.integrate(r.t+dt)
	#print "i"
	#print(r.t)
	#print(r.y[0])
        self.update(r.y)
      self.transform_to_sent()
      pub.publish(self.kps)
      length_pub.publish(self.length)
      trial_pub.publish(self.config_field)
      diff_length_pub.publish(self.diff_length)
      
  def vel_callback(self, msg):
    self.v[0] = msg.data[0]
    self.v[1] = msg.data[1]
    self.v[2] = msg.data[2]
    
  def obs_callback(self, msg):
    self.v_obs = Matrix([[msg.data[0]],[msg.data[1]],[msg.data[2]],[msg.data[3]],[msg.data[4]],[msg.data[5]],[msg.data[6]],[msg.data[7]],[msg.data[8]]])
    
  def deriv(self,t,y):  # return derivatives of the array y
    velocity = Matrix([self.v[0], self.v[1], self.v[2]])
    #velocity = Matrix([[0], [0], [5*sin(50*t)]])
    #oke = pseudo_inv(velocity)
    #velocity = 2*embuh
    #print(self.dummy)
    #print(self.embuh2_array)
    #print(self.embuh2.tolist())
    #dydt_mat = self.inv_J*velocity + self.v_obs
    #self.configuration_field(y)
    #matrix_con_field = Matrix([[self.vel[0]],[self.vel[1]],[self.vel[2]],[self.vel[3]],[self.vel[4]],[self.vel[5]],[self.vel[6]],[self.vel[7]],[self.vel[8]]])
    dydt_mat = self.inv_J*velocity + self.v_obs #+ matrix_con_field
    
    dydt = dydt_mat.tolist()
    #print(dydt)
    return dydt
    
  def update(self,state):
    self.l = [state[0], state[1], state[2], state[3], state[4], state[5], state[6], state[7], state[8]]
    self.k = [self.k_out(self.l[0],self.l[1],self.l[2]), self.k_out(self.l[3],self.l[4],self.l[5]), self.k_out(self.l[6],self.l[7],self.l[8])]
    self.p = [l_to_phi(self.l[0],self.l[1],self.l[2]),l_to_phi(self.l[3],self.l[4],self.l[5]),l_to_phi(self.l[6],self.l[7],self.l[8])]
    self.s = [self.s_out(self.l[0],self.l[1],self.l[2]), self.s_out(self.l[3],self.l[4],self.l[5]), self.s_out(self.l[6],self.l[7],self.l[8])]
    self.l_diff = [abs(state[0]-state[1]), abs(state[0]-state[2]), abs(state[1]-state[2]), abs(state[3]-state[4]), abs(state[3]-state[5]), abs(state[4]-state[5]), abs(state[6]-state[7]), abs(state[6]-state[8]), abs(state[7]-state[8])]
    self.singularity_checker()

    
  def singularity_checker(self):
    checker = [0.0, 0.0, 0.0]
    for i in range(0,3):
      for j in range(0,3):
	checker[j] = self.l_diff[3*i+j]
      if((checker[0]<1e-5) and (checker[1]<1e-5) and (checker[2]<1e-5)):
	print "SINGULARITY in segment" + str(i)
    
  def transform_to_sent(self):
    self.kps.data = []
    self.length.data = []
    self.diff_length.data = []
    self.kps.data.append(self.k[0])
    self.kps.data.append(self.p[0])
    self.kps.data.append(self.s[0])
    self.kps.data.append(self.k[1])
    self.kps.data.append(self.p[1])
    self.kps.data.append(self.s[1])
    self.kps.data.append(self.k[2])
    self.kps.data.append(self.p[2])
    self.kps.data.append(self.s[2])
    self.length.data.append(self.l[0])
    self.length.data.append(self.l[1])
    self.length.data.append(self.l[2])
    self.length.data.append(self.l[3])
    self.length.data.append(self.l[4])
    self.length.data.append(self.l[5])
    self.length.data.append(self.l[6])
    self.length.data.append(self.l[7])
    self.length.data.append(self.l[8])
    self.diff_length.data.append(self.l_diff[0])
    self.diff_length.data.append(self.l_diff[1])
    self.diff_length.data.append(self.l_diff[2])
    self.diff_length.data.append(self.l_diff[3])
    self.diff_length.data.append(self.l_diff[4])
    self.diff_length.data.append(self.l_diff[5])
    self.diff_length.data.append(self.l_diff[6])
    self.diff_length.data.append(self.l_diff[7])
    self.diff_length.data.append(self.l_diff[8])

  def pseudo_inv(self,matrix):
    matrix_dummy = (matrix.T*matrix)
    if (matrix_dummy.det()!=0):
      out = ((matrix_dummy**-1)*(matrix.T))
    else:
      print "singularity..."
      self.error_flag = 1
      out = matrix.T #just to make out have certain values
    return out
    
  def init_jacobian_velocity(self,segment_num):
    homo_matrix = homogeneous_transform_sym(segment_num)
    self.homo_length = homo_matrix.subs([(k1, self.k1_ex),(p1, self.p1_ex),(s1, self.s1_ex)])
    #print(self.homo_length[2,3])
    
    #fun = exp(-(l11**2+l12**2+l13**2))
    #print(fun.subs(l11,l11*eps).subs(l12,l12*eps).subs(l13, l13*eps).series(eps,n=3).removeO().subs(eps,1))
    #print(self.homo_length[1,3].subs(l11,l11*eps).subs(l12,l12*eps).subs(l13, l13*eps).series(eps,n=3).removeO().subs(eps,1))
    #print("aka")
    pose_point = homo_to_pose_sym(homo_matrix)
    #print(pose_point)
    #if(segment_num == 1):
      #self.JacVelSymBot = Matrix([[simplify(diff(pose_point[0,0],k1)), simplify(diff(pose_point[0,0],p1)), diff(pose_point[0,0],s1), diff(pose_point[0,0],k2), diff(pose_point[0,0],p2),diff(pose_point[0,0],s2), diff(pose_point[0,0],k3), diff(pose_point[0,0],p3),diff(pose_point[0,0],s3)],
				  #[diff(pose_point[1,0],k1), diff(pose_point[1,0],p1), diff(pose_point[1,0],s1), diff(pose_point[1,0],k2), diff(pose_point[1,0],p2),diff(pose_point[1,0],s2), diff(pose_point[1,0],k3), diff(pose_point[1,0],p3),diff(pose_point[1,0],s3)],
				  #[diff(pose_point[2,0],k1), diff(pose_point[2,0],p1), diff(pose_point[2,0],s1), diff(pose_point[2,0],k2), diff(pose_point[2,0],p2),diff(pose_point[2,0],s2), diff(pose_point[2,0],k3), diff(pose_point[2,0],p3),diff(pose_point[2,0],s3)]])
    #elif(segment_num == 2):
      #self.JacVelSymMid = Matrix([[diff(pose_point[0,0],k1), diff(pose_point[0,0],p1), diff(pose_point[0,0],s1), diff(pose_point[0,0],k2), diff(pose_point[0,0],p2),diff(pose_point[0,0],s2), diff(pose_point[0,0],k3), diff(pose_point[0,0],p3),diff(pose_point[0,0],s3)],
				  #[diff(pose_point[1,0],k1), diff(pose_point[1,0],p1), diff(pose_point[1,0],s1), diff(pose_point[1,0],k2), diff(pose_point[1,0],p2),diff(pose_point[1,0],s2), diff(pose_point[1,0],k3), diff(pose_point[1,0],p3),diff(pose_point[1,0],s3)],
				  #[diff(pose_point[2,0],k1), diff(pose_point[2,0],p1), diff(pose_point[2,0],s1), diff(pose_point[2,0],k2), diff(pose_point[2,0],p2),diff(pose_point[2,0],s2), diff(pose_point[2,0],k3), diff(pose_point[2,0],p3),diff(pose_point[2,0],s3)]])
    #else:
      #self.JacVelSymUp = Matrix([[simplify(diff(pose_point[0,0],k1)), simplify(diff(pose_point[0,0],p1)), simplify(diff(pose_point[0,0],s1)), simplify(diff(pose_point[0,0],k2)), simplify(diff(pose_point[0,0],p2)),simplify(diff(pose_point[0,0],s2)), simplify(diff(pose_point[0,0],k3)), simplify(diff(pose_point[0,0],p3)),simplify(diff(pose_point[0,0],s3))],
				  #[simplify(diff(pose_point[1,0],k1)), simplify(diff(pose_point[1,0],p1)), simplify(diff(pose_point[1,0],s1)), simplify(diff(pose_point[1,0],k2)), simplify(diff(pose_point[1,0],p2)),simplify(diff(pose_point[1,0],s2)), simplify(diff(pose_point[1,0],k3)), simplify(diff(pose_point[1,0],p3)),simplify(diff(pose_point[1,0],s3))],
				  #[simplify(diff(pose_point[2,0],k1)), simplify(diff(pose_point[2,0],p1)), simplify(diff(pose_point[2,0],s1)), simplify(diff(pose_point[2,0],k2)), simplify(diff(pose_point[2,0],p2)),simplify(diff(pose_point[2,0],s2)), simplify(diff(pose_point[2,0],k3)), simplify(diff(pose_point[2,0],p3)),simplify(diff(pose_point[2,0],s3))]])
    self.JacVelSymUp = Matrix([[diff(pose_point[0,0],k1), diff(pose_point[0,0],p1), diff(pose_point[0,0],s1), diff(pose_point[0,0],k2), diff(pose_point[0,0],p2),diff(pose_point[0,0],s2), diff(pose_point[0,0],k3), diff(pose_point[0,0],p3),diff(pose_point[0,0],s3)],
				  [diff(pose_point[1,0],k1), diff(pose_point[1,0],p1), diff(pose_point[1,0],s1), diff(pose_point[1,0],k2), diff(pose_point[1,0],p2),diff(pose_point[1,0],s2), diff(pose_point[1,0],k3), diff(pose_point[1,0],p3),diff(pose_point[1,0],s3)],
				  [diff(pose_point[2,0],k1), diff(pose_point[2,0],p1), diff(pose_point[2,0],s1), diff(pose_point[2,0],k2), diff(pose_point[2,0],p2),diff(pose_point[2,0],s2), diff(pose_point[2,0],k3), diff(pose_point[2,0],p3),diff(pose_point[2,0],s3)]])
    #return JacVelSym
  def init_jacobian_kps_l(self):
    self.Jacobian_kps_l = Matrix([ [diff(self.k1_ex, l11), diff(self.k1_ex, l12), diff(self.k1_ex, l13), diff(self.k1_ex, l21), diff(self.k1_ex, l22), diff(self.k1_ex, l23), diff(self.k1_ex, l31), diff(self.k1_ex, l32), diff(self.k1_ex, l33)],
				    [diff(self.p1_ex, l11), diff(self.p1_ex, l12), diff(self.p1_ex, l13), diff(self.p1_ex, l21), diff(self.p1_ex, l22), diff(self.p1_ex, l23), diff(self.p1_ex, l31), diff(self.p1_ex, l32), diff(self.p1_ex, l33)],
				    [diff(self.s1_ex, l11), diff(self.s1_ex, l12), diff(self.s1_ex, l13), diff(self.s1_ex, l21), diff(self.s1_ex, l22), diff(self.s1_ex, l23), diff(self.s1_ex, l31), diff(self.s1_ex, l32), diff(self.s1_ex, l33)],
				    [diff(self.k2_ex, l11), diff(self.k2_ex, l12), diff(self.k2_ex, l13), diff(self.k2_ex, l21), diff(self.k2_ex, l22), diff(self.k2_ex, l23), diff(self.k2_ex, l31), diff(self.k2_ex, l32), diff(self.k2_ex, l33)],
				    [diff(self.p2_ex, l11), diff(self.p2_ex, l12), diff(self.p2_ex, l13), diff(self.p2_ex, l21), diff(self.p2_ex, l22), diff(self.p2_ex, l23), diff(self.p2_ex, l31), diff(self.p2_ex, l32), diff(self.p2_ex, l33)],
				    [diff(self.s2_ex, l11), diff(self.s2_ex, l12), diff(self.s2_ex, l13), diff(self.s2_ex, l21), diff(self.s2_ex, l22), diff(self.s2_ex, l23), diff(self.s2_ex, l31), diff(self.s2_ex, l32), diff(self.s2_ex, l33)],
				    [diff(self.k3_ex, l11), diff(self.k3_ex, l12), diff(self.k3_ex, l13), diff(self.k3_ex, l21), diff(self.k3_ex, l22), diff(self.k3_ex, l23), diff(self.k3_ex, l31), diff(self.k3_ex, l32), diff(self.k3_ex, l33)],
				    [diff(self.p3_ex, l11), diff(self.p3_ex, l12), diff(self.p3_ex, l13), diff(self.p3_ex, l21), diff(self.p3_ex, l22), diff(self.p3_ex, l23), diff(self.p3_ex, l31), diff(self.p3_ex, l32), diff(self.p3_ex, l33)],
				    [diff(self.s3_ex, l11), diff(self.s3_ex, l12), diff(self.s3_ex, l13), diff(self.s3_ex, l21), diff(self.s3_ex, l22), diff(self.s3_ex, l23), diff(self.s3_ex, l31), diff(self.s3_ex, l32), diff(self.s3_ex, l33)]])
    self.JacobianKPSlFunc = lambdify((l11, l12, l13, l21, l22, l23, l31, l32, l33), self.Jacobian_kps_l, "sympy")

  
  def init_jacobian_velocity_fast(self,segment_num):
    self.JacVelSymUp = Matrix([[(k1**2*k2*s1*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1**2*k3*s1*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k1*k2*k3*s1*sin(k1*s1) + k2*k3*(cos(k1*s1) - 1))*cos(p1)/(k1**2*k2*k3), (k1*k2*(-(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k3*s3) - 1)*sin(p3) + (cos(k3*s3) - 1)*(-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*cos(p3) - (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*sin(k3*s3)) + k1*k3*((cos(k2*s2) - 1)*sin(p1)*cos(p2)*cos(k1*s1) + (cos(k2*s2) - 1)*sin(p2)*cos(p1) - sin(p1)*sin(k1*s1)*sin(k2*s2)) + k2*k3*(cos(k1*s1) - 1)*sin(p1))/(k1*k2*k3), (k1*k2*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1*k3*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k2*k3*sin(k1*s1))*cos(p1)/(k2*k3), (k2**2*s2*((cos(k3*s3) - 1)*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(p3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3)) + k2*k3*s2*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1)) + k3*(-(cos(k2*s2) - 1)*sin(p1)*sin(p2) + (cos(k2*s2) - 1)*cos(p1)*cos(p2)*cos(k1*s1) - sin(k1*s1)*sin(k2*s2)*cos(p1)))/(k2**2*k3), (k2*(-(sin(p1)*sin(p2) - cos(p1)*cos(p2)*cos(k1*s1))*(cos(k3*s3) - 1)*sin(p3) + (sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3)*cos(k2*s2) - (sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*sin(k2*s2)*sin(k3*s3)) + k3*(sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k2*s2) - 1))/(k2*k3), (k2*(cos(k3*s3) - 1)*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(p3) - k2*(sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + k3*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1)))/k3, (k3*s3*(-(sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(k3*s3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3)*cos(p3)) - (sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k3*s3) - 1)*sin(p3) - (cos(k3*s3) - 1)*(sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*cos(p3) - (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*sin(k3*s3))/k3**2, ((sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*cos(p3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(p3))*(cos(k3*s3) - 1)/k3, -(sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(k3*s3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3)*cos(p3)], [(k1**2*k2*s1*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1**2*k3*s1*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k1*k2*k3*s1*sin(k1*s1) + k2*k3*(cos(k1*s1) - 1))*sin(p1)/(k1**2*k2*k3), (k1*k2*((sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k3*s3) - 1)*sin(p3) + (cos(k3*s3) - 1)*(sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*cos(p3) + (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*sin(k3*s3)) + k1*k3*((cos(k2*s2) - 1)*sin(p1)*sin(p2) - (cos(k2*s2) - 1)*cos(p1)*cos(p2)*cos(k1*s1) + sin(k1*s1)*sin(k2*s2)*cos(p1)) - k2*k3*(cos(k1*s1) - 1)*cos(p1))/(k1*k2*k3), (k1*k2*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1*k3*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k2*k3*sin(k1*s1))*sin(p1)/(k2*k3), (k2**2*s2*((cos(k3*s3) - 1)*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(p3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3)) + k2*k3*s2*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1)) + k3*((cos(k2*s2) - 1)*sin(p1)*cos(p2)*cos(k1*s1) + (cos(k2*s2) - 1)*sin(p2)*cos(p1) - sin(p1)*sin(k1*s1)*sin(k2*s2)))/(k2**2*k3), (k2*((sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k3*s3) - 1)*cos(p3)*cos(k2*s2) - (sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*sin(k2*s2)*sin(k3*s3) + (sin(p1)*cos(p2)*cos(k1*s1) + sin(p2)*cos(p1))*(cos(k3*s3) - 1)*sin(p3)) + k3*(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k2*s2) - 1))/(k2*k3), (k2*(cos(k3*s3) - 1)*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(p3) + k2*(-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3) + k3*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1)))/k3, (k3*s3*(-(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3)*cos(p3) + (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(k3*s3)) - (sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k3*s3) - 1)*sin(p3) + (cos(k3*s3) - 1)*(-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*cos(p3) - (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*sin(k3*s3))/k3**2, ((sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*cos(p3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(p3))*(cos(k3*s3) - 1)/k3, -(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3)*cos(p3) + (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(k3*s3)], [(-k1**2*k2*s1*((sin(k1*s1)*sin(k2*s2) - cos(p2)*cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) + (sin(k1*s1)*cos(k2*s2) + sin(k2*s2)*cos(p2)*cos(k1*s1))*sin(k3*s3) + (cos(k3*s3) - 1)*sin(p2)*sin(p3)*cos(k1*s1)) + k1**2*k3*s1*((cos(k2*s2) - 1)*cos(p2)*cos(k1*s1) - sin(k1*s1)*sin(k2*s2)) + k1*k2*k3*s1*cos(k1*s1) - k2*k3*sin(k1*s1))/(k1**2*k2*k3), 0, (-k1*k2*((sin(k1*s1)*sin(k2*s2) - cos(p2)*cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) + (sin(k1*s1)*cos(k2*s2) + sin(k2*s2)*cos(p2)*cos(k1*s1))*sin(k3*s3) + (cos(k3*s3) - 1)*sin(p2)*sin(p3)*cos(k1*s1)) + k1*k3*((cos(k2*s2) - 1)*cos(p2)*cos(k1*s1) - sin(k1*s1)*sin(k2*s2)) + k2*k3*cos(k1*s1))/(k2*k3), (-k2**2*s2*((sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3)) + k2*k3*s2*(-sin(k1*s1)*sin(k2*s2)*cos(p2) + cos(k1*s1)*cos(k2*s2)) - k3*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)))/(k2**2*k3), (k2*(-(cos(k3*s3) - 1)*sin(p2)*cos(p3)*cos(k2*s2) - (cos(k3*s3) - 1)*sin(p3)*cos(p2) + sin(p2)*sin(k2*s2)*sin(k3*s3)) - k3*(cos(k2*s2) - 1)*sin(p2))*sin(k1*s1)/(k2*k3), (-k2*(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) - k2*(sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3) + k3*(-sin(k1*s1)*sin(k2*s2)*cos(p2) + cos(k1*s1)*cos(k2*s2)))/k3, (k3*s3*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*cos(k3*s3) - (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3)*cos(p3) + sin(p2)*sin(p3)*sin(k1*s1)*sin(k3*s3)) + (sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) - (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) + (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1))/k3**2, -((sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(p3) + sin(p2)*sin(k1*s1)*cos(p3))*(cos(k3*s3) - 1)/k3, -(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*cos(k3*s3) - (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3)*cos(p3) + sin(p2)*sin(p3)*sin(k1*s1)*sin(k3*s3)]])

    self.JacVelFunc = lambdify((k1, k2, k3, p1, p2, p3, s1, s2, s3), self.JacVelSymUp, "sympy")
    #print(self.JacVelFunc)

  def jacobian_velocity(self, segment_num):
    if(segment_num == 1):
      self.JacVelSym = self.JacVelSymBot
    elif(segment_num == 2):
      self.JacVelSym = self.JacVelSymMid
    elif(segment_num == 3):
      self.JacVelSym = self.JacVelSymUp
    #print(self.JacVelSym)
    #self.Jv = self.JacVelSym.subs([(k1, self.k[0]),(k2, self.k[1]),(k3, self.k[2]),(p1, self.p[0]),(p2, self.p[1]),(p3, self.p[2]),(s1, self.s[0]),(s2, self.s[1]),(s3, self.s[2])])

    #aka = self.k[0]
    #aka1 = self.k[1]
    #aka2 = self.k[2]
    #aka3 = self.p[0]
    #aka4 = self.p[1]
    #aka5 = self.p[2]
    #aka6 = self.s[0]
    #aka7 = self.s[1]
    #aka8 = self.s[2]
    #print(self.p)
    #print(self.s)
    #self.Jv = self.JacVelFunc(aka, aka1, aka2, aka3, aka4, aka5, aka6, aka7, aka8)
    #print(self.Jv)
    self.Jv = self.JacVelFunc(self.k[0], self.k[1], self.k[2], self.p[0], self.p[1], self.p[2], self.s[0], self.s[1], self.s[2])
    #return JacVel
    
  def jacobian_kps_l(self):
    self.JacobianKPSlNow = self.JacobianKPSlFunc(self.l[0],self.l[1],self.l[2],self.l[3],self.l[4],self.l[5],self.l[6],self.l[7],self.l[8])
    
  def init_jacobian_omega(self):
    T = homogeneous_transform_sym(3)
    JacOmega11 = diff(T[2,0],k1)*T[1,0]+diff(T[2,1],k1)*T[1,1]+diff(T[2,2],k1)*T[1,2];
    JacOmega21 = diff(T[0,0],k1)*T[2,0]+diff(T[0,1],k1)*T[2,1]+diff(T[0,2],k1)*T[2,2];
    JacOmega31 = diff(T[1,0],k1)*T[0,0]+diff(T[1,1],k1)*T[0,1]+diff(T[1,2],k1)*T[0,2];
    JacOmega12 = diff(T[2,0],p1)*T[1,0]+diff(T[2,1],p1)*T[1,1]+diff(T[2,2],p1)*T[1,2];
    JacOmega22 = diff(T[0,0],p1)*T[2,0]+diff(T[0,1],p1)*T[2,1]+diff(T[0,2],p1)*T[2,2];
    JacOmega32 = diff(T[1,0],p1)*T[0,0]+diff(T[1,1],p1)*T[0,1]+diff(T[1,2],p1)*T[0,2];
    JacOmega13 = diff(T[2,0],s1)*T[1,0]+diff(T[2,1],s1)*T[1,1]+diff(T[2,2],s1)*T[1,2];
    JacOmega23 = diff(T[0,0],s1)*T[2,0]+diff(T[0,1],s1)*T[2,1]+diff(T[0,2],s1)*T[2,2];
    JacOmega33 = diff(T[1,0],s1)*T[0,0]+diff(T[1,1],s1)*T[0,1]+diff(T[1,2],s1)*T[0,2];
    JacOmega14 = diff(T[2,0],k2)*T[1,0]+diff(T[2,1],k2)*T[1,1]+diff(T[2,2],k2)*T[1,2];
    JacOmega24 = diff(T[0,0],k2)*T[2,0]+diff(T[0,1],k2)*T[2,1]+diff(T[0,2],k2)*T[2,2];
    JacOmega34 = diff(T[1,0],k2)*T[0,0]+diff(T[1,1],k2)*T[0,1]+diff(T[1,2],k2)*T[0,2];
    JacOmega15 = diff(T[2,0],p2)*T[1,0]+diff(T[2,1],p2)*T[1,1]+diff(T[2,2],p2)*T[1,2];
    JacOmega25 = diff(T[0,0],p2)*T[2,0]+diff(T[0,1],p2)*T[2,1]+diff(T[0,2],p2)*T[2,2];
    JacOmega35 = diff(T[1,0],p2)*T[0,0]+diff(T[1,1],p2)*T[0,1]+diff(T[1,2],p2)*T[0,2];
    JacOmega16 = diff(T[2,0],s2)*T[1,0]+diff(T[2,1],s2)*T[1,1]+diff(T[2,2],s2)*T[1,2];
    JacOmega26 = diff(T[0,0],s2)*T[2,0]+diff(T[0,1],s2)*T[2,1]+diff(T[0,2],s2)*T[2,2];
    JacOmega36 = diff(T[1,0],s2)*T[0,0]+diff(T[1,1],s2)*T[0,1]+diff(T[1,2],s2)*T[0,2];
    JacOmega17 = diff(T[2,0],k3)*T[1,0]+diff(T[2,1],k3)*T[1,1]+diff(T[2,2],k3)*T[1,2];
    JacOmega27 = diff(T[0,0],k3)*T[2,0]+diff(T[0,1],k3)*T[2,1]+diff(T[0,2],k3)*T[2,2];
    JacOmega37 = diff(T[1,0],k3)*T[0,0]+diff(T[1,1],k3)*T[0,1]+diff(T[1,2],k3)*T[0,2];
    JacOmega18 = diff(T[2,0],p3)*T[1,0]+diff(T[2,1],p3)*T[1,1]+diff(T[2,2],p3)*T[1,2];
    JacOmega28 = diff(T[0,0],p3)*T[2,0]+diff(T[0,1],p3)*T[2,1]+diff(T[0,2],p3)*T[2,2];
    JacOmega38 = diff(T[1,0],p3)*T[0,0]+diff(T[1,1],p3)*T[0,1]+diff(T[1,2],p3)*T[0,2];
    JacOmega19 = diff(T[2,0],s3)*T[1,0]+diff(T[2,1],s3)*T[1,1]+diff(T[2,2],s3)*T[1,2];
    JacOmega29 = diff(T[0,0],s3)*T[2,0]+diff(T[0,1],s3)*T[2,1]+diff(T[0,2],s3)*T[2,2];
    JacOmega39 = diff(T[1,0],s3)*T[0,0]+diff(T[1,1],s3)*T[0,1]+diff(T[1,2],s3)*T[0,2];
    self.JacOmegaSym = Matrix([[JacOmega11, JacOmega12, JacOmega13, JacOmega14, JacOmega15, JacOmega16, JacOmega17, JacOmega18, JacOmega19],
		      [JacOmega21, JacOmega22, JacOmega23, JacOmega24, JacOmega25, JacOmega26, JacOmega27, JacOmega28, JacOmega29],
		      [JacOmega31, JacOmega32, JacOmega33, JacOmega34, JacOmega35, JacOmega36, JacOmega37, JacOmega38, JacOmega39]])
    #print(JacOmegaSym)
    #return JacOmegaSym
    
  def jacobian_omega(self):
    self.Jw = self.JacOmegaSym.subs([(k1, self.k[0]),(k2, self.k[1]),(k3, self.k[2]),(p1, self.p[0]),(p2, self.p[1]),(p3, self.p[2]),(s1, self.s[0]),(s2, self.s[1]),(s3, self.s[2])])

  def configuration_field(self,length_of_segment):
    self.config_field.data = []
    for i in range(0,9):
      vector_min = length_of_segment[i] - self.smin
      vector_max = length_of_segment[i] - self.smax
      rhomin = sqrt(vector_min*vector_min)
      rhomax = sqrt(vector_max*vector_max)
      #print(rhomax)
      #print(rhomin)
      if(rhomin<self.slim):
	self.vel[i] = self.K_config*vector_min*(1/rhomin - 1/self.slim)/pow(rhomin,3)
      elif(rhomax<self.slim):
	self.vel[i] = self.K_config*vector_max*(1/rhomax - 1/self.slim)/pow(rhomax,3)
      else:
	self.vel[i] = 0
      #print(self.vel[i])
      self.config_field.data.append(self.vel[i])
      
if __name__ == '__main__':
    try:
        inv_jacobi()
    except rospy.ROSInterruptException: pass
