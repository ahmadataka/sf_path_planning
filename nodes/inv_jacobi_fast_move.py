#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import math
import rospy
import numpy as np
from sympy import *
from scipy.integrate import ode
from std_msgs.msg import Float64MultiArray

k1, k2, k3, p1, p2, p3, s1, s2, s3 = symbols('k1 k2 k3 p1 p2 p3 s1 s2 s3')
xb, yb, zb, alphab, betab, gammab = symbols('xb yb zb alphab betab gammab')
#JacOmegaSym
#JacVelSymBot
#JacVelSymMid
#JacVelSymUp
def homogeneous_transform_sym(segment_num):
    TransMatrix = Matrix([[cos(p1)*cos(k1*s1), -sin(p1), cos(p1)*sin(k1*s1), cos(p1)*(1-cos(k1*s1))/k1],
			  [sin(p1)*cos(k1*s1), cos(p1), sin(p1)*sin(k1*s1), sin(p1)*(1-cos(k1*s1))/k1],
			  [-sin(k1*s1), 0, cos(k1*s1), sin(k1*s1)/k1],
			  [0, 0, 0, 1]])
    Tb = Matrix([ [cos(alphab)*cos(betab), cos(alphab)*sin(betab)*sin(gammab)-sin(alphab)*cos(gammab), cos(alphab)*sin(betab)*cos(gammab)+sin(alphab)*sin(gammab), xb],
		  [sin(alphab)*cos(betab), sin(alphab)*sin(betab)*sin(gammab)+cos(alphab)*cos(gammab), sin(alphab)*sin(betab)*cos(gammab)-cos(alphab)*sin(gammab), yb],
		  [-sin(betab), cos(betab)*sin(gammab), cos(betab)*cos(gammab), zb],
		  [0, 0, 0, 1]])

    if (segment_num == 1):
      TM = Tb*TransMatrix
    elif(segment_num == 2):
      TM = Tb*TransMatrix*(TransMatrix.subs([(k1, k2),(p1, p2),(s1, s2)]))
    else:
      TM = Tb*TransMatrix*(TransMatrix.subs([(k1, k2),(p1, p2),(s1, s2)]))*(TransMatrix.subs([(k1, k3),(p1, p3),(s1, s3)]))
    
    return TM
    
def homo_to_pose_sym(T):
    position = Matrix([[T[0,3]],[T[1,3]],[T[2,3]]])
    #print(position)
    return position
  
class inv_jacobi(object):
  
  def __init__(self):
    rospy.init_node('inv_jacobi')
    self.JacVelSym = zeros(3,9)
    self.JacVelSymBot = zeros(3,9)
    self.JacVelSymMid = zeros(3,9)
    self.JacVelSymUp = zeros(3,9)
    self.JacOmegaSym = zeros(3,9)
    self.Jw = zeros(3,9)
    self.Jv = zeros(3,9)
    
    self.init_jacobian_omega()
    self.init_jacobian_velocity(3)
    #self.init_jacobian_velocity_fast(3)
    #print(simplify(sin(k1)**2+cos(k1)**2))
    #just_try = Matrix([[sin(k1)+k2], [k1+sin(k2)]])
    #out = lambdify((k1, k2), just_try, "numpy")
    #print(out(0, 2))
    #print((self.JacVelSymUp))
    
    pub = rospy.Publisher('/configuration_space', Float64MultiArray, queue_size = 10)
    trial_pub = rospy.Publisher('/configuration_field', Float64MultiArray, queue_size = 10)
    self.rec_vel = rospy.Subscriber('/field', Float64MultiArray, self.vel_callback)
    self.rec_vel_obs = rospy.Subscriber('/obs_field_configuration_space', Float64MultiArray, self.obs_callback)
    self.kps = Float64MultiArray()
    self.config_field = Float64MultiArray()
    self.error_flag = 0
    # The state spaces are xb, yb, zb, alphab, betab, gammab, k1, p1, s1, k2, p2, s2, k3, p3, s3
    y0, t0 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 0, 1, -0.5, 0, 1, 0.5, 0, 1], 0
    self.k = [y0[6], y0[9], y0[12]]
    self.p = [y0[7], y0[10], y0[13]]
    self.s = [y0[8], y0[11], y0[14]]
    self.position_base = [y0[0], y0[1], y0[2]]
    self.angle_base = [y0[3], y0[4], y0[5]]
    self.v_obs = zeros(15,1)
    self.v = [0, 0, 0]
    self.vel = [0.0, 0.0, 0.0]
    
    self.smin = 0.5
    self.smax = 1.5
    self.slim = 0.1
    self.K_config = 0.001
    #r = ode(deriv).set_integrator('zvode', method='bdf')
    r = ode(self.deriv).set_integrator('dopri5')
    r.set_initial_value(y0, t0)
    
    dt = 0.01
    
    while (not rospy.is_shutdown()) and (self.error_flag==0):
      #saiki = rospy.Time.now()
      #print(self.k)
      self.jacobian_velocity(3)
      #print(self.Jv)
      #self.jacobian_omega()
      #print(self.error_flag)
      #self.inv_Jv = self.pseudo_inv(Jv)
      self.inv_Jv = np.linalg.pinv(self.Jv)
      #rampung = rospy.Time.now()
      #print((rampung.nsecs-saiki.nsecs))
      #print(embuh)
      #print(embuh2)
      #while r.successful() and r.t < t1:
      if r.successful():
	#print "a"
	r.integrate(r.t+dt)
	#print "i"
	#print(r.t)
	#print(r.y[0])
        self.update(r.y)
      self.transform_to_sent()
      pub.publish(self.kps)
      trial_pub.publish(self.config_field)
      
  def vel_callback(self, msg):
    self.v[0] = msg.data[0]
    self.v[1] = msg.data[1]
    self.v[2] = msg.data[2]
    
  def obs_callback(self, msg):
    self.v_obs = Matrix([[msg.data[0]],[msg.data[1]],[msg.data[2]],[msg.data[3]],[msg.data[4]],[msg.data[5]],[msg.data[6]],[msg.data[7]],[msg.data[8]], [msg.data[9]], [msg.data[10]], [msg.data[11]], [msg.data[12]], [msg.data[13]], [msg.data[14]]])
    
  def deriv(self,t,y):  # return derivatives of the array y
    velocity = Matrix([self.v[0], self.v[1], self.v[2]])
    #velocity = Matrix([[0], [0], [5*sin(50*t)]])
    #oke = pseudo_inv(velocity)
    #velocity = 2*embuh
    #print(self.dummy)
    #print(self.embuh2_array)
    #print(self.embuh2.tolist())
    
    #self.configuration_field([y[2], y[5], y[8]])
    #matrix_con_field = Matrix([[0],[0],[self.vel[0]],[0],[0],[self.vel[1]],[0],[0],[self.vel[2]]])
    dydt_mat = self.inv_Jv*velocity + self.v_obs #+ matrix_con_field
    dydt = dydt_mat.tolist()
    
    #Limit
    #for i in range(1,4):
      #if(y[3*i-1]<self.smin):
	#y[3*i-1] = self.smin
	#if(dydt[3*i-1][0]<0):
	  #dydt[3*i-1][0]=0
      #elif(y[3*i-1]>self.smax):
	#y[3*i-1] = self.smax
	#if(dydt[3*i-1][0]>0):
	  #dydt[3*i-1][0]=0

    #print(dydt[2][0])
    #print(y[2])
    return dydt
    
  def update(self,state):
    self.k = [state[6], state[9], state[12]]
    self.p = [state[7], state[10], state[13]]
    self.s = [state[8], state[11], state[14]]
    self.position_base = [state[0], state[1], state[2]]
    self.angle_base = [state[3], state[4], state[5]]
    
  def transform_to_sent(self):
    self.kps.data = []
    self.kps.data.append(self.position_base[0])
    self.kps.data.append(self.position_base[1])
    self.kps.data.append(self.position_base[2])
    self.kps.data.append(self.angle_base[0])
    self.kps.data.append(self.angle_base[1])
    self.kps.data.append(self.angle_base[2])
    self.kps.data.append(self.k[0])
    self.kps.data.append(self.p[0])
    self.kps.data.append(self.s[0])
    self.kps.data.append(self.k[1])
    self.kps.data.append(self.p[1])
    self.kps.data.append(self.s[1])
    self.kps.data.append(self.k[2])
    self.kps.data.append(self.p[2])
    self.kps.data.append(self.s[2])

  def pseudo_inv(self,matrix):
    matrix_dummy = (matrix.T*matrix)
    if (matrix_dummy.det()!=0):
      out = ((matrix_dummy**-1)*(matrix.T))
    else:
      print "singularity..."
      self.error_flag = 1
      out = matrix.T #just to make out have certain values
    return out
    
  def init_jacobian_velocity(self,segment_num):
    homo_matrix = homogeneous_transform_sym(segment_num)
    pose_point = homo_to_pose_sym(homo_matrix)
    #print(pose_point)
    #if(segment_num == 1):
      #self.JacVelSymBot = Matrix([[simplify(diff(pose_point[0,0],k1)), simplify(diff(pose_point[0,0],p1)), diff(pose_point[0,0],s1), diff(pose_point[0,0],k2), diff(pose_point[0,0],p2),diff(pose_point[0,0],s2), diff(pose_point[0,0],k3), diff(pose_point[0,0],p3),diff(pose_point[0,0],s3)],
				  #[diff(pose_point[1,0],k1), diff(pose_point[1,0],p1), diff(pose_point[1,0],s1), diff(pose_point[1,0],k2), diff(pose_point[1,0],p2),diff(pose_point[1,0],s2), diff(pose_point[1,0],k3), diff(pose_point[1,0],p3),diff(pose_point[1,0],s3)],
				  #[diff(pose_point[2,0],k1), diff(pose_point[2,0],p1), diff(pose_point[2,0],s1), diff(pose_point[2,0],k2), diff(pose_point[2,0],p2),diff(pose_point[2,0],s2), diff(pose_point[2,0],k3), diff(pose_point[2,0],p3),diff(pose_point[2,0],s3)]])
    #elif(segment_num == 2):
      #self.JacVelSymMid = Matrix([[diff(pose_point[0,0],k1), diff(pose_point[0,0],p1), diff(pose_point[0,0],s1), diff(pose_point[0,0],k2), diff(pose_point[0,0],p2),diff(pose_point[0,0],s2), diff(pose_point[0,0],k3), diff(pose_point[0,0],p3),diff(pose_point[0,0],s3)],
				  #[diff(pose_point[1,0],k1), diff(pose_point[1,0],p1), diff(pose_point[1,0],s1), diff(pose_point[1,0],k2), diff(pose_point[1,0],p2),diff(pose_point[1,0],s2), diff(pose_point[1,0],k3), diff(pose_point[1,0],p3),diff(pose_point[1,0],s3)],
				  #[diff(pose_point[2,0],k1), diff(pose_point[2,0],p1), diff(pose_point[2,0],s1), diff(pose_point[2,0],k2), diff(pose_point[2,0],p2),diff(pose_point[2,0],s2), diff(pose_point[2,0],k3), diff(pose_point[2,0],p3),diff(pose_point[2,0],s3)]])
    #else:
      #self.JacVelSymUp = Matrix([[simplify(diff(pose_point[0,0],k1)), simplify(diff(pose_point[0,0],p1)), simplify(diff(pose_point[0,0],s1)), simplify(diff(pose_point[0,0],k2)), simplify(diff(pose_point[0,0],p2)),simplify(diff(pose_point[0,0],s2)), simplify(diff(pose_point[0,0],k3)), simplify(diff(pose_point[0,0],p3)),simplify(diff(pose_point[0,0],s3))],
				  #[simplify(diff(pose_point[1,0],k1)), simplify(diff(pose_point[1,0],p1)), simplify(diff(pose_point[1,0],s1)), simplify(diff(pose_point[1,0],k2)), simplify(diff(pose_point[1,0],p2)),simplify(diff(pose_point[1,0],s2)), simplify(diff(pose_point[1,0],k3)), simplify(diff(pose_point[1,0],p3)),simplify(diff(pose_point[1,0],s3))],
				  #[simplify(diff(pose_point[2,0],k1)), simplify(diff(pose_point[2,0],p1)), simplify(diff(pose_point[2,0],s1)), simplify(diff(pose_point[2,0],k2)), simplify(diff(pose_point[2,0],p2)),simplify(diff(pose_point[2,0],s2)), simplify(diff(pose_point[2,0],k3)), simplify(diff(pose_point[2,0],p3)),simplify(diff(pose_point[2,0],s3))]])
    self.JacVelSymUp = Matrix([[diff(pose_point[0,0],xb), diff(pose_point[0,0],yb), diff(pose_point[0,0],zb), diff(pose_point[0,0],alphab), diff(pose_point[0,0],betab), diff(pose_point[0,0],gammab), diff(pose_point[0,0],k1), diff(pose_point[0,0],p1), diff(pose_point[0,0],s1), diff(pose_point[0,0],k2), diff(pose_point[0,0],p2),diff(pose_point[0,0],s2), diff(pose_point[0,0],k3), diff(pose_point[0,0],p3),diff(pose_point[0,0],s3)],
				  [diff(pose_point[1,0],xb), diff(pose_point[1,0],yb), diff(pose_point[1,0],zb), diff(pose_point[1,0],alphab), diff(pose_point[1,0],betab), diff(pose_point[1,0],gammab), diff(pose_point[1,0],k1), diff(pose_point[1,0],p1), diff(pose_point[1,0],s1), diff(pose_point[1,0],k2), diff(pose_point[1,0],p2),diff(pose_point[1,0],s2), diff(pose_point[1,0],k3), diff(pose_point[1,0],p3),diff(pose_point[1,0],s3)],
				  [diff(pose_point[2,0],xb), diff(pose_point[2,0],yb), diff(pose_point[2,0],zb), diff(pose_point[2,0],alphab), diff(pose_point[2,0],betab), diff(pose_point[2,0],gammab), diff(pose_point[2,0],k1), diff(pose_point[2,0],p1), diff(pose_point[2,0],s1), diff(pose_point[2,0],k2), diff(pose_point[2,0],p2),diff(pose_point[2,0],s2), diff(pose_point[2,0],k3), diff(pose_point[2,0],p3),diff(pose_point[2,0],s3)]])
    #return JacVelSym
    print(self.JacVelSymUp)
    self.JacVelFunc = lambdify((alphab, betab, gammab, k1, k2, k3, p1, p2, p3, s1, s2, s3), self.JacVelSymUp, "numpy")
    
  def init_jacobian_velocity_fast(self,segment_num):
    self.JacVelSymUp = Matrix([[(k1**2*k2*s1*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1**2*k3*s1*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k1*k2*k3*s1*sin(k1*s1) + k2*k3*(cos(k1*s1) - 1))*cos(p1)/(k1**2*k2*k3), (k1*k2*(-(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k3*s3) - 1)*sin(p3) + (cos(k3*s3) - 1)*(-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*cos(p3) - (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*sin(k3*s3)) + k1*k3*((cos(k2*s2) - 1)*sin(p1)*cos(p2)*cos(k1*s1) + (cos(k2*s2) - 1)*sin(p2)*cos(p1) - sin(p1)*sin(k1*s1)*sin(k2*s2)) + k2*k3*(cos(k1*s1) - 1)*sin(p1))/(k1*k2*k3), (k1*k2*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1*k3*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k2*k3*sin(k1*s1))*cos(p1)/(k2*k3), (k2**2*s2*((cos(k3*s3) - 1)*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(p3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3)) + k2*k3*s2*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1)) + k3*(-(cos(k2*s2) - 1)*sin(p1)*sin(p2) + (cos(k2*s2) - 1)*cos(p1)*cos(p2)*cos(k1*s1) - sin(k1*s1)*sin(k2*s2)*cos(p1)))/(k2**2*k3), (k2*(-(sin(p1)*sin(p2) - cos(p1)*cos(p2)*cos(k1*s1))*(cos(k3*s3) - 1)*sin(p3) + (sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3)*cos(k2*s2) - (sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*sin(k2*s2)*sin(k3*s3)) + k3*(sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k2*s2) - 1))/(k2*k3), (k2*(cos(k3*s3) - 1)*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(p3) - k2*(sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + k3*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1)))/k3, (k3*s3*(-(sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(k3*s3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3)*cos(p3)) - (sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k3*s3) - 1)*sin(p3) - (cos(k3*s3) - 1)*(sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*cos(p3) - (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*sin(k3*s3))/k3**2, ((sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*cos(p3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(p3))*(cos(k3*s3) - 1)/k3, -(sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(k3*s3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3)*cos(p3)], [(k1**2*k2*s1*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1**2*k3*s1*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k1*k2*k3*s1*sin(k1*s1) + k2*k3*(cos(k1*s1) - 1))*sin(p1)/(k1**2*k2*k3), (k1*k2*((sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k3*s3) - 1)*sin(p3) + (cos(k3*s3) - 1)*(sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*cos(p3) + (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*sin(k3*s3)) + k1*k3*((cos(k2*s2) - 1)*sin(p1)*sin(p2) - (cos(k2*s2) - 1)*cos(p1)*cos(p2)*cos(k1*s1) + sin(k1*s1)*sin(k2*s2)*cos(p1)) - k2*k3*(cos(k1*s1) - 1)*cos(p1))/(k1*k2*k3), (k1*k2*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1*k3*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k2*k3*sin(k1*s1))*sin(p1)/(k2*k3), (k2**2*s2*((cos(k3*s3) - 1)*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(p3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3)) + k2*k3*s2*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1)) + k3*((cos(k2*s2) - 1)*sin(p1)*cos(p2)*cos(k1*s1) + (cos(k2*s2) - 1)*sin(p2)*cos(p1) - sin(p1)*sin(k1*s1)*sin(k2*s2)))/(k2**2*k3), (k2*((sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k3*s3) - 1)*cos(p3)*cos(k2*s2) - (sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*sin(k2*s2)*sin(k3*s3) + (sin(p1)*cos(p2)*cos(k1*s1) + sin(p2)*cos(p1))*(cos(k3*s3) - 1)*sin(p3)) + k3*(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k2*s2) - 1))/(k2*k3), (k2*(cos(k3*s3) - 1)*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(p3) + k2*(-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3) + k3*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1)))/k3, (k3*s3*(-(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3)*cos(p3) + (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(k3*s3)) - (sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k3*s3) - 1)*sin(p3) + (cos(k3*s3) - 1)*(-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*cos(p3) - (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*sin(k3*s3))/k3**2, ((sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*cos(p3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(p3))*(cos(k3*s3) - 1)/k3, -(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3)*cos(p3) + (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(k3*s3)], [(-k1**2*k2*s1*((sin(k1*s1)*sin(k2*s2) - cos(p2)*cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) + (sin(k1*s1)*cos(k2*s2) + sin(k2*s2)*cos(p2)*cos(k1*s1))*sin(k3*s3) + (cos(k3*s3) - 1)*sin(p2)*sin(p3)*cos(k1*s1)) + k1**2*k3*s1*((cos(k2*s2) - 1)*cos(p2)*cos(k1*s1) - sin(k1*s1)*sin(k2*s2)) + k1*k2*k3*s1*cos(k1*s1) - k2*k3*sin(k1*s1))/(k1**2*k2*k3), 0, (-k1*k2*((sin(k1*s1)*sin(k2*s2) - cos(p2)*cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) + (sin(k1*s1)*cos(k2*s2) + sin(k2*s2)*cos(p2)*cos(k1*s1))*sin(k3*s3) + (cos(k3*s3) - 1)*sin(p2)*sin(p3)*cos(k1*s1)) + k1*k3*((cos(k2*s2) - 1)*cos(p2)*cos(k1*s1) - sin(k1*s1)*sin(k2*s2)) + k2*k3*cos(k1*s1))/(k2*k3), (-k2**2*s2*((sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3)) + k2*k3*s2*(-sin(k1*s1)*sin(k2*s2)*cos(p2) + cos(k1*s1)*cos(k2*s2)) - k3*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)))/(k2**2*k3), (k2*(-(cos(k3*s3) - 1)*sin(p2)*cos(p3)*cos(k2*s2) - (cos(k3*s3) - 1)*sin(p3)*cos(p2) + sin(p2)*sin(k2*s2)*sin(k3*s3)) - k3*(cos(k2*s2) - 1)*sin(p2))*sin(k1*s1)/(k2*k3), (-k2*(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) - k2*(sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3) + k3*(-sin(k1*s1)*sin(k2*s2)*cos(p2) + cos(k1*s1)*cos(k2*s2)))/k3, (k3*s3*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*cos(k3*s3) - (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3)*cos(p3) + sin(p2)*sin(p3)*sin(k1*s1)*sin(k3*s3)) + (sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) - (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) + (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1))/k3**2, -((sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(p3) + sin(p2)*sin(k1*s1)*cos(p3))*(cos(k3*s3) - 1)/k3, -(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*cos(k3*s3) - (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3)*cos(p3) + sin(p2)*sin(p3)*sin(k1*s1)*sin(k3*s3)]])
    self.JacVelFunc = lambdify((k1, k2, k3, p1, p2, p3, s1, s2, s3), self.JacVelSymUp, "numpy")

  def jacobian_velocity(self, segment_num):
    if(segment_num == 1):
      self.JacVelSym = self.JacVelSymBot
    elif(segment_num == 2):
      self.JacVelSym = self.JacVelSymMid
    elif(segment_num == 3):
      self.JacVelSym = self.JacVelSymUp
    #print(JacVelSymUp)
    #self.Jv = self.JacVelSym.subs([(k1, self.k[0]),(k2, self.k[1]),(k3, self.k[2]),(p1, self.p[0]),(p2, self.p[1]),(p3, self.p[2]),(s1, self.s[0]),(s2, self.s[1]),(s3, self.s[2])])
    self.Jv = self.JacVelFunc(self.angle_base[0], self.angle_base[1], self.angle_base[2], self.k[0], self.k[1], self.k[2], self.p[0], self.p[1], self.p[2], self.s[0], self.s[1], self.s[2])
    #return JacVel

  def init_jacobian_omega(self):
    T = homogeneous_transform_sym(3)
    JacOmega11 = diff(T[2,0],k1)*T[1,0]+diff(T[2,1],k1)*T[1,1]+diff(T[2,2],k1)*T[1,2];
    JacOmega21 = diff(T[0,0],k1)*T[2,0]+diff(T[0,1],k1)*T[2,1]+diff(T[0,2],k1)*T[2,2];
    JacOmega31 = diff(T[1,0],k1)*T[0,0]+diff(T[1,1],k1)*T[0,1]+diff(T[1,2],k1)*T[0,2];
    JacOmega12 = diff(T[2,0],p1)*T[1,0]+diff(T[2,1],p1)*T[1,1]+diff(T[2,2],p1)*T[1,2];
    JacOmega22 = diff(T[0,0],p1)*T[2,0]+diff(T[0,1],p1)*T[2,1]+diff(T[0,2],p1)*T[2,2];
    JacOmega32 = diff(T[1,0],p1)*T[0,0]+diff(T[1,1],p1)*T[0,1]+diff(T[1,2],p1)*T[0,2];
    JacOmega13 = diff(T[2,0],s1)*T[1,0]+diff(T[2,1],s1)*T[1,1]+diff(T[2,2],s1)*T[1,2];
    JacOmega23 = diff(T[0,0],s1)*T[2,0]+diff(T[0,1],s1)*T[2,1]+diff(T[0,2],s1)*T[2,2];
    JacOmega33 = diff(T[1,0],s1)*T[0,0]+diff(T[1,1],s1)*T[0,1]+diff(T[1,2],s1)*T[0,2];
    JacOmega14 = diff(T[2,0],k2)*T[1,0]+diff(T[2,1],k2)*T[1,1]+diff(T[2,2],k2)*T[1,2];
    JacOmega24 = diff(T[0,0],k2)*T[2,0]+diff(T[0,1],k2)*T[2,1]+diff(T[0,2],k2)*T[2,2];
    JacOmega34 = diff(T[1,0],k2)*T[0,0]+diff(T[1,1],k2)*T[0,1]+diff(T[1,2],k2)*T[0,2];
    JacOmega15 = diff(T[2,0],p2)*T[1,0]+diff(T[2,1],p2)*T[1,1]+diff(T[2,2],p2)*T[1,2];
    JacOmega25 = diff(T[0,0],p2)*T[2,0]+diff(T[0,1],p2)*T[2,1]+diff(T[0,2],p2)*T[2,2];
    JacOmega35 = diff(T[1,0],p2)*T[0,0]+diff(T[1,1],p2)*T[0,1]+diff(T[1,2],p2)*T[0,2];
    JacOmega16 = diff(T[2,0],s2)*T[1,0]+diff(T[2,1],s2)*T[1,1]+diff(T[2,2],s2)*T[1,2];
    JacOmega26 = diff(T[0,0],s2)*T[2,0]+diff(T[0,1],s2)*T[2,1]+diff(T[0,2],s2)*T[2,2];
    JacOmega36 = diff(T[1,0],s2)*T[0,0]+diff(T[1,1],s2)*T[0,1]+diff(T[1,2],s2)*T[0,2];
    JacOmega17 = diff(T[2,0],k3)*T[1,0]+diff(T[2,1],k3)*T[1,1]+diff(T[2,2],k3)*T[1,2];
    JacOmega27 = diff(T[0,0],k3)*T[2,0]+diff(T[0,1],k3)*T[2,1]+diff(T[0,2],k3)*T[2,2];
    JacOmega37 = diff(T[1,0],k3)*T[0,0]+diff(T[1,1],k3)*T[0,1]+diff(T[1,2],k3)*T[0,2];
    JacOmega18 = diff(T[2,0],p3)*T[1,0]+diff(T[2,1],p3)*T[1,1]+diff(T[2,2],p3)*T[1,2];
    JacOmega28 = diff(T[0,0],p3)*T[2,0]+diff(T[0,1],p3)*T[2,1]+diff(T[0,2],p3)*T[2,2];
    JacOmega38 = diff(T[1,0],p3)*T[0,0]+diff(T[1,1],p3)*T[0,1]+diff(T[1,2],p3)*T[0,2];
    JacOmega19 = diff(T[2,0],s3)*T[1,0]+diff(T[2,1],s3)*T[1,1]+diff(T[2,2],s3)*T[1,2];
    JacOmega29 = diff(T[0,0],s3)*T[2,0]+diff(T[0,1],s3)*T[2,1]+diff(T[0,2],s3)*T[2,2];
    JacOmega39 = diff(T[1,0],s3)*T[0,0]+diff(T[1,1],s3)*T[0,1]+diff(T[1,2],s3)*T[0,2];
    self.JacOmegaSym = Matrix([[JacOmega11, JacOmega12, JacOmega13, JacOmega14, JacOmega15, JacOmega16, JacOmega17, JacOmega18, JacOmega19],
		      [JacOmega21, JacOmega22, JacOmega23, JacOmega24, JacOmega25, JacOmega26, JacOmega27, JacOmega28, JacOmega29],
		      [JacOmega31, JacOmega32, JacOmega33, JacOmega34, JacOmega35, JacOmega36, JacOmega37, JacOmega38, JacOmega39]])
    #print(JacOmegaSym)
    #return JacOmegaSym
    
  def jacobian_omega(self):
    self.Jw = self.JacOmegaSym.subs([(k1, self.k[0]),(k2, self.k[1]),(k3, self.k[2]),(p1, self.p[0]),(p2, self.p[1]),(p3, self.p[2]),(s1, self.s[0]),(s2, self.s[1]),(s3, self.s[2])])
    
  def configuration_field(self,length_of_segment):
    self.config_field.data = []
    for i in range(0,3):
      vector_min = length_of_segment[i] - self.smin
      vector_max = length_of_segment[i] - self.smax
      rhomin = sqrt(vector_min*vector_min)
      rhomax = sqrt(vector_max*vector_max)
      #print(rhomax)
      #print(rhomin)
      if(rhomin<self.slim):
	self.vel[i] = self.K_config*vector_min*(1/rhomin - 1/self.slim)/pow(rhomin,3)
      elif(rhomax<self.slim):
	self.vel[i] = self.K_config*vector_max*(1/rhomax - 1/self.slim)/pow(rhomax,3)
      else:
	self.vel[i] = 0
      #print(self.vel[i])
      self.config_field.data.append(self.vel[i])
    
if __name__ == '__main__':
    try:
        inv_jacobi()
    except rospy.ROSInterruptException: pass
