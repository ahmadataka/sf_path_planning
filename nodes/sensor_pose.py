#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import math
import rospy
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseArray
from std_msgs.msg import Float64MultiArray
from sympy import *
from sympy.mpmath import norm
import numpy as np

k1, k2, k3, p1, p2, p3, s1, s2, s3 = symbols('k1 k2 k3 p1 p2 p3 s1 s2 s3')
xb, yb, zb, alphab, betab, gammab = symbols('xb yb zb alphab betab gammab')
flag_base = 0

def homogeneous_transform_sym(segment_num):
    #TransMatrix = Matrix([[cos(p1)*cos(k1*s1), -sin(p1), cos(p1)*sin(k1*s1), cos(p1)*(1-cos(k1*s1))/k1],
			  #[sin(p1)*cos(k1*s1), cos(p1), sin(p1)*sin(k1*s1), sin(p1)*(1-cos(k1*s1))/k1],
			  #[-sin(k1*s1), 0, cos(k1*s1), sin(k1*s1)/k1],
			  #[0, 0, 0, 1]])
    if(flag_base == 1):
      Tb = Matrix([ [cos(alphab)*cos(betab), cos(alphab)*sin(betab)*sin(gammab)-sin(alphab)*cos(gammab), cos(alphab)*sin(betab)*cos(gammab)+sin(alphab)*sin(gammab), xb],
		    [sin(alphab)*cos(betab), sin(alphab)*sin(betab)*sin(gammab)+cos(alphab)*cos(gammab), sin(alphab)*sin(betab)*cos(gammab)-cos(alphab)*sin(gammab), yb],
		    [-sin(betab), cos(betab)*sin(gammab), cos(betab)*cos(gammab), zb],
		    [0, 0, 0, 1]])
    else:
      Tb = eye(4)
      
    if (segment_num == 1):
      #TM = TransMatrix
      TM = Tb*Matrix([[cos(p1)*cos(k1*s1), -sin(p1), sin(k1*s1)*cos(p1), (-cos(k1*s1) + 1)*cos(p1)/k1], [sin(p1)*cos(k1*s1), cos(p1), sin(p1)*sin(k1*s1), (-cos(k1*s1) + 1)*sin(p1)/k1], [-sin(k1*s1), 0, cos(k1*s1), sin(k1*s1)/k1], [0, 0, 0, 1]])
    elif(segment_num == 2):
      #TM = TransMatrix*(TransMatrix.subs([(k1, k2),(p1, p2),(s1, s2)]))
      TM = Tb*Matrix([[-sin(p1)*sin(p2)*cos(k2*s2) - sin(k1*s1)*sin(k2*s2)*cos(p1) + cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2), -sin(p1)*cos(p2) - sin(p2)*cos(p1)*cos(k1*s1), -sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1), -(-cos(k2*s2) + 1)*sin(p1)*sin(p2)/k2 + (-cos(k2*s2) + 1)*cos(p1)*cos(p2)*cos(k1*s1)/k2 + sin(k1*s1)*sin(k2*s2)*cos(p1)/k2 + (-cos(k1*s1) + 1)*cos(p1)/k1], [-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2), -sin(p1)*sin(p2)*cos(k1*s1) + cos(p1)*cos(p2), sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1), (-cos(k2*s2) + 1)*sin(p1)*cos(p2)*cos(k1*s1)/k2 + (-cos(k2*s2) + 1)*sin(p2)*cos(p1)/k2 + sin(p1)*sin(k1*s1)*sin(k2*s2)/k2 + (-cos(k1*s1) + 1)*sin(p1)/k1], [-sin(k1*s1)*cos(p2)*cos(k2*s2) - sin(k2*s2)*cos(k1*s1), sin(p2)*sin(k1*s1), -sin(k1*s1)*sin(k2*s2)*cos(p2) + cos(k1*s1)*cos(k2*s2), -(-cos(k2*s2) + 1)*sin(k1*s1)*cos(p2)/k2 + sin(k2*s2)*cos(k1*s1)/k2 + sin(k1*s1)/k1], [0, 0, 0, 1]])
    else:
      #TM = TransMatrix*(TransMatrix.subs([(k1, k2),(p1, p2),(s1, s2)]))*(TransMatrix.subs([(k1, k3),(p1, p3),(s1, s3)]))
      TM = Tb*Matrix([[(-sin(p1)*cos(p2) - sin(p2)*cos(p1)*cos(k1*s1))*sin(p3)*cos(k3*s3) - (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*sin(k3*s3) + (-sin(p1)*sin(p2)*cos(k2*s2) - sin(k1*s1)*sin(k2*s2)*cos(p1) + cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*cos(p3)*cos(k3*s3), (-sin(p1)*cos(p2) - sin(p2)*cos(p1)*cos(k1*s1))*cos(p3) - (-sin(p1)*sin(p2)*cos(k2*s2) - sin(k1*s1)*sin(k2*s2)*cos(p1) + cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(p3), (-sin(p1)*cos(p2) - sin(p2)*cos(p1)*cos(k1*s1))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(k3*s3) + (-sin(p1)*sin(p2)*cos(k2*s2) - sin(k1*s1)*sin(k2*s2)*cos(p1) + cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3)*cos(p3), (-sin(p1)*cos(p2) - sin(p2)*cos(p1)*cos(k1*s1))*(-cos(k3*s3) + 1)*sin(p3)/k3 + (-cos(k3*s3) + 1)*(-sin(p1)*sin(p2)*cos(k2*s2) - sin(k1*s1)*sin(k2*s2)*cos(p1) + cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*cos(p3)/k3 + (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*sin(k3*s3)/k3 - (-cos(k2*s2) + 1)*sin(p1)*sin(p2)/k2 + (-cos(k2*s2) + 1)*cos(p1)*cos(p2)*cos(k1*s1)/k2 + sin(k1*s1)*sin(k2*s2)*cos(p1)/k2 + (-cos(k1*s1) + 1)*cos(p1)/k1], [(-sin(p1)*sin(p2)*cos(k1*s1) + cos(p1)*cos(p2))*sin(p3)*cos(k3*s3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*cos(p3)*cos(k3*s3) - (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*sin(k3*s3), (-sin(p1)*sin(p2)*cos(k1*s1) + cos(p1)*cos(p2))*cos(p3) - (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(p3), (-sin(p1)*sin(p2)*cos(k1*s1) + cos(p1)*cos(p2))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3)*cos(p3) + (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(k3*s3), (-sin(p1)*sin(p2)*cos(k1*s1) + cos(p1)*cos(p2))*(-cos(k3*s3) + 1)*sin(p3)/k3 + (-cos(k3*s3) + 1)*(-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*cos(p3)/k3 + (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*sin(k3*s3)/k3 + (-cos(k2*s2) + 1)*sin(p1)*cos(p2)*cos(k1*s1)/k2 + (-cos(k2*s2) + 1)*sin(p2)*cos(p1)/k2 + sin(p1)*sin(k1*s1)*sin(k2*s2)/k2 + (-cos(k1*s1) + 1)*sin(p1)/k1], [-(-sin(k1*s1)*sin(k2*s2)*cos(p2) + cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (-sin(k1*s1)*cos(p2)*cos(k2*s2) - sin(k2*s2)*cos(k1*s1))*cos(p3)*cos(k3*s3) + sin(p2)*sin(p3)*sin(k1*s1)*cos(k3*s3), -(-sin(k1*s1)*cos(p2)*cos(k2*s2) - sin(k2*s2)*cos(k1*s1))*sin(p3) + sin(p2)*sin(k1*s1)*cos(p3), (-sin(k1*s1)*sin(k2*s2)*cos(p2) + cos(k1*s1)*cos(k2*s2))*cos(k3*s3) + (-sin(k1*s1)*cos(p2)*cos(k2*s2) - sin(k2*s2)*cos(k1*s1))*sin(k3*s3)*cos(p3) + sin(p2)*sin(p3)*sin(k1*s1)*sin(k3*s3), (-sin(k1*s1)*sin(k2*s2)*cos(p2) + cos(k1*s1)*cos(k2*s2))*sin(k3*s3)/k3 + (-sin(k1*s1)*cos(p2)*cos(k2*s2) - sin(k2*s2)*cos(k1*s1))*(-cos(k3*s3) + 1)*cos(p3)/k3 + (-cos(k3*s3) + 1)*sin(p2)*sin(p3)*sin(k1*s1)/k3 - (-cos(k2*s2) + 1)*sin(k1*s1)*cos(p2)/k2 + sin(k2*s2)*cos(k1*s1)/k2 + sin(k1*s1)/k1], [0, 0, 0, 1]])
    
    return TM
    
def homogeneous_transform(kappa_sym, phi_sym, s_sym):
    T = Matrix([[cos(phi_sym)*cos(kappa_sym*s_sym), -sin(phi_sym), cos(phi_sym)*sin(kappa_sym*s_sym), cos(phi_sym)*(1-cos(kappa_sym*s_sym))/kappa_sym],
		[sin(phi_sym)*cos(kappa_sym*s_sym), cos(phi_sym), sin(phi_sym)*sin(kappa_sym*s_sym), sin(phi_sym)*(1-cos(kappa_sym*s_sym))/kappa_sym],
		[-sin(kappa_sym*s_sym), 0, cos(kappa_sym*s_sym), sin(kappa_sym*s_sym)/kappa_sym],
		[0, 0, 0, 1]])
    return T
    
def homogeneous_transform_base(x, y, z, alpha, beta, gamma):
    Tb = Matrix([ [cos(alpha)*cos(beta), cos(alpha)*sin(beta)*sin(gamma)-sin(alpha)*cos(gamma), cos(alpha)*sin(beta)*cos(gamma)+sin(alpha)*sin(gamma), x],
		  [sin(alpha)*cos(beta), sin(alpha)*sin(beta)*sin(gamma)+cos(alpha)*cos(gamma), sin(alpha)*sin(beta)*cos(gamma)-cos(alpha)*sin(gamma), y],
		  [-sin(beta), cos(beta)*sin(gamma), cos(beta)*cos(gamma), z],
		  [0, 0, 0, 1]])
    return Tb
    
def homo_to_pose(T):
    pose = Pose()
    pose.position.x = T[0,3]
    pose.position.y = T[1,3]
    pose.position.z = T[2,3]
    pose.orientation.w = 0.5*sqrt(1+T[0,0]+T[1,1]+T[2,2])
    pose.orientation.x = (T[2,1]-T[1,2])/(4*pose.orientation.w)
    pose.orientation.y = (T[0,2]-T[2,0])/(4*pose.orientation.w)
    pose.orientation.z = (T[1,0]-T[0,1])/(4*pose.orientation.w)
    
    return pose
    
def homo_to_pose_sym(T):
    position = Matrix([[T[0,3]],[T[1,3]],[T[2,3]]])
    #print(position)
    return position

class sensor_broadcast(object):
  def __init__(self):
    rospy.init_node('sensor_pose_node')
    self.field_type = rospy.get_param('/fieldtype')
    self.obs_num = rospy.get_param('/obs_num')
    self.base = rospy.get_param('/base')
    flag_base = self.base
    self.max_vel = 0.02
    #self.max_vel = 0.004
    self.maks = 0
    self.mini = 1000.0
    self.go_to_goal = zeros(3,1)
    #k, p, s = symbols('k p s')
    self.init_jacobian_velocity()
    #print(self.JacVelFuncBot)
    #print(self.JacVelFuncMid)
    #print(self.JacVelFuncUp)
    self.k = [0.706325262037712, 0.706325262037712, 0.706325262037712]
    self.p = [2.09439510239320, -1.04719755119660, 2.09439510239320]
    self.s = [0.122, 0.122, 0.122]
    self.position_base = [0.0, 0.0, 0.0]
    self.angle_base = [0.0, 0.0, 0.0]

    self.obs = []
    for i in range(0, self.obs_num):
      self.obs.append(Matrix([[1], [1], [1]]))
    sensor_pose = PoseArray()
    self.send_obs = Float64MultiArray()
    self.num_per_seg = 5
    #self.rho_0 = 0.75
    #self.K2 = 0.05
    self.rho_0 = 0.025
    #self.rho_0 = 0.04
    #self.K2 = 0.000005
    self.K2 = 0.0000003
    #homo_mat_bot = homogeneous_transform_sym(1)
    #homo_mat_mid = homogeneous_transform_sym(2)
    #homo_mat_up = homogeneous_transform_sym(3)
    #print(homo_mat_bot)
    #print(homo_mat_mid)
    #print(homo_mat_up)
    obs_sub = rospy.Subscriber('obs_pose', PoseArray, self.get_obs)
    config_space_sub = rospy.Subscriber('configuration_space', Float64MultiArray, self.get_kps)
    sensor_pub = rospy.Publisher('sensor_pose', PoseArray, queue_size = 10)
    self.vel_obs_pub = rospy.Publisher('/obs_field_configuration_space', Float64MultiArray, queue_size = 10)
    self.rec_vel = rospy.Subscriber('/field', Float64MultiArray, self.vel_callback)
    
    r = rospy.Rate(200)
    while not rospy.is_shutdown():
	sensor_ = eye(4)
	sensor_pose.poses = []
	if(self.base == 1):
	  sensor_ = sensor_*homogeneous_transform_base(self.position_base[0], self.position_base[1], self.position_base[2], self.angle_base[0], self.angle_base[1], self.angle_base[2])
	  field_sum = zeros(15,1)
	else:
	  field_sum = zeros(9,1)
	for i in range(0,3):
	  for j in range(0,self.num_per_seg):
	    sensor_matrix = sensor_*homogeneous_transform(self.k[i], self.p[i], (j+1)*self.s[i]/(self.num_per_seg))
	    sensor_pose.poses.append(homo_to_pose(sensor_matrix))
	    self.calc_sensor_jacobian(i+1, j)
	    for h in range(0, self.obs_num):
	      self.obstacle_field(homo_to_pose_sym(sensor_matrix),h)
	      #print(self.velocity)
	      if(self.field_type == 2):
		self.tip_jacobian = self.jacobian_velocity(3, [self.angle_base[0], self.angle_base[1], self.angle_base[2]], [self.k[0], self.k[1], self.k[2]], [self.p[0], self.p[1], self.p[2]], [self.s[0], self.s[1], self.s[2]])
		if(self.base==0):
		  field_sum += self.alpha_n*np.linalg.pinv(self.sensor_jacobian*(eye(9)-np.linalg.pinv(self.tip_jacobian)*self.tip_jacobian))*(self.alpha_o*self.velocity-self.sensor_jacobian*np.linalg.pinv(self.tip_jacobian)*self.go_to_goal)
		else:
		  field_sum += self.alpha_n*np.linalg.pinv(self.sensor_jacobian*(eye(15)-np.linalg.pinv(self.tip_jacobian)*self.tip_jacobian))*(self.alpha_o*self.velocity-self.sensor_jacobian*np.linalg.pinv(self.tip_jacobian)*self.go_to_goal)
	      elif(self.field_type == 3):
		self.tip_jacobian = self.jacobian_velocity(3, [self.angle_base[0], self.angle_base[1], self.angle_base[2]], [self.k[0], self.k[1], self.k[2]], [self.p[0], self.p[1], self.p[2]], [self.s[0], self.s[1], self.s[2]])
		if(self.base==0):
		  field_sum += np.linalg.pinv(self.sensor_jacobian*(eye(9)-np.linalg.pinv(self.tip_jacobian)*self.tip_jacobian))*(self.velocity-self.sensor_jacobian*np.linalg.pinv(self.tip_jacobian)*self.go_to_goal)
		else:
		  field_sum += np.linalg.pinv(self.sensor_jacobian*(eye(15)-np.linalg.pinv(self.tip_jacobian)*self.tip_jacobian))*(self.velocity-self.sensor_jacobian*np.linalg.pinv(self.tip_jacobian)*self.go_to_goal)
	      else:
		field_sum += np.linalg.pinv(self.sensor_jacobian)*self.velocity
	  sensor_ = sensor_matrix
	#print(field_sum)
	self.send_obs_field(field_sum)
	sensor_pub.publish(sensor_pose);
	#print(self.s)
        r.sleep()
        #rospy.spin()
        
  def get_kps(self,kps):
    if(self.base == 1):
      self.position_base = [kps.data[0], kps.data[1], kps.data[2]]
      self.angle_base = [kps.data[3], kps.data[4], kps.data[5]]
      self.k = [kps.data[6], kps.data[9], kps.data[12]]
      self.p = [kps.data[7], kps.data[10], kps.data[13]]
      self.s = [kps.data[8], kps.data[11], kps.data[14]]
    else:
      self.k = [kps.data[0], kps.data[3], kps.data[6]]
      self.p = [kps.data[1], kps.data[4], kps.data[7]]
      self.s = [kps.data[2], kps.data[5], kps.data[8]]
      
    
  def calc_sensor_jacobian(self, segment_num, sensor_num):
    if(segment_num == 1):
      self.sensor_jacobian = self.jacobian_velocity(segment_num, [self.angle_base[0], self.angle_base[1], self.angle_base[2]], [self.k[0], 0, 0], [self.p[0], 0, 0], [(sensor_num+1)*self.s[segment_num-1]/(self.num_per_seg), 0, 0])
    elif(segment_num == 2):
      self.sensor_jacobian = self.jacobian_velocity(segment_num, [self.angle_base[0], self.angle_base[1], self.angle_base[2]], [self.k[0], self.k[1], 0], [self.p[0], self.p[1], 0], [self.s[0], (sensor_num+1)*self.s[segment_num-1]/(self.num_per_seg), 0])
    elif(segment_num == 3):
      self.sensor_jacobian = self.jacobian_velocity(segment_num, [self.angle_base[0], self.angle_base[1], self.angle_base[2]], [self.k[0], self.k[1], self.k[2]], [self.p[0], self.p[1], self.p[2]], [self.s[0], self.s[1], (sensor_num+1)*self.s[segment_num-1]/(self.num_per_seg)])
      
  def get_obs(self,msg):
    for i in range(0, self.obs_num):
      self.obs[i] = Matrix([[msg.poses[i].position.x],[msg.poses[i].position.y],[msg.poses[i].position.z]])
    
  def vel_callback(self, msg):
    self.go_to_goal = Matrix([[msg.data[0]],[msg.data[1]],[msg.data[2]]])

  def obstacle_field(self,pose_of_sensor,obs_index):
      self.radius = 0.0134
      self.obs_range = 0.01
      vector_obs = pose_of_sensor - self.obs[obs_index]
      #print(vector_obs)
      rho = sqrt((vector_obs.T*vector_obs).tolist()[0][0])
      rho_modify = rho - self.radius - self.obs_range
      if(rho_modify < self.mini):
	self.mini = rho_modify
	print(self.mini)
      if(self.field_type == 0):
	if(rho_modify<self.rho_0):
	  #self.velocity = self.K2*vector_obs*(1/rho - 1/self.rho_0)/pow(rho,3)
	  self.velocity = self.K2*(vector_obs/rho)*(1/rho_modify - 1/self.rho_0)/pow(rho_modify,2)
	else:
	  self.velocity = zeros(3,1)
      elif((self.field_type == 1)):
	if(rho_modify<self.rho_0):
	  #self.velocity = self.K2*vector_obs*(1/rho - 1/self.rho_0)/pow(rho,3)
	  self.velocity = self.K2*(vector_obs/rho)*(1/rho_modify - 1/self.rho_0)/pow(rho_modify,2)
	  #print(self.velocity)
	  for i in range(0,3):
	    if(self.velocity[i,0]>self.max_vel):
	      self.velocity[i,0] = self.max_vel
	    elif(self.velocity[i,0]<-self.max_vel):
	      self.velocity[i,0] = -self.max_vel
	else:
	  self.velocity = zeros(3,1)
      elif((self.field_type == 2)):
	if(rho<self.rho_0/2):
	  self.alpha_n = 1
	elif(rho>=self.rho_0/2 and rho<self.rho_0):
	  self.alpha_n = pow(cos(pi/2*(rho-self.rho_0/2)/(self.rho_0-self.rho_0/2)),2)
	else:
	  self.alpha_n = 0
	self.alpha_n = 10*self.alpha_n
	if(rho<self.rho_0/2):
	  self.alpha_o = 10*(1-pow(sin(pi/2*rho/(self.rho_0/2)),(0.25)))
	else:
	  self.alpha_o = 0
	self.velocity = vector_obs/norm(vector_obs)
      elif((self.field_type == 3)):
	if(rho<self.rho_0):
	  self.velocity = self.K2*vector_obs*(1/rho - 1/self.rho_0)/pow(rho,3)
	  for i in range(0,3):
	    if(self.velocity[i,0]>self.max_vel):
	      self.velocity[i,0] = self.max_vel
	    elif(self.velocity[i,0]<-self.max_vel):
	      self.velocity[i,0] = -self.max_vel
	else:
	  self.velocity = zeros(3,1)
	  
      for i in range(0,3):
	if(abs(self.velocity[i,0])>self.maks):
	  self.maks = abs(self.velocity[i,0])
      #print(self.velocity)
      
  def send_obs_field(self,field_sum):
    self.send_obs.data = []
    field_sent = field_sum.tolist()
    #print(field_sent[0][1])
    if(self.base == 0):
      state_num = 9
    else:
      state_num = 15
    for i in range(0,state_num):
      #print(field_sent[0][i])
      self.send_obs.data.append(field_sent[i][0])
    self.vel_obs_pub.publish(self.send_obs)

  def init_jacobian_velocity(self):
    for segment_num in range(1,4):
      homo_matrix = homogeneous_transform_sym(segment_num)
      pose_point = homo_to_pose_sym(homo_matrix)
      if(self.base == 0):
	self.JacVelSym = Matrix([[diff(pose_point[0,0],k1), diff(pose_point[0,0],p1), diff(pose_point[0,0],s1), diff(pose_point[0,0],k2), diff(pose_point[0,0],p2),diff(pose_point[0,0],s2), diff(pose_point[0,0],k3), diff(pose_point[0,0],p3),diff(pose_point[0,0],s3)],
				  [diff(pose_point[1,0],k1), diff(pose_point[1,0],p1), diff(pose_point[1,0],s1), diff(pose_point[1,0],k2), diff(pose_point[1,0],p2),diff(pose_point[1,0],s2), diff(pose_point[1,0],k3), diff(pose_point[1,0],p3),diff(pose_point[1,0],s3)],
				  [diff(pose_point[2,0],k1), diff(pose_point[2,0],p1), diff(pose_point[2,0],s1), diff(pose_point[2,0],k2), diff(pose_point[2,0],p2),diff(pose_point[2,0],s2), diff(pose_point[2,0],k3), diff(pose_point[2,0],p3),diff(pose_point[2,0],s3)]])
        if(segment_num == 1):
	  self.JacVelFuncBot = lambdify((k1, k2, k3, p1, p2, p3, s1, s2, s3), self.JacVelSym, "numpy")
	elif(segment_num == 2):
	  self.JacVelFuncMid = lambdify((k1, k2, k3, p1, p2, p3, s1, s2, s3), self.JacVelSym, "numpy")
	else:
	  self.JacVelFuncUp = lambdify((k1, k2, k3, p1, p2, p3, s1, s2, s3), self.JacVelSym, "numpy")

      else:
	self.JacVelSym = Matrix([[diff(pose_point[0,0],xb), diff(pose_point[0,0],yb), diff(pose_point[0,0],zb), diff(pose_point[0,0],alphab), diff(pose_point[0,0],betab), diff(pose_point[0,0],gammab), diff(pose_point[0,0],k1), diff(pose_point[0,0],p1), diff(pose_point[0,0],s1), diff(pose_point[0,0],k2), diff(pose_point[0,0],p2),diff(pose_point[0,0],s2), diff(pose_point[0,0],k3), diff(pose_point[0,0],p3),diff(pose_point[0,0],s3)],
				  [diff(pose_point[1,0],xb), diff(pose_point[1,0],yb), diff(pose_point[1,0],zb), diff(pose_point[1,0],alphab), diff(pose_point[1,0],betab), diff(pose_point[1,0],gammab), diff(pose_point[1,0],k1), diff(pose_point[1,0],p1), diff(pose_point[1,0],s1), diff(pose_point[1,0],k2), diff(pose_point[1,0],p2),diff(pose_point[1,0],s2), diff(pose_point[1,0],k3), diff(pose_point[1,0],p3),diff(pose_point[1,0],s3)],
				  [diff(pose_point[2,0],xb), diff(pose_point[2,0],yb), diff(pose_point[2,0],zb), diff(pose_point[2,0],alphab), diff(pose_point[2,0],betab), diff(pose_point[2,0],gammab), diff(pose_point[2,0],k1), diff(pose_point[2,0],p1), diff(pose_point[2,0],s1), diff(pose_point[2,0],k2), diff(pose_point[2,0],p2),diff(pose_point[2,0],s2), diff(pose_point[2,0],k3), diff(pose_point[2,0],p3),diff(pose_point[2,0],s3)]])
	if(segment_num == 1):
	  self.JacVelFuncBot = lambdify((alphab, betab, gammab, k1, k2, k3, p1, p2, p3, s1, s2, s3), self.JacVelSym, "numpy")
	elif(segment_num == 2):
	  self.JacVelFuncMid = lambdify((alphab, betab, gammab, k1, k2, k3, p1, p2, p3, s1, s2, s3), self.JacVelSym, "numpy")
	else:
	  self.JacVelFuncUp = lambdify((alphab, betab, gammab, k1, k2, k3, p1, p2, p3, s1, s2, s3), self.JacVelSym, "numpy")

    #JacVel = JacVelSym.subs([(k1, k_vec[0]),(k2, k_vec[1]),(k3, k_vec[2]),(p1, p_vec[0]),(p2, p_vec[1]),(p3, p_vec[2]),(s1, s_vec[0]),(s2, s_vec[1]),(s3, s_vec[2])])
    #if(segment_num == 1):
      #print(JacVel)
    #return JacVel

  def jacobian_velocity(self, segment_num, angle_vec, k_vec, p_vec, s_vec):
    #homo_matrix = homogeneous_transform_sym(segment_num)
    #pose_point = homo_to_pose_sym(homo_matrix)
    #if(segment_num == 1):
      #print(pose_point)
    #JacVelSym = Matrix([[diff(pose_point[0,0],k1), diff(pose_point[0,0],p1), diff(pose_point[0,0],s1), diff(pose_point[0,0],k2), diff(pose_point[0,0],p2),diff(pose_point[0,0],s2), diff(pose_point[0,0],k3), diff(pose_point[0,0],p3),diff(pose_point[0,0],s3)],
		    #[diff(pose_point[1,0],k1), diff(pose_point[1,0],p1), diff(pose_point[1,0],s1), diff(pose_point[1,0],k2), diff(pose_point[1,0],p2),diff(pose_point[1,0],s2), diff(pose_point[1,0],k3), diff(pose_point[1,0],p3),diff(pose_point[1,0],s3)],
		    #[diff(pose_point[2,0],k1), diff(pose_point[2,0],p1), diff(pose_point[2,0],s1), diff(pose_point[2,0],k2), diff(pose_point[2,0],p2),diff(pose_point[2,0],s2), diff(pose_point[2,0],k3), diff(pose_point[2,0],p3),diff(pose_point[2,0],s3)]])
    #JacVel = JacVelSym.subs([(k1, k_vec[0]),(k2, k_vec[1]),(k3, k_vec[2]),(p1, p_vec[0]),(p2, p_vec[1]),(p3, p_vec[2]),(s1, s_vec[0]),(s2, s_vec[1]),(s3, s_vec[2])])
    #if(segment_num == 1):
      #print(JacVel)
    if(self.base == 0):
      if(segment_num == 1):
	JacVel = self.JacVelFuncBot(k_vec[0],k_vec[1],k_vec[2],p_vec[0],p_vec[1],p_vec[2],s_vec[0],s_vec[1],s_vec[2])
      elif(segment_num == 2):
	JacVel = self.JacVelFuncMid(k_vec[0],k_vec[1],k_vec[2],p_vec[0],p_vec[1],p_vec[2],s_vec[0],s_vec[1],s_vec[2])
      else:
	JacVel = self.JacVelFuncUp(k_vec[0],k_vec[1],k_vec[2],p_vec[0],p_vec[1],p_vec[2],s_vec[0],s_vec[1],s_vec[2])
    else:
      if(segment_num == 1):
	JacVel = self.JacVelFuncBot(angle_vec[0], angle_vec[1], angle_vec[2], k_vec[0],k_vec[1],k_vec[2],p_vec[0],p_vec[1],p_vec[2],s_vec[0],s_vec[1],s_vec[2])
      elif(segment_num == 2):
	JacVel = self.JacVelFuncMid(angle_vec[0], angle_vec[1], angle_vec[2], k_vec[0],k_vec[1],k_vec[2],p_vec[0],p_vec[1],p_vec[2],s_vec[0],s_vec[1],s_vec[2])
      else:
	JacVel = self.JacVelFuncUp(angle_vec[0], angle_vec[1], angle_vec[2], k_vec[0],k_vec[1],k_vec[2],p_vec[0],p_vec[1],p_vec[2],s_vec[0],s_vec[1],s_vec[2])
    return JacVel


if __name__ == '__main__':
    try:
        sensor_broadcast()
    except rospy.ROSInterruptException: pass
