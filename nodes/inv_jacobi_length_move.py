#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import math
import rospy
import numpy as np
from sympy import *
from scipy.integrate import ode
from std_msgs.msg import Float64MultiArray
from geometry_msgs.msg import Pose
from sf_path_planning.msg import sf_msg
from geometry_msgs.msg import PoseArray
from sympy.mpmath import norm

k1, k2, k3, p1, p2, p3, s1, s2, s3 = symbols('k1 k2 k3 p1 p2 p3 s1 s2 s3')
l11, l12, l13, l21, l22, l23, l31, l32, l33 = symbols('l11 l12 l13 l21 l22 l23 l31 l32 l33')
xb, yb, zb, alphab, betab, gammab = symbols('xb yb zb alphab betab gammab')

def homogeneous_transform_sym(segment_num):
    TransMatrix = Matrix([[cos(p1)*cos(k1*s1), -sin(p1), cos(p1)*sin(k1*s1), cos(p1)*(1-cos(k1*s1))/k1],
			  [sin(p1)*cos(k1*s1), cos(p1), sin(p1)*sin(k1*s1), sin(p1)*(1-cos(k1*s1))/k1],
			  [-sin(k1*s1), 0, cos(k1*s1), sin(k1*s1)/k1],
			  [0, 0, 0, 1]])
    Tb = Matrix([ [cos(alphab)*cos(betab), cos(alphab)*sin(betab)*sin(gammab)-sin(alphab)*cos(gammab), cos(alphab)*sin(betab)*cos(gammab)+sin(alphab)*sin(gammab), xb],
		  [sin(alphab)*cos(betab), sin(alphab)*sin(betab)*sin(gammab)+cos(alphab)*cos(gammab), sin(alphab)*sin(betab)*cos(gammab)-cos(alphab)*sin(gammab), yb],
		  [-sin(betab), cos(betab)*sin(gammab), cos(betab)*cos(gammab), zb],
		  [0, 0, 0, 1]])

    if (segment_num == 1):
      TM = Tb*TransMatrix
    elif(segment_num == 2):
      TM = Tb*TransMatrix*(TransMatrix.subs([(k1, k2),(p1, p2),(s1, s2)]))
    else:
      TM = Tb*TransMatrix*(TransMatrix.subs([(k1, k2),(p1, p2),(s1, s2)]))*(TransMatrix.subs([(k1, k3),(p1, p3),(s1, s3)]))
    
    return TM
    
def homo_to_pose_sym(T):
    position = Matrix([[T[0,3]],[T[1,3]],[T[2,3]]])
    #print(position)
    return position
    
def l_to_phi(l1, l2, l3):
    phi_out = atan2((sqrt(3)*(l2+l3-2*l1)),(3*(l2-l3)))
    #phi_out = atan((sqrt(3)*(l2+l3-2*l1))/(3*(l2-l3)))
    #print(trigsimp(phi_out))
    return phi_out.evalf()
      
class inv_jacobi(object):
  
  def __init__(self):
    rospy.init_node('inv_jacobi')
    ##self.obs_num = rospy.get_param('/obs_num')
    self.JacVelSym = zeros(3,9)
    self.JacVelSymBot = zeros(3,9)
    self.JacVelSymMid = zeros(3,9)
    self.JacVelSymUp = zeros(3,9)
    self.JacOmegaSym = zeros(3,9)
    self.Jw = zeros(3,9)
    self.Jv = zeros(3,9)
    #We assume this to be 10 times larger than it should be
    self.Lo = 0.1200
    d = 0.0134
    self.p1_ex = atan((sqrt(3)*(l12+l13-2*l11))/(3*(l12-l13)))
    self.k1_ex = 2*(sqrt(pow(l11,2)+pow(l12,2)+pow(l13,2)-l11*l12-l11*l13-l12*l13))/(d*(3*self.Lo+l11+l12+l13))
    self.s1_ex = (3*self.Lo+l11+l12+l13)/3
    self.p2_ex = atan((sqrt(3)*(l22+l23-2*l21))/(3*(l22-l23)))
    self.k2_ex = 2*(sqrt(pow(l21,2)+pow(l22,2)+pow(l23,2)-l21*l22-l21*l23-l22*l23))/(d*(3*self.Lo+l21+l22+l23))
    self.s2_ex = (3*self.Lo+l21+l22+l23)/3
    self.p3_ex = atan((sqrt(3)*(l32+l33-2*l31))/(3*(l32-l33)))
    self.k3_ex = 2*(sqrt(pow(l31,2)+pow(l32,2)+pow(l33,2)-l31*l32-l31*l33-l32*l33))/(d*(3*self.Lo+l31+l32+l33))
    self.s3_ex = (3*self.Lo+l31+l32+l33)/3
    
    self.k_out = lambdify((l11, l12, l13), self.k1_ex, "sympy")
    self.s_out = lambdify((l11, l12, l13), self.s1_ex, "sympy")
    
    self.init_jacobian_velocity(3)
    #self.init_jacobian_velocity_fast(3)
    self.init_jacobian_kps_l()
    
    #print(simplify(sin(k1)**2+cos(k1)**2))
    #just_try = Matrix([[sin(k1)+k2], [k1+sin(k2)]])
    #out = lambdify((k1, k2), just_try, "numpy")
    #print(out(0, 2))
    #print((self.JacVelSymUp))
    #print(self.Jacobian_kps_l)
    pub = rospy.Publisher('/configuration_space', Float64MultiArray, queue_size = 10)
    length_pub = rospy.Publisher('/length', Float64MultiArray, queue_size = 10)
    base_pub = rospy.Publisher('/base_pose', Pose, queue_size = 10)
    trial_pub = rospy.Publisher('/configuration_field', Float64MultiArray, queue_size = 10)
    dot_pub = rospy.Publisher('/length_rate', Float64MultiArray, queue_size = 10)
    #diff_length_pub = rospy.Publisher('/length_difference', Float64MultiArray)
    self.rec_vel = rospy.Subscriber('/field', Float64MultiArray, self.vel_callback)
    self.rec_vel_obs = rospy.Subscriber('/obs_field_configuration_space', Float64MultiArray, self.obs_callback)
    self.kps = Float64MultiArray()
    self.length = Float64MultiArray()
    self.config_field = Float64MultiArray()
    self.diff_length = Float64MultiArray()
    self.error_flag = 0
    self.base_pose = Pose()

    #this is the state space initial value
    #in this case the segment's length component
    y0, t0 = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.001, 0.002, 0.003, 0.003, 0.002, 0.001, 0.001, 0.002, 0.003], 0.0
    #y0, t0 = [0.01, 0.0101, 0.01, 0.01, 0.0101, 0.01, 0.01, 0.0101, 0.01], 0.0
    self.l = y0[6:]
    self.y_dot = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00, 0.00]
    self.q_dot = Float64MultiArray()
    #print(self.l)
    self.k = [self.k_out(self.l[0],self.l[1],self.l[2]), self.k_out(self.l[3],self.l[4],self.l[5]), self.k_out(self.l[6],self.l[7],self.l[8])]
    self.p = [l_to_phi(self.l[0],self.l[1],self.l[2]),l_to_phi(self.l[3],self.l[4],self.l[5]),l_to_phi(self.l[6],self.l[7],self.l[8])]
    self.s = [self.s_out(self.l[0],self.l[1],self.l[2]), self.s_out(self.l[3],self.l[4],self.l[5]), self.s_out(self.l[6],self.l[7],self.l[8])]
    #self.l_diff = [abs(y0[0]-y0[1]), abs(y0[0]-y0[2]), abs(y0[1]-y0[2]), abs(y0[3]-y0[4]), abs(y0[3]-y0[5]), abs(y0[4]-y0[5]), abs(y0[6]-y0[7]), abs(y0[6]-y0[8]), abs(y0[7]-y0[8])]
    #print(self.k)
    #print(self.p)
    #print(self.s)
    self.position_base = [y0[0], y0[1], y0[2]]
    self.angle_base = [y0[3], y0[4], y0[5]]
    self.v_obs = zeros(15,1)
    self.v = [0, 0, 0]
    self.vel = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    
    # DELETE THIS Trial only
    self.velocity = Matrix([[1.0], [-1.0], [0.5]])
    
    self.smin = -0.3*self.Lo
    self.smax = 0.3*self.Lo
    self.slim = 0.005
    self.K_config = 0.0000001
    #print((atan2(sqrt(3),1)).evalf())
    #print(atan2((sqrt(3)*(0.2+0.3-2*0.1)),(3*(0.2-0.3))))
    #print(self.k)
    #print(self.p)
    #print(self.s)
    #r = ode(deriv).set_integrator('zvode', method='bdf')
    #r = ode(self.deriv).set_integrator('dopri5')
    #r = ode(self.deriv).set_integrator('dop853')
    r = ode(self.deriv).set_integrator('vode', method = 'bdf', order=5)
    #r = ode(self.deriv).set_integrator('zvode', method = 'bdf')
    r.set_initial_value(y0, t0)
    
    self.dt = 0.01
    
    flag_try = 0
    while (not rospy.is_shutdown()) and (self.error_flag==0):
      #saiki = rospy.Time.now()
      #print(self.k)
      #print("a")
      self.jacobian_velocity(3)
      self.jacobian_kps_l()
      #print(self.Jv)
      #self.jacobian_omega()
      #print(self.error_flag)
      #self.inv_Jv = self.pseudo_inv(Jv)
      #print(self.Jv)
      #print(self.JacobianKPSlNow)
      
      jumper_matrix = self.Jv*self.JacobianKPSlNow 
      dummy_matrix = self.Jvb.row_join(jumper_matrix)
      
      #print(dummy_matrix)
      self.inv_J = np.linalg.pinv(dummy_matrix)
      #self.trans_J = dummy_matrix.T
      #self.configuration_field(self.l)
      
      #IMPORTANT! constraint_potential
      self.constraint_potential(self.l)
      self.matrix_con_field = Matrix([[0], [0], [0], [0], [0], [0], [self.vel[0]],[self.vel[1]],[self.vel[2]],[self.vel[3]],[self.vel[4]],[self.vel[5]],[self.vel[6]],[self.vel[7]],[self.vel[8]]])
      
      #rampung = rospy.Time.now()
      #print((rampung.nsecs-saiki.nsecs))
      #print(embuh)
      #print(embuh2)
      #while r.successful() and r.t < t1:
   
      self.integral_length()
      if(flag_try==0):
	print(self.l)
	flag_try = 1
      #print(r.successful())
      #if r.successful():
	#print "a"
	#r.integrate(r.t+self.dt)
	##print "i"
	##print(r.t)
	##print(r.y[0])
        #self.update(r.y)
      
      self.transform_to_sent()
      pub.publish(self.kps)
      length_pub.publish(self.length)
      base_pub.publish(self.base_pose)
      trial_pub.publish(self.config_field)
      dot_pub.publish(self.q_dot)
      #print("A")
      #diff_length_pub.publish(self.diff_length)
      
  def vel_callback(self, msg):
    self.v[0] = msg.data[0]
    self.v[1] = msg.data[1]
    self.v[2] = msg.data[2]
    self.velocity = Matrix([[self.v[0]], [self.v[1]], [self.v[2]]])
    
  def obs_callback(self, msg):
    self.v_obs = Matrix([[msg.data[0]],[msg.data[1]],[msg.data[2]],[msg.data[3]],[msg.data[4]],[msg.data[5]],[msg.data[6]],[msg.data[7]],[msg.data[8]], [msg.data[9]], [msg.data[10]], [msg.data[11]], [msg.data[12]], [msg.data[13]], [msg.data[14]]])
    
          
  def deriv(self,t,y):  # return derivatives of the array y
    print("A")
    #velocity = Matrix([[0], [0], [5*sin(50*t)]])
    #oke = pseudo_inv(velocity)
    #velocity = 2*embuh
    #print(self.dummy)
    #print(self.embuh2_array)
    #print(self.embuh2.tolist())
    #dydt_mat = self.inv_J*velocity + self.v_obs
    #print(self.inv_J)
    #print(velocity.evalf())
    #print(self.v_obs)
    #Potential Field in Config Space
    dydt_mat = self.inv_J*self.velocity + self.v_obs# + self.matrix_con_field
    #dydt_mat = self.trans_J*velocity 
    #print(dydt_mat)
    dydt = dydt_mat.tolist()
    self.y_dot = dydt
    #print "a"
    #print(dydt)
    #Limit
    #for i in range(6,15):
      #if(y[i]<self.smin):
	#y[i] = self.smin
	#if(dydt[i][0]<0):
	  #dydt[i][0]=0
      #elif(y[i]>self.smax):
	#y[i] = self.smax
	#if(dydt[i][0]>0):
	  #dydt[i][0]=0
    #for i in range(6,15):
      #if(y[i]<self.smin):
	#if(dydt[i][0]<0):
	  #y[i] = self.smin
	  #dydt[i][0]=0
      #elif(y[i]>self.smax):
	#if(dydt[i][0]>0):
	  #y[i] = self.smax
	  #dydt[i][0]=0

    #print(dydt)
    return dydt
    
  def update(self,state):
    self.l = [state[6], state[7], state[8], state[9], state[10], state[11], state[12], state[13], state[14]]
    self.k = [self.k_out(self.l[0],self.l[1],self.l[2]), self.k_out(self.l[3],self.l[4],self.l[5]), self.k_out(self.l[6],self.l[7],self.l[8])]
    self.p = [l_to_phi(self.l[0],self.l[1],self.l[2]),l_to_phi(self.l[3],self.l[4],self.l[5]),l_to_phi(self.l[6],self.l[7],self.l[8])]
    self.s = [self.s_out(self.l[0],self.l[1],self.l[2]), self.s_out(self.l[3],self.l[4],self.l[5]), self.s_out(self.l[6],self.l[7],self.l[8])]
    self.position_base = [state[0], state[1], state[2]]
    self.angle_base = [state[3], state[4], state[5]]
    #self.l_diff = [abs(state[0]-state[1]), abs(state[0]-state[2]), abs(state[1]-state[2]), abs(state[3]-state[4]), abs(state[3]-state[5]), abs(state[4]-state[5]), abs(state[6]-state[7]), abs(state[6]-state[8]), abs(state[7]-state[8])]
    #self.singularity_checker()

  def integral_length(self):
    self.inverse_jacobian()
    self.position_base = [self.position_base[0]+self.vl[0][0]*self.dt, self.position_base[1]+self.vl[1][0]*self.dt, self.position_base[2]+self.vl[2][0]*self.dt]
    self.angle_base = [self.angle_base[0]+self.vl[3][0]*self.dt, self.angle_base[1]+self.vl[4][0]*self.dt, self.angle_base[2]+self.vl[5][0]*self.dt]
    self.l = [self.l[0]+self.vl[6][0]*self.dt, self.l[1]+self.vl[7][0]*self.dt, self.l[2]+self.vl[8][0]*self.dt, self.l[3]+self.vl[9][0]*self.dt, self.l[4]+self.vl[10][0]*self.dt, self.l[5]+self.vl[11][0]*self.dt, self.l[6]+self.vl[12][0]*self.dt, self.l[7]+self.vl[13][0]*self.dt, self.l[8]+self.vl[14][0]*self.dt]
    #print(self.l)
    self.k = [self.k_out(self.l[0],self.l[1],self.l[2]), self.k_out(self.l[3],self.l[4],self.l[5]), self.k_out(self.l[6],self.l[7],self.l[8])]
    self.p = [l_to_phi(self.l[0],self.l[1],self.l[2]),l_to_phi(self.l[3],self.l[4],self.l[5]),l_to_phi(self.l[6],self.l[7],self.l[8])]
    self.s = [self.s_out(self.l[0],self.l[1],self.l[2]), self.s_out(self.l[3],self.l[4],self.l[5]), self.s_out(self.l[6],self.l[7],self.l[8])]
    
  def inverse_jacobian(self):
    velocity = Matrix([self.v[0], self.v[1], self.v[2]])
    #dydt_mat = self.inv_J*velocity + self.v_obs #+ matrix_con_field
    dydt_mat = self.inv_J*self.velocity + self.v_obs + self.matrix_con_field
    self.vl = dydt_mat.tolist()
    #for i in range(0,9):
      #if(self.l[i]<self.smin):
	#if(self.vl[i+6][0]<0):
	  #self.l[i] = self.smin
	  #self.vl[i+6][0]=0
      #elif(self.l[i]>self.smax):
	#if(self.vl[i+6][0]>0):
	  #self.l[i] = self.smax
	  #self.vl[i+6][0]=0

    self.y_dot = dydt_mat
  
  def singularity_checker(self):
    #checker = [0.0, 0.0, 0.0]
    #for i in range(0,3):
      #for j in range(0,3):
	#checker[j] = self.l_diff[3*i+j]
      #if((checker[0]<1e-5) and (checker[1]<1e-5) and (checker[2]<1e-5)):
	#print "SINGULARITY in segment" + str(i)
    for i in range(0,3):
      if(self.k[i]<1e-5):
	print "SINGULARITY in segment" + str(i)
    
  def transform_to_sent(self):
    self.kps.data = []
    self.length.data = []
    self.diff_length.data = []
    self.q_dot.data = []
    self.kps.data.append(self.position_base[0])
    self.kps.data.append(self.position_base[1])
    self.kps.data.append(self.position_base[2])
    self.kps.data.append(self.angle_base[0])
    self.kps.data.append(self.angle_base[1])
    self.kps.data.append(self.angle_base[2])
    self.kps.data.append(self.k[0])
    self.kps.data.append(self.p[0])
    self.kps.data.append(self.s[0])
    self.kps.data.append(self.k[1])
    self.kps.data.append(self.p[1])
    self.kps.data.append(self.s[1])
    self.kps.data.append(self.k[2])
    self.kps.data.append(self.p[2])
    self.kps.data.append(self.s[2])
    self.length.data.append(self.l[0])
    self.length.data.append(self.l[1])
    self.length.data.append(self.l[2])
    self.length.data.append(self.l[3])
    self.length.data.append(self.l[4])
    self.length.data.append(self.l[5])
    self.length.data.append(self.l[6])
    self.length.data.append(self.l[7])
    self.length.data.append(self.l[8])
    self.base_pose.position.x = self.position_base[0]
    self.base_pose.position.y = self.position_base[1]
    self.base_pose.position.z = self.position_base[2]
    self.base_pose.orientation.w = 0.5*sqrt(1+cos(self.angle_base[0])*cos(self.angle_base[1])+sin(self.angle_base[0])*sin(self.angle_base[1])*sin(self.angle_base[2])+cos(self.angle_base[0])*cos(self.angle_base[2])+cos(self.angle_base[1])*cos(self.angle_base[2]))
    self.base_pose.orientation.x = (cos(self.angle_base[1])*sin(self.angle_base[2])-(sin(self.angle_base[0])*sin(self.angle_base[1])*cos(self.angle_base[2])-cos(self.angle_base[0])*sin(self.angle_base[2])))/(4*self.base_pose.orientation.w)
    self.base_pose.orientation.y = (cos(self.angle_base[0])*sin(self.angle_base[1])*cos(self.angle_base[2])+sin(self.angle_base[0])*sin(self.angle_base[2])-(-sin(self.angle_base[1])))/(4*self.base_pose.orientation.w)
    self.base_pose.orientation.z = (sin(self.angle_base[0])*cos(self.angle_base[1])-(cos(self.angle_base[0])*sin(self.angle_base[1])*sin(self.angle_base[2])-sin(self.angle_base[0])*cos(self.angle_base[2])))/(4*self.base_pose.orientation.w)
        
    for i in range(0,15):
      self.q_dot.data.append(self.y_dot[i])
    #self.diff_length.data.append(self.l_diff[0])
    #self.diff_length.data.append(self.l_diff[1])
    #self.diff_length.data.append(self.l_diff[2])
    #self.diff_length.data.append(self.l_diff[3])
    #self.diff_length.data.append(self.l_diff[4])
    #self.diff_length.data.append(self.l_diff[5])
    #self.diff_length.data.append(self.l_diff[6])
    #self.diff_length.data.append(self.l_diff[7])
    #self.diff_length.data.append(self.l_diff[8])

  def pseudo_inv(self,matrix):
    matrix_dummy = (matrix.T*matrix)
    if (matrix_dummy.det()!=0):
      out = ((matrix_dummy**-1)*(matrix.T))
    else:
      print "singularity..."
      self.error_flag = 1
      out = matrix.T #just to make out have certain values
    return out
    
  def init_jacobian_velocity(self,segment_num):
    homo_matrix = homogeneous_transform_sym(segment_num)
    pose_point = homo_to_pose_sym(homo_matrix)
    #print(pose_point)
    #if(segment_num == 1):
      #self.JacVelSymBot = Matrix([[simplify(diff(pose_point[0,0],k1)), simplify(diff(pose_point[0,0],p1)), diff(pose_point[0,0],s1), diff(pose_point[0,0],k2), diff(pose_point[0,0],p2),diff(pose_point[0,0],s2), diff(pose_point[0,0],k3), diff(pose_point[0,0],p3),diff(pose_point[0,0],s3)],
				  #[diff(pose_point[1,0],k1), diff(pose_point[1,0],p1), diff(pose_point[1,0],s1), diff(pose_point[1,0],k2), diff(pose_point[1,0],p2),diff(pose_point[1,0],s2), diff(pose_point[1,0],k3), diff(pose_point[1,0],p3),diff(pose_point[1,0],s3)],
				  #[diff(pose_point[2,0],k1), diff(pose_point[2,0],p1), diff(pose_point[2,0],s1), diff(pose_point[2,0],k2), diff(pose_point[2,0],p2),diff(pose_point[2,0],s2), diff(pose_point[2,0],k3), diff(pose_point[2,0],p3),diff(pose_point[2,0],s3)]])
    #elif(segment_num == 2):
      #self.JacVelSymMid = Matrix([[diff(pose_point[0,0],k1), diff(pose_point[0,0],p1), diff(pose_point[0,0],s1), diff(pose_point[0,0],k2), diff(pose_point[0,0],p2),diff(pose_point[0,0],s2), diff(pose_point[0,0],k3), diff(pose_point[0,0],p3),diff(pose_point[0,0],s3)],
				  #[diff(pose_point[1,0],k1), diff(pose_point[1,0],p1), diff(pose_point[1,0],s1), diff(pose_point[1,0],k2), diff(pose_point[1,0],p2),diff(pose_point[1,0],s2), diff(pose_point[1,0],k3), diff(pose_point[1,0],p3),diff(pose_point[1,0],s3)],
				  #[diff(pose_point[2,0],k1), diff(pose_point[2,0],p1), diff(pose_point[2,0],s1), diff(pose_point[2,0],k2), diff(pose_point[2,0],p2),diff(pose_point[2,0],s2), diff(pose_point[2,0],k3), diff(pose_point[2,0],p3),diff(pose_point[2,0],s3)]])
    #else:
      #self.JacVelSymUp = Matrix([[simplify(diff(pose_point[0,0],k1)), simplify(diff(pose_point[0,0],p1)), simplify(diff(pose_point[0,0],s1)), simplify(diff(pose_point[0,0],k2)), simplify(diff(pose_point[0,0],p2)),simplify(diff(pose_point[0,0],s2)), simplify(diff(pose_point[0,0],k3)), simplify(diff(pose_point[0,0],p3)),simplify(diff(pose_point[0,0],s3))],
				  #[simplify(diff(pose_point[1,0],k1)), simplify(diff(pose_point[1,0],p1)), simplify(diff(pose_point[1,0],s1)), simplify(diff(pose_point[1,0],k2)), simplify(diff(pose_point[1,0],p2)),simplify(diff(pose_point[1,0],s2)), simplify(diff(pose_point[1,0],k3)), simplify(diff(pose_point[1,0],p3)),simplify(diff(pose_point[1,0],s3))],
				  #[simplify(diff(pose_point[2,0],k1)), simplify(diff(pose_point[2,0],p1)), simplify(diff(pose_point[2,0],s1)), simplify(diff(pose_point[2,0],k2)), simplify(diff(pose_point[2,0],p2)),simplify(diff(pose_point[2,0],s2)), simplify(diff(pose_point[2,0],k3)), simplify(diff(pose_point[2,0],p3)),simplify(diff(pose_point[2,0],s3))]])
    self.JacVelSymUp = Matrix([[diff(pose_point[0,0],k1), diff(pose_point[0,0],p1), diff(pose_point[0,0],s1), diff(pose_point[0,0],k2), diff(pose_point[0,0],p2),diff(pose_point[0,0],s2), diff(pose_point[0,0],k3), diff(pose_point[0,0],p3),diff(pose_point[0,0],s3)],
				  [diff(pose_point[1,0],k1), diff(pose_point[1,0],p1), diff(pose_point[1,0],s1), diff(pose_point[1,0],k2), diff(pose_point[1,0],p2),diff(pose_point[1,0],s2), diff(pose_point[1,0],k3), diff(pose_point[1,0],p3),diff(pose_point[1,0],s3)],
				  [diff(pose_point[2,0],k1), diff(pose_point[2,0],p1), diff(pose_point[2,0],s1), diff(pose_point[2,0],k2), diff(pose_point[2,0],p2),diff(pose_point[2,0],s2), diff(pose_point[2,0],k3), diff(pose_point[2,0],p3),diff(pose_point[2,0],s3)]])
    #print(self.JacVelSymUp[2,8])
    #print(pose_point[0,0])
    self.JacBase = Matrix([[diff(pose_point[0,0],xb), diff(pose_point[0,0],yb), diff(pose_point[0,0],zb), diff(pose_point[0,0],alphab), diff(pose_point[0,0],betab), diff(pose_point[0,0],gammab)],
				  [diff(pose_point[1,0],xb), diff(pose_point[1,0],yb), diff(pose_point[1,0],zb), diff(pose_point[1,0],alphab), diff(pose_point[1,0],betab), diff(pose_point[1,0],gammab)],
				  [diff(pose_point[2,0],xb), diff(pose_point[2,0],yb), diff(pose_point[2,0],zb), diff(pose_point[2,0],alphab), diff(pose_point[2,0],betab), diff(pose_point[2,0],gammab)]])
    #print(self.JacBase[2,5])
    self.JacVelFunc = lambdify((alphab, betab, gammab, k1, k2, k3, p1, p2, p3, s1, s2, s3), self.JacVelSymUp, "sympy")
    #print(self.JacBase)
    self.JacBaseFunc = lambdify((alphab, betab, gammab, k1, k2, k3, p1, p2, p3, s1, s2, s3), self.JacBase, "sympy")
    #return JacVelSym
  def init_jacobian_kps_l(self):
    self.Jacobian_kps_l = Matrix([  [diff(self.k1_ex, l11), diff(self.k1_ex, l12), diff(self.k1_ex, l13), diff(self.k1_ex, l21), diff(self.k1_ex, l22), diff(self.k1_ex, l23), diff(self.k1_ex, l31), diff(self.k1_ex, l32), diff(self.k1_ex, l33)],
				    [diff(self.p1_ex, l11), diff(self.p1_ex, l12), diff(self.p1_ex, l13), diff(self.p1_ex, l21), diff(self.p1_ex, l22), diff(self.p1_ex, l23), diff(self.p1_ex, l31), diff(self.p1_ex, l32), diff(self.p1_ex, l33)],
				    [diff(self.s1_ex, l11), diff(self.s1_ex, l12), diff(self.s1_ex, l13), diff(self.s1_ex, l21), diff(self.s1_ex, l22), diff(self.s1_ex, l23), diff(self.s1_ex, l31), diff(self.s1_ex, l32), diff(self.s1_ex, l33)],
				    [diff(self.k2_ex, l11), diff(self.k2_ex, l12), diff(self.k2_ex, l13), diff(self.k2_ex, l21), diff(self.k2_ex, l22), diff(self.k2_ex, l23), diff(self.k2_ex, l31), diff(self.k2_ex, l32), diff(self.k2_ex, l33)],
				    [diff(self.p2_ex, l11), diff(self.p2_ex, l12), diff(self.p2_ex, l13), diff(self.p2_ex, l21), diff(self.p2_ex, l22), diff(self.p2_ex, l23), diff(self.p2_ex, l31), diff(self.p2_ex, l32), diff(self.p2_ex, l33)],
				    [diff(self.s2_ex, l11), diff(self.s2_ex, l12), diff(self.s2_ex, l13), diff(self.s2_ex, l21), diff(self.s2_ex, l22), diff(self.s2_ex, l23), diff(self.s2_ex, l31), diff(self.s2_ex, l32), diff(self.s2_ex, l33)],
				    [diff(self.k3_ex, l11), diff(self.k3_ex, l12), diff(self.k3_ex, l13), diff(self.k3_ex, l21), diff(self.k3_ex, l22), diff(self.k3_ex, l23), diff(self.k3_ex, l31), diff(self.k3_ex, l32), diff(self.k3_ex, l33)],
				    [diff(self.p3_ex, l11), diff(self.p3_ex, l12), diff(self.p3_ex, l13), diff(self.p3_ex, l21), diff(self.p3_ex, l22), diff(self.p3_ex, l23), diff(self.p3_ex, l31), diff(self.p3_ex, l32), diff(self.p3_ex, l33)],
				    [diff(self.s3_ex, l11), diff(self.s3_ex, l12), diff(self.s3_ex, l13), diff(self.s3_ex, l21), diff(self.s3_ex, l22), diff(self.s3_ex, l23), diff(self.s3_ex, l31), diff(self.s3_ex, l32), diff(self.s3_ex, l33)]])
    print(self.Jacobian_kps_l[8,6])
    print(self.Jacobian_kps_l[8,7])
    print(self.Jacobian_kps_l[8,8])
    self.JacobianKPSlFunc = lambdify((l11, l12, l13, l21, l22, l23, l31, l32, l33), self.Jacobian_kps_l, "sympy")

  
  def init_jacobian_velocity_fast(self,segment_num):
    self.JacVelSymUp = Matrix([[(k1**2*k2*s1*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1**2*k3*s1*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k1*k2*k3*s1*sin(k1*s1) + k2*k3*(cos(k1*s1) - 1))*cos(p1)/(k1**2*k2*k3), (k1*k2*(-(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k3*s3) - 1)*sin(p3) + (cos(k3*s3) - 1)*(-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*cos(p3) - (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*sin(k3*s3)) + k1*k3*((cos(k2*s2) - 1)*sin(p1)*cos(p2)*cos(k1*s1) + (cos(k2*s2) - 1)*sin(p2)*cos(p1) - sin(p1)*sin(k1*s1)*sin(k2*s2)) + k2*k3*(cos(k1*s1) - 1)*sin(p1))/(k1*k2*k3), (k1*k2*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1*k3*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k2*k3*sin(k1*s1))*cos(p1)/(k2*k3), (k2**2*s2*((cos(k3*s3) - 1)*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(p3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3)) + k2*k3*s2*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1)) + k3*(-(cos(k2*s2) - 1)*sin(p1)*sin(p2) + (cos(k2*s2) - 1)*cos(p1)*cos(p2)*cos(k1*s1) - sin(k1*s1)*sin(k2*s2)*cos(p1)))/(k2**2*k3), (k2*(-(sin(p1)*sin(p2) - cos(p1)*cos(p2)*cos(k1*s1))*(cos(k3*s3) - 1)*sin(p3) + (sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3)*cos(k2*s2) - (sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*sin(k2*s2)*sin(k3*s3)) + k3*(sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k2*s2) - 1))/(k2*k3), (k2*(cos(k3*s3) - 1)*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(p3) - k2*(sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + k3*(-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1)))/k3, (k3*s3*(-(sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(k3*s3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3)*cos(p3)) - (sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k3*s3) - 1)*sin(p3) - (cos(k3*s3) - 1)*(sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*cos(p3) - (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*sin(k3*s3))/k3**2, ((sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*cos(p3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(p3))*(cos(k3*s3) - 1)/k3, -(sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*cos(k3*s3) - (sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*sin(k3*s3)*cos(p3)], [(k1**2*k2*s1*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1**2*k3*s1*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k1*k2*k3*s1*sin(k1*s1) + k2*k3*(cos(k1*s1) - 1))*sin(p1)/(k1**2*k2*k3), (k1*k2*((sin(p1)*cos(p2) + sin(p2)*cos(p1)*cos(k1*s1))*(cos(k3*s3) - 1)*sin(p3) + (cos(k3*s3) - 1)*(sin(p1)*sin(p2)*cos(k2*s2) + sin(k1*s1)*sin(k2*s2)*cos(p1) - cos(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2))*cos(p3) + (-sin(p1)*sin(p2)*sin(k2*s2) + sin(k1*s1)*cos(p1)*cos(k2*s2) + sin(k2*s2)*cos(p1)*cos(p2)*cos(k1*s1))*sin(k3*s3)) + k1*k3*((cos(k2*s2) - 1)*sin(p1)*sin(p2) - (cos(k2*s2) - 1)*cos(p1)*cos(p2)*cos(k1*s1) + sin(k1*s1)*sin(k2*s2)*cos(p1)) - k2*k3*(cos(k1*s1) - 1)*cos(p1))/(k1*k2*k3), (k1*k2*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) - (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1)) + k1*k3*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)) + k2*k3*sin(k1*s1))*sin(p1)/(k2*k3), (k2**2*s2*((cos(k3*s3) - 1)*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(p3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3)) + k2*k3*s2*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1)) + k3*((cos(k2*s2) - 1)*sin(p1)*cos(p2)*cos(k1*s1) + (cos(k2*s2) - 1)*sin(p2)*cos(p1) - sin(p1)*sin(k1*s1)*sin(k2*s2)))/(k2**2*k3), (k2*((sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k3*s3) - 1)*cos(p3)*cos(k2*s2) - (sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*sin(k2*s2)*sin(k3*s3) + (sin(p1)*cos(p2)*cos(k1*s1) + sin(p2)*cos(p1))*(cos(k3*s3) - 1)*sin(p3)) + k3*(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k2*s2) - 1))/(k2*k3), (k2*(cos(k3*s3) - 1)*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(p3) + k2*(-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3) + k3*(sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1)))/k3, (k3*s3*(-(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3)*cos(p3) + (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(k3*s3)) - (sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*(cos(k3*s3) - 1)*sin(p3) + (cos(k3*s3) - 1)*(-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*cos(p3) - (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*sin(k3*s3))/k3**2, ((sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*cos(p3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(p3))*(cos(k3*s3) - 1)/k3, -(sin(p1)*sin(p2)*cos(k1*s1) - cos(p1)*cos(p2))*sin(p3)*sin(k3*s3) + (-sin(p1)*sin(k1*s1)*sin(k2*s2) + sin(p1)*cos(p2)*cos(k1*s1)*cos(k2*s2) + sin(p2)*cos(p1)*cos(k2*s2))*sin(k3*s3)*cos(p3) + (sin(p1)*sin(k1*s1)*cos(k2*s2) + sin(p1)*sin(k2*s2)*cos(p2)*cos(k1*s1) + sin(p2)*sin(k2*s2)*cos(p1))*cos(k3*s3)], [(-k1**2*k2*s1*((sin(k1*s1)*sin(k2*s2) - cos(p2)*cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) + (sin(k1*s1)*cos(k2*s2) + sin(k2*s2)*cos(p2)*cos(k1*s1))*sin(k3*s3) + (cos(k3*s3) - 1)*sin(p2)*sin(p3)*cos(k1*s1)) + k1**2*k3*s1*((cos(k2*s2) - 1)*cos(p2)*cos(k1*s1) - sin(k1*s1)*sin(k2*s2)) + k1*k2*k3*s1*cos(k1*s1) - k2*k3*sin(k1*s1))/(k1**2*k2*k3), 0, (-k1*k2*((sin(k1*s1)*sin(k2*s2) - cos(p2)*cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) + (sin(k1*s1)*cos(k2*s2) + sin(k2*s2)*cos(p2)*cos(k1*s1))*sin(k3*s3) + (cos(k3*s3) - 1)*sin(p2)*sin(p3)*cos(k1*s1)) + k1*k3*((cos(k2*s2) - 1)*cos(p2)*cos(k1*s1) - sin(k1*s1)*sin(k2*s2)) + k2*k3*cos(k1*s1))/(k2*k3), (-k2**2*s2*((sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) + (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3)) + k2*k3*s2*(-sin(k1*s1)*sin(k2*s2)*cos(p2) + cos(k1*s1)*cos(k2*s2)) - k3*((cos(k2*s2) - 1)*sin(k1*s1)*cos(p2) + sin(k2*s2)*cos(k1*s1)))/(k2**2*k3), (k2*(-(cos(k3*s3) - 1)*sin(p2)*cos(p3)*cos(k2*s2) - (cos(k3*s3) - 1)*sin(p3)*cos(p2) + sin(p2)*sin(k2*s2)*sin(k3*s3)) - k3*(cos(k2*s2) - 1)*sin(p2))*sin(k1*s1)/(k2*k3), (-k2*(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*(cos(k3*s3) - 1)*cos(p3) - k2*(sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3) + k3*(-sin(k1*s1)*sin(k2*s2)*cos(p2) + cos(k1*s1)*cos(k2*s2)))/k3, (k3*s3*(-(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*cos(k3*s3) - (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3)*cos(p3) + sin(p2)*sin(p3)*sin(k1*s1)*sin(k3*s3)) + (sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*sin(k3*s3) - (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*(cos(k3*s3) - 1)*cos(p3) + (cos(k3*s3) - 1)*sin(p2)*sin(p3)*sin(k1*s1))/k3**2, -((sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(p3) + sin(p2)*sin(k1*s1)*cos(p3))*(cos(k3*s3) - 1)/k3, -(sin(k1*s1)*sin(k2*s2)*cos(p2) - cos(k1*s1)*cos(k2*s2))*cos(k3*s3) - (sin(k1*s1)*cos(p2)*cos(k2*s2) + sin(k2*s2)*cos(k1*s1))*sin(k3*s3)*cos(p3) + sin(p2)*sin(p3)*sin(k1*s1)*sin(k3*s3)]])
    self.JacVelFunc = lambdify((k1, k2, k3, p1, p2, p3, s1, s2, s3), self.JacVelSymUp, "sympy")
    #print(self.JacVelFunc)

  def jacobian_velocity(self, segment_num):
    if(segment_num == 1):
      self.JacVelSym = self.JacVelSymBot
    elif(segment_num == 2):
      self.JacVelSym = self.JacVelSymMid
    elif(segment_num == 3):
      self.JacVelSym = self.JacVelSymUp
    #print(self.JacVelSym)
    #self.Jv = self.JacVelSym.subs([(k1, self.k[0]),(k2, self.k[1]),(k3, self.k[2]),(p1, self.p[0]),(p2, self.p[1]),(p3, self.p[2]),(s1, self.s[0]),(s2, self.s[1]),(s3, self.s[2])])

    #aka = self.k[0]
    #aka1 = self.k[1]
    #aka2 = self.k[2]
    #aka3 = self.p[0]
    #aka4 = self.p[1]
    #aka5 = self.p[2]
    #aka6 = self.s[0]
    #aka7 = self.s[1]
    #aka8 = self.s[2]
    #print(self.p)
    #print(self.s)
    #self.Jv = self.JacVelFunc(aka, aka1, aka2, aka3, aka4, aka5, aka6, aka7, aka8)
    
    self.Jv = self.JacVelFunc(self.angle_base[0], self.angle_base[1], self.angle_base[2], self.k[0], self.k[1], self.k[2], self.p[0], self.p[1], self.p[2], self.s[0], self.s[1], self.s[2])
    self.Jvb = self.JacBaseFunc(self.angle_base[0], self.angle_base[1], self.angle_base[2], self.k[0], self.k[1], self.k[2], self.p[0], self.p[1], self.p[2], self.s[0], self.s[1], self.s[2])
    #print(self.Jv)
    #return JacVel
    
  def jacobian_kps_l(self):
    self.JacobianKPSlNow = self.JacobianKPSlFunc(self.l[0],self.l[1],self.l[2],self.l[3],self.l[4],self.l[5],self.l[6],self.l[7],self.l[8])
    #print(self.JacobianKPSlNow)

  def configuration_field(self,length_of_segment):
    self.config_field.data = []
    for i in range(0,9):
      vector_min = length_of_segment[i] - self.smin
      vector_max = length_of_segment[i] - self.smax
      rhomin = sqrt(vector_min*vector_min)
      rhomax = sqrt(vector_max*vector_max)
      #print(rhomax)
      #print(rhomin)
      if(rhomin<self.slim):
	self.vel[i] = self.K_config*vector_min*(1/rhomin - 1/self.slim)/pow(rhomin,3)
      elif(rhomax<self.slim):
	self.vel[i] = self.K_config*vector_max*(1/rhomax - 1/self.slim)/pow(rhomax,3)
      else:
	self.vel[i] = 0
      #print(self.vel[i])
      self.config_field.data.append(self.vel[i])
      
  def constraint_potential(self, length_of_segment):
    self.config_field.data = []
    a_i = 0.5*(self.Lo + self.smin + self.Lo + self.smax)
    for i in range(0,9):
      #vector_min = length_of_segment[i] - self.smin
      #vector_max = length_of_segment[i] - self.smax
      #rhomin = sqrt(vector_min*vector_min)
      #rhomax = sqrt(vector_max*vector_max)
      ##print(rhomax)
      ##print(rhomin)
      #if(rhomin<self.slim):
	#self.vel[i] = self.K_config*vector_min*(1/rhomin - 1/self.slim)/pow(rhomin,3)
      #elif(rhomax<self.slim):
	#self.vel[i] = self.K_config*vector_max*(1/rhomax - 1/self.slim)/pow(rhomax,3)
      #else:
	#self.vel[i] = 0
      #print(self.vel[i])
      
      #weight = 5.0*(1.0-math.exp(-1.0*norm(self.velocity)))
      weight = 5.0*(1.0-math.exp(-0.5*norm(self.velocity)))
      #print(weight)
      self.vel[i] = -0.05*weight*1.0/3.0*(self.Lo+length_of_segment[i]-a_i)/pow((a_i-self.smax),2)
      
      #if((((self.smax-length_of_segment[i])< self.slim) or ((length_of_segment[i]-self.smin)< self.slim))):
	#self.vel[i] = -0.01*1.0/3.0*(self.Lo+length_of_segment[i]-a_i)/pow((a_i-self.smax),2)
      #else:
	#self.vel[i] = 0.0
      #print(self.vel[i])
      
      self.config_field.data.append(self.vel[i])
    
if __name__ == '__main__':
    try:
        inv_jacobi()
    except rospy.ROSInterruptException: pass
