#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('soft_fuzzy')
import math
import rospy
from geometry_msgs.msg import Pose
from std_msgs.msg import Float64MultiArray
from sympy import *

class fuzzy_control(object):
  def __init__(self):
    rospy.init_node('fuzzy_control')
    self.F = [eye(3), eye(3), eye(3), eye(3), eye(3), eye(3)]
    self.G = [eye(3), eye(3), eye(3), eye(3), eye(3), eye(3)]
    
    self.e_vector = zeros(3,1)
    self.path_vector = zeros(3,1)
    self.tip = zeros(3,1)
    self.vel_send = Float64MultiArray()
    self.w1 = [0.0, 0.0]
    self.w2 = [0.0, 0.0, 0.0]
    self.w3 = [0.0]
    self.SW = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
    #print(self.F[1].tolist()[0][0])
    #print(self.tip.tolist()[0][0])
    vel_pub = rospy.Publisher('motor_speed', Float64MultiArray)
    error_sub = rospy.Subscriber('error', Pose, self.get_error)
    path_sub = rospy.Subscriber('path', Pose, self.get_path)
    tip_sub = rospy.Subscriber('tip_pose', Pose, self.get_tip)
    #tip_to_goal = tf.TransformListener()
    
    r = rospy.Rate(40.0)
    
    self.initialize_matrix()
    
    while not rospy.is_shutdown():
        self.vel_send.data = []
        self.membership_function()
        #self.v = A*self.e_vector+B*self.path_vector
        for i in range(0,3):
	  self.vel_send.data.append(self.v[i])
	#print(self.vel_send)
	vel_pub.publish(self.vel_send)
        #r.sleep()
        
        #rospy.spin()

  def get_error(self,msg):
    self.e_vector = Matrix([[msg.position.x],[msg.position.y],[msg.position.z]])
    
  def get_path(self,msg):
    self.path_vector = Matrix([[msg.position.x],[msg.position.y],[msg.position.z]])
    
  def get_tip(self,msg):
    self.tip = Matrix([[msg.position.x],[msg.position.y],[msg.position.z]])
  
  def initialize_matrix(self):
    self.F[0] = Matrix([[-2.7206e-01, 2.1012e-02, -7.6949e-01], [-1.6041e-01, -5.1160e-02, -8.6324e-01], [-1.5828e-01, 9.4601e-02, -9.3110e-01]])
    self.F[1] = Matrix([[-1.1953e-01, 1.6692e-01, -1.3760e-01], [-4.5952e-02, 1.1546e-01, -2.1266e-01], [-4.3577e-02, 2.0607e-01, -2.3030e-01]])
    self.F[2] = Matrix([[-8.9587e-01, -4.1654e-01, -1.5727e+00], [-3.2598e-01, -7.8800e-01, -1.7191e+00], [-3.2850e-01, -1.1540e-01, -1.8467e+00]])
    self.F[3] = Matrix([[-1.3254e-01, -3.9851e-04, -1.0950e-01], [-1.0896e-01, -4.5417e-02, -1.1184e-01], [-9.8319e-02, 4.2714e-03, -1.5240e-01]])
    self.F[4] = Matrix([[-5.3656e-02, 5.5767e-02, -1.2270e-01], [3.1950e-02, -2.5215e-02, -1.7084e-01], [5.7224e-02, 9.2365e-02, -1.6886e-01]])
    self.F[5] = Matrix([[-1.8371e-01, 6.7326e-02, -8.1116e-01], [-1.1379e-01, -1.0915e-02, -8.9478e-01], [-9.7454e-02, 1.0932e-01, -8.7763e-01]])
    
    self.G[0] = Matrix([[-8.0989e-07, 3.9152e-06, 4.7043e-06], [-1.0545e-06, 4.2497e-06, 5.3309e-06], [-9.4849e-07, 4.3765e-06, 5.3049e-06]])
    self.G[1] = Matrix([[1.2539e-06, -1.0286e-06, -9.9682e-06], [1.2648e-06, -9.5685e-07, -1.0360e-05], [1.2766e-06, -1.0633e-06, -1.0450e-05]])
    self.G[2] = Matrix([[1.7291e-06, -3.6196e-06, -5.9067e-06], [1.7942e-06, -3.8542e-06, -6.1566e-06], [1.8305e-06, -3.8248e-06, -6.2378e-06]])
    self.G[3] = Matrix([[-4.3764e-06, -3.2132e-06, -6.1168e-06], [-3.9718e-06, -3.7454e-06, -5.1166e-06], [-4.6104e-06, -3.3195e-06, -5.4842e-06]])
    self.G[4] = Matrix([[1.6175e-06, -1.3109e-06, 3.7304e-07], [2.0731e-06, -1.4002e-06, 4.8839e-07], [1.1053e-06, -1.3574e-06, 3.9661e-07]])
    self.G[5] = Matrix([[-2.4043e-06, 3.8768e-06, 5.0956e-06], [-2.2209e-06, 3.6820e-06, 5.0905e-06], [-2.4214e-06, 3.7718e-06, 4.9021e-06]])

    #self.F[0] = Matrix([[-0.68292, 0.15441, -1.021], [-0.30821, -0.061657, -1.2034], [-0.30511, 0.40052, -1.2998]])
    #self.F[1] = Matrix([[-0.4853, 0.42345, -0.066545], [-0.16602, 0.2206, -0.20654], [-0.16233, 0.59961, -0.30033]])
    #self.F[2] = Matrix([[-2.664, -1.3223, -6.1385], [-1.082, -2.3611, -6.594], [-1.0927, -0.4918, -6.9924]])
    #self.F[3] = Matrix([[-0.72488, -0.084633, 0.015428], [-0.48926, -0.3065, 0.0027866], [-0.46061, 0.040922, -0.1042]])
    #self.F[4] = Matrix([[-0.2572, 0.19092, -0.078121], [0.011435, -0.046873, -0.21329], [0.066976, 0.31599, -0.23739]])
    #self.F[5] = Matrix([[-0.52974, 0.26869, -1.007], [-0.24016, 0.028202, -1.1884], [-0.23404, 0.41514, -1.2771]])
    
    #self.G[0] = Matrix([[0.000000000000025159, 0.00000000000039922, -0.00000000000003064], [0.000000000000025117, 0.00000000000048862, -0.000000000000060626], [0.000000000000027274, 0.0000000000004479, -0.000000000000033045]])
    #self.G[1] = Matrix([[-0.00000000000013757, 0.000000000000023014, 0.00000000000014712], [-0.00000000000013051, 0.000000000000056426, 0.00000000000016857], [-0.00000000000014575, 7.0612E-015, 0.00000000000015543]])
    #self.G[2] = Matrix([[0.000000000000012018, -0.00000000000042931, 0.00000000000011698], [6.6795E-015, -0.00000000000038989, 0.000000000000094701], [0.000000000000014205, -0.00000000000046689, 0.00000000000013036]])
    #self.G[3] = Matrix([[0.000000000000072111, -0.00000000000022383, 0.0000000000001707], [0.000000000000043894, -0.00000000000025739, 0.0000000000000399], [0.00000000000006139, -0.00000000000021085, 0.000000000000066933]])
    #self.G[4] = Matrix([[-0.000000000000034618, 0.00000000000026322, 0.000000000000096315], [-0.000000000000012337, 0.000000000000088343, 0.00000000000018802], [-0.000000000000020923, 0.00000000000026597, 0.000000000000089991]])
    #self.G[5] = Matrix([[0.000000000000029613, 0.00000000000047558, -0.000000000000056699], [0.000000000000023219, 0.00000000000050242, -0.000000000000081282], [0.000000000000030678, 0.00000000000045453, -0.000000000000051586]])
    
    
    #self.F[0] = Matrix([[-4.4750e+00, -1.1256e+00, -8.6796e-01], [-2.4067e+00, -1.0222e+00, -1.0503e+00], [-1.3398e+00, 5.4994e-01, -1.1465e+00]])
    #self.F[1] = Matrix([[-2.5326e+00, 1.6683e-01, -8.9918e-02], [-6.8881e-01, 2.0827e-01, -2.2981e-01], [2.5401e-01, 1.5820e+00, -3.2729e-01]])
    #self.F[2] = Matrix([[-2.1789e+01, -1.0633e+01, -6.7795e+00], [-1.2969e+01, -1.0550e+01, -7.2718e+00], [-8.6595e+00, -4.0086e+00, -7.7163e+00]])
    #self.F[3] = Matrix([[-4.6592e+00, -1.4727e+00, 6.6116e-02], [-3.4840e+00, -1.6401e+00, -4.7885e-03], [-2.4967e+00, -3.9332e-01, -1.0194e-01]])
    #self.F[4] = Matrix([[-1.8139e+00, -1.4084e-01, -7.8208e-02], [-4.9522e-01, -3.2354e-01, -2.0853e-01], [6.3710e-01, 1.0036e+00, -2.6234e-01]])
    #self.F[5] = Matrix([[-2.8026e+00, -2.8252e-01, -9.4846e-01], [-1.1518e+00, -4.0678e-01, -1.1214e+00], [-1.4985e-01, 1.0329e+00, -1.2256e+00]])
    
    #self.G[0] = Matrix([[3.3998e-02, 1.0102e-02, -4.6707e-09], [3.5803e-02, 9.6638e-03, -5.1621e-09], [3.7794e-02, 1.1156e-02, -5.3606e-09]])
    #self.G[1] = Matrix([[1.4438e-03, -3.1870e-03, -2.4116e-09], [2.5347e-03, -3.3419e-03, -2.7044e-09], [2.0744e-03, -2.9722e-03, -2.5546e-09]])
    #self.G[2] = Matrix([[-1.3740e-02, -7.6282e-04, 6.9686e-09], [-1.5160e-02, -6.6197e-04, 7.2357e-09], [-1.6242e-02, -1.7487e-03, 7.3610e-09]])
    #self.G[3] = Matrix([[-5.5859e-03, -7.1389e-03, 1.4342e-09], [2.9052e-04, -1.0987e-02, 1.2685e-09], [-8.7507e-04, -6.2305e-03, 1.0200e-09]])
    #self.G[4] = Matrix([[-1.6054e-03, -6.9280e-03, 2.3322e-09], [2.7601e-03, -1.0067e-02, 2.6088e-09], [2.9723e-03, -4.0227e-03, 2.4108e-09]])
    #self.G[5] = Matrix([[5.1508e-04, -6.4608e-03, -6.2608e-09], [1.4233e-03, -7.2234e-03, -6.0044e-09], [-4.2606e-04, -6.8414e-03, -6.0802e-09]])
    
    #self.F[0] = Matrix([[-4.4750, -1.1256, -0.86796], [-2.4067, -1.0222, -1.0503], [-1.3398, 0.54994, -1.1465]])
    #self.F[1] = Matrix([[-2.5326, 0.16683, -0.089918], [-0.68881, 0.20827, -0.22981], [0.25401, 1.5820, -0.32729]])
    #self.F[2] = Matrix([[-21.789, -10.633, -6.7795], [-12.969, -10.550, -7.2718], [-8.6595, -4.0086, -7.7163]])
    #self.F[3] = Matrix([[-4.6592, -1.4727, 0.066116], [-3.4840, -1.6401, -0.0047885], [-2.4967, -0.39332, -0.10194]])
    #self.F[4] = Matrix([[-1.8139, -0.14084, -0.078208], [-0.49522, -0.32354, -0.20853], [0.63710, 1.0036, -0.26234]])
    #self.F[5] = Matrix([[-2.8026, -0.28252, -0.94846], [-1.1518, -0.40678, -1.1214], [-0.14985, 1.0329, -1.2256]])
    
    #self.G[0] = Matrix([[0.033998, 0.010102, -0.0000000046707], [0.035803, 0.0096638, -0.0000000051621], [0.037794, 0.011156, -0.0000000053606]])
    #self.G[1] = Matrix([[0.0014438, -0.0031870, -0.0000000024116], [0.0025347, -0.0033419, -0.0000000027044], [0.0020744, -0.0029722, -0.0000000025546]])
    #self.G[2] = Matrix([[-0.013740, -0.00076282, 0.0000000069686], [-0.015160, -0.00066197, 0.0000000072357], [-0.016242, -0.0017487, 0.0000000073610]])
    #self.G[3] = Matrix([[-0.0055859, -0.0071389, 0.0000000014342], [0.00029052, -0.010987, 0.0000000012685], [-0.00087507, -0.0062305, 0.0000000010200]])
    #self.G[4] = Matrix([[-0.0016054, -0.0069280, 0.0000000023322], [0.0027601, -0.010067, 0.0000000026088], [0.0029723, -0.0040227, 0.0000000024108]])
    #self.G[5] = Matrix([[0.00051508, -0.0064608, -0.0000000062608], [0.0014233, -0.0072234, -0.0000000060044], [-0.00042606, -0.0068414, -0.0000000060802]])
    #self.F[0] = Matrix([[-1.0471e+00, 1.0254e-02, -8.5140e-01], [-5.1754e-01, -1.7227e-01, -1.0314e+00], [-4.1197e-01, 3.8401e-01, -1.1266e+00]])
    #self.F[1] = Matrix([[-6.8355e-01, 3.5105e-01, -9.0919e-02], [-2.0495e-01, 1.6607e-01, -2.2979e-01], [-1.0976e-01, 6.5193e-01, -3.2639e-01]])
    #self.F[2] = Matrix([[-4.5358e+00, -2.1680e+00, -6.6988e+00], [-2.2319e+00, -3.1020e+00, -7.1855e+00], [-1.8263e+00, -7.6850e-01, -7.6252e+00]])
    #self.F[3] = Matrix([[-1.0638e+00, -1.2502e-01, 6.1949e-02], [-7.3664e-01, -3.2701e-01, -6.5886e-03], [-6.1550e-01, 1.0045e-01, -1.0364e-01]])
    #self.F[4] = Matrix([[-4.6002e-01, 1.3756e-01, -8.0265e-02], [-9.2798e-02, -8.6528e-02, -2.0925e-01], [5.8003e-02, 3.6000e-01, -2.6205e-01]])
    #self.F[5] = Matrix([[-6.9966e-01, 1.8935e-01, -9.3356e-01], [-2.5349e-01, -4.5507e-02, -1.1047e+00], [-1.4986e-01, 4.6212e-01, -1.2074e+00]])
    
    #self.G[0] = Matrix([[3.1024e-04, -2.7069e-04, 2.8114e-09], [3.6455e-04, -3.9076e-04, 3.0444e-09], [4.0529e-04, -2.6951e-04, 3.1951e-09]])
    #self.G[1] = Matrix([[1.6829e-04, -3.1366e-04, -2.3034e-10], [2.8532e-04, -3.7649e-04, -8.2829e-11], [2.5388e-04, -2.8040e-04, -3.1560e-10]])
    #self.G[2] = Matrix([[-6.5918e-04, -6.5134e-05, -3.5416e-09], [-6.2281e-04, -1.2007e-04, -3.6828e-09], [-6.7045e-04, -5.3872e-05, -3.7443e-09]])
    #self.G[3] = Matrix([[-2.5472e-04, -5.8033e-04, 3.6964e-11], [3.2403e-04, -9.5673e-04, 1.5020e-10], [2.0168e-04, -4.7670e-04, 2.8926e-10]])
    #self.G[4] = Matrix([[-3.9491e-05, -6.4307e-04, 8.9125e-10], [4.1934e-04, -9.5663e-04, 6.0908e-10], [3.8512e-04, -3.9179e-04, 2.3905e-10]])
    #self.G[5] = Matrix([[5.1061e-04, -2.6354e-04, 3.1334e-09], [5.9421e-04, -3.6952e-04, 3.1283e-09], [5.4639e-04, -2.4472e-04, 3.0028e-09]])

    #self.F[0] = Matrix([[-1.0285e+02, -3.8203e+01, -1.2337e+00], [-5.9279e+01, -2.6392e+01, -1.4254e+00], [-3.0656e+01, -2.7095e+00, -1.5371e+00]])
    #self.F[1] = Matrix([[-3.8582e+01, -3.9180e+00 , -6.3049e-02], [-4.6524e+00, 4.8050e+00, -2.0928e-01], [1.7374e+01, 2.2943e+01, -2.8703e-01]])
    #self.F[2] = Matrix([[-4.6606e+02, -2.2106e+02, -5.5008e+00], [-2.9322e+02, -1.7777e+02, -5.9228e+00], [-1.8370e+02, -8.6039e+01, -6.2937e+00]])
    #self.F[3] = Matrix([[-1.2079e+02, -5.2071e+01, 9.0889e-03], [-1.0049e+02, -4.9595e+01, 2.9002e-02], [-7.6624e+01, -3.1369e+01, -9.1211e-02]])
    #self.F[4] = Matrix([[-2.5198e+01, -5.3688e+00, -6.5545e-02], [2.4615e+00, -5.3480e-01, -1.9762e-01], [3.3816e+01, 2.1814e+01, -2.1224e-01]])
    #self.F[5] = Matrix([[-7.3864e+01, -2.3761e+01, -1.2522e+00], [-4.6063e+01, -1.8832e+01, -1.4425e+00], [-2.0796e+01, 1.4113e+00, -1.5042e+00]])
    
    #self.G[0] = Matrix([[2.5280e-03, -5.1375e-03, 1.1083e-13], [3.8405e-03, -1.0211e-02, 1.1956e-13], [8.6638e-04, -6.6031e-03, 1.2394e-13]])
    #self.G[1] = Matrix([[-2.3645e-04, -7.4860e-03, -9.3189e-14], [2.0127e-03, -1.2514e-02, -1.0631e-13], [-6.6631e-04, -7.7557e-03, -8.4980e-14]])
    #self.G[2] = Matrix([[6.1980e-03, 1.7455e-02, -1.2028e-13], [1.2933e-02, 7.3986e-03, -1.6062e-13], [6.0332e-03, 1.9057e-02, -1.2621e-13]])
    #self.G[3] = Matrix([[7.1110e-03, -1.1674e-03, 8.0277e-14], [1.0955e-02, -6.1028e-03, 9.2144e-14], [7.5838e-03, -1.8711e-03, 6.4439e-14]])
    #self.G[4] = Matrix([[2.4662e-03, -5.0853e-03, 1.9890e-13], [4.8620e-03, -9.8065e-03, 2.3756e-13], [2.6790e-03, -4.8205e-03, 2.4605e-13]])
    #self.G[5] = Matrix([[-7.1710e-03, -1.0092e-02, 1.3644e-13], [-5.4831e-03, -1.3748e-02, 8.6329e-14], [-6.7272e-03, -9.1956e-03, 1.3229e-13]])

    print(self.F[5][0,0])
    print(self.G[5][0,0])

  def membership_function(self):
    self.w1[0] = 1-1/(1 + exp(-(self.tip.tolist()[0][0] - 0.019)/0.0019))
    self.w1[1] = 1 - self.w1[0]

    self.w2[0] = 1-1/(1 + exp(-(self.tip.tolist()[1][0]+0.015)/0.00315));
    self.w2[2] = 1/(1 + exp(-(self.tip.tolist()[1][0]-0.015)/0.00315));
    self.w2[1] = 1 - self.w2[0] - self.w2[2];

    self.w3[0] = 1;

    np = len(self.w1)*len(self.w2)*len(self.w3)

    l = 0
    for i in range(0, len(self.w1)):
      for j in range(0, len(self.w2)):
        for k in range(0, len(self.w3)):
            self.SW[l] = self.w1[i]*self.w2[j]*self.w3[k];
            l = l + 1;
    FF = zeros(3);
    GG = zeros(3);
    for i in range(0, np):
      FF = FF + self.SW[i]*self.F[i]
      GG = GG + self.SW[i]*self.G[i]
    self.v = FF*self.e_vector + GG*self.path_vector
    
if __name__ == '__main__':
    try:
        fuzzy_control()
    except rospy.ROSInterruptException: pass