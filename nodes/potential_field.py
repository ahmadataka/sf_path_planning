#!/usr/bin/env python
# Software License Agreement (BSD License)
#
# Copyright (c) 2008, Willow Garage, Inc.
# All rights reserved.
#


import roslib; roslib.load_manifest('sf_path_planning')
import math
import rospy
from geometry_msgs.msg import Pose
from geometry_msgs.msg import PoseArray
from std_msgs.msg import Float64MultiArray
#import tf

class potential_field(object):
  def __init__(self):
    rospy.init_node('potential_field')
    #print "a"
    self.field_type = rospy.get_param('/fieldtype')
    self.end_pose = Pose()
    self.vel_send = Float64MultiArray()
    self.omega_send = Float64MultiArray()
    #KP = 5.0
    # KP = 2.0
    KP = 0.5
    K_angle = 0.1
    #k, p, s = symbols('k p s')
    self.v = [0, 0, 0]
    self.w = [0, 0, 0]
    self.goal = [-0.0104626115946, 0.0363025846822, 0.36351393246]
    self.euler = [0.2944086776927468, -0.039215662698727255, 1.0206535062911635]
    self.goal_angle = [0.2944086776927468, -0.039215662698727255, 1.0206535062911635]
    self.obs = [10, 10, 10]
    self.limit_velocity = 5
    self.stiff = 1
    vel_pub = rospy.Publisher('field', Float64MultiArray, queue_size = 10)
    omega_pub = rospy.Publisher('field_euler', Float64MultiArray, queue_size = 10)
    pose_sub = rospy.Subscriber('tip_pose', PoseArray, self.get_pose)
    goal_sub = rospy.Subscriber('goal_pose', Pose, self.get_goal)
    euler_goal_sub = rospy.Subscriber('euler_goal', Float64MultiArray, self.get_euler_goal)
    euler_sub = rospy.Subscriber('euler', Float64MultiArray, self.get_euler)
    stiff_sub = rospy.Subscriber('stiffness/goal', Float64MultiArray, self.get_stiff)

    #tip_to_goal = tf.TransformListener()

    r = rospy.Rate(40)

    while not rospy.is_shutdown():
        #try:
            #(trans,rot) = tip_to_goal.lookupTransform("up_tip", "goal", rospy.Time(0))
        #except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            #continue
        self.vel_send.data = []
        self.omega_send.data = []
        self.v[0] = -KP*self.stiff*(self.end_pose.position.x-self.goal[0])
        self.v[1] = -KP*self.stiff*(self.end_pose.position.y-self.goal[1])
        self.v[2] = -KP*self.stiff*(self.end_pose.position.z-self.goal[2])
        self.w[0] = -K_angle*self.stiff*(self.euler[0]-self.goal_angle[0])
        self.w[1] = -K_angle*self.stiff*(self.euler[1]-self.goal_angle[1])
        self.w[2] = -K_angle*self.stiff*(self.euler[2]-self.goal_angle[2])
        #self.v[0] = KP*(trans[0])
        #self.v[1] = KP*(trans[1])
        #self.v[2] = KP*(trans[2])
        if(self.field_type == 1):
	      self.vel_limit()
        print(self.goal)
        for i in range(0,3):
            self.vel_send.data.append(self.v[i])
            self.omega_send.data.append(self.w[i])
        vel_pub.publish(self.vel_send)
        omega_pub.publish(self.omega_send)
        r.sleep()

        #rospy.spin()

  def get_pose(self,msg):
    self.end_pose.position.x = msg.poses[2].position.x
    self.end_pose.position.y = msg.poses[2].position.y
    self.end_pose.position.z = msg.poses[2].position.z
    self.end_pose.orientation.w = msg.poses[2].orientation.w
    self.end_pose.orientation.x = msg.poses[2].orientation.x
    self.end_pose.orientation.y = msg.poses[2].orientation.y
    self.end_pose.orientation.z = msg.poses[2].orientation.z

  def get_goal(self,msg):
    self.goal[0] = msg.position.x
    self.goal[1] = msg.position.y
    self.goal[2] = msg.position.z

  def get_stiff(self,msg):
    self.stiff = msg.data[0]

  def get_euler_goal(self,msg):
    self.goal_angle[0] = msg.data[0]
    self.goal_angle[1] = msg.data[1]
    self.goal_angle[2] = msg.data[2]

  def get_euler(self,msg):
    self.euler[0] = msg.data[0]
    self.euler[1] = msg.data[1]
    self.euler[2] = msg.data[2]

  def vel_limit(self):
    for i in range(0,3):
      if(self.v[i]>self.limit_velocity):
	self.v[i] = self.limit_velocity
      elif(self.v[i]<(self.limit_velocity*(-1))):
	self.v[i] = self.limit_velocity*(-1)

if __name__ == '__main__':
    try:
        potential_field()
    except rospy.ROSInterruptException: pass
