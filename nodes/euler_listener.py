#!/usr/bin/env python
import roslib
roslib.load_manifest('sf_path_planning')
import rospy
import tf
from std_msgs.msg import Float64MultiArray

if __name__ == '__main__':
    rospy.init_node('euler_listener')

    active_frame = rospy.get_param('~frame')
    reference_frame = rospy.get_param('~ref')
    topic_name = rospy.get_param('~publish_to')
    listener = tf.TransformListener()

    tip_pose = rospy.Publisher(topic_name, Float64MultiArray, queue_size = 10)

    rate = rospy.Rate(40.0)
    while not rospy.is_shutdown():
        try:
            (trans,rot) = listener.lookupTransform(reference_frame, active_frame, rospy.Time(0))
        except (tf.LookupException, tf.ConnectivityException, tf.ExtrapolationException):
            continue
        pose = Float64MultiArray()
        # rot = [0.0, 0.0, 0.707107, 0.707107]
        eul = tf.transformations.euler_from_quaternion(rot)
        # print eul
        pose.data = []
        for i in range(0,3):
            pose.data.append(eul[i])

        tip_pose.publish(pose)

        rate.sleep()
